/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Control.Doctor;

import Control.Login.AccountAuthentication;
import Control.User.CustomersListController;
import DAO.DoctorDAO;
import DAO.ServiceDAO;
import DAO.UserDAO;
import Model.Account.Doctor;
import Model.Account.User;
import Model.Service.ServiceType;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Asus
 */
public class DoctorListController extends AccountAuthentication {
 
    final String profileJsp = "../frontend/jsp/doctor/doctorList.jsp";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User account = (User) session.getAttribute("account");
        UserDAO userDAO = new UserDAO();
        User user = null;
        try {
            user = userDAO.getUser(account.getEmail());
        } catch (Exception ex) {
            Logger.getLogger(DoctorListController.class.getName()).log(Level.SEVERE, null, ex);
        }
        request.setAttribute("account", user);
        
        try {
            DoctorDAO doctorDao = new DoctorDAO();
            ServiceDAO serviceDao = new ServiceDAO();
            ArrayList<ServiceType> listService = serviceDao.listServiceType();

            String raw_service = request.getParameter("service");
            String raw_gender = request.getParameter("gender");
            String raw_status = request.getParameter("status");

            int service = (raw_service == null || raw_service.equals(""))
                    ? 0 : Integer.parseInt(raw_service);
            if (raw_gender == null) {
                raw_gender = "both";
            }
            if (raw_status == null) {
                raw_status = "both";
            }
            Boolean gender = (raw_gender.equals("both"))
                    ? null : (raw_gender.equals("male"));

            Boolean status = (raw_status.equals("both"))
                    ? null : (raw_status.equals("on"));

            request.setAttribute("listService", listService);
            request.setAttribute("service", service);
            request.setAttribute("gender", raw_gender);
            request.setAttribute("status", raw_status);

            ArrayList<Doctor> doctorList = doctorDao.searchDoctor(service, gender, status);
            request.setAttribute("doctorList", doctorList);
            request.getRequestDispatcher(profileJsp).forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(DoctorListController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
