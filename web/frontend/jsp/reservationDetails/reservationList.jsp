<%-- 
    Document   : reservationContact
    Created on : Jun 4, 2022, 4:12:47 PM
    Author     : tkoko
--%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <jsp:include page="../component/top.jsp"></jsp:include>
        <head>
            <style>
                .centerrr td{
                    padding-right: 80px;
                }
                .orderDetail tr td{
                    padding-right: 80px;
                }
            </style>
        </head>


        <div style="margin-bottom:  50px">
            <form action="list" method="GET" >
                Status:<select id="status" name="status" onchange="return submit()">
                    <option value="-1"
                            <c:if test="${requestScope.status eq -1}">
                                selected=""
                            </c:if>
                            >All</option>
                    <option value="0"
                            <c:if test="${requestScope.status eq 0}">
                                selected=""
                            </c:if>
                            >Incomplete</option>
                    <option value="1"
                            <c:if test="${requestScope.status eq 1}">
                                selected=""
                            </c:if>
                            >Complete</option>
                </select>
                From:<input id="from" type="date" name="from" value="${requestScope.from}">
                To:<input id="to" type="date" name="to" value="${requestScope.to}">
                <input id="search" type="text" name="search" placeholder="search" value="${requestScope.search}">
                <input type="submit" value="Submit">
            </form>
            <a href="add" style="font-size: 20px">Add</a>
            <div>
                <table class="table table-striped table-class" id= "table-id" border="1px" style="margin-bottom: 100px">
                    <tr>
                        <td style="text-align: center" >Order Information</td>
                        <td>Status</td>
                    </tr>
                    <c:set var="index" value="1"></c:set>
                    <c:forEach items="${requestScope.orders}" var="o">
                        <tr>
                            <td>
                                <table>
                                    <tr class="centerrr">
                                        <td>Ordinal number</td>
                                        <td>Name</td>
                                        <td style="padding-right: 200px">Email</td>
                                        <td>Total Price</td>
                                        <td><input value="See More" type="button" onclick="SeeMore(${o.oid})"></td>
                                    </tr>
                                    <tr>
                                        <td>${index}</td>
                                        <c:if test="${o.customer != null}">
                                            <td>${o.customer.name}</td> 
                                            <td>${o.customer.email}</td> 
                                        </c:if>
                                        <c:if test="${o.customer eq null}">
                                            <td>Emtpy</td> 
                                            <td>Emtpy</td> 
                                        </c:if>
                                        <td>${o.totalPrice}</td> 
                                    </tr>
                                </table>
                            </td>

                            <c:if test="${o.status eq true}">
                                <td>Complete</td>
                            </c:if>
                            <c:if test="${o.status eq false}">
                                <td>Incomplete
                                    <form action="list"  method="POST">
                                        <input hidden="" type="text" name="idUpdateStatus" value="${o.oid}">
                                        <input type="submit" value="Change status" onclick=" return ChangeStatus()">
                                        <input hidden=""  type="text" name="status" value="${requestScope.status}"  >
                                        <input hidden=""  type="date" name="from" value="${requestScope.from}" >
                                        <input hidden=""   type="date" name="to" value="${requestScope.to}" >
                                        <input hidden=""   type="text" name="search" value="${requestScope.search}" >
                                    </form>
                                </td>
                            </c:if>
                        </tr>
                        <tr>
                            <td>
                                <table class="orderDetail" id="orderDetail${o.oid}" style="display: none">
                                    <tr>
                                        <td>Service</td>
                                        <td>Examine Date</td>
                                        <td>Doctor</td>
                                        <td>Ordinal Number</td>
                                        <td>Name patient</td>
                                        <td>Phone</td>
                                    </tr>  
                                    <c:forEach items="${o.orderDetail}" var="od">
                                        <tr>
                                            <td>${od.service.sname}</td>
                                            <td>${od.examDate}</td>
                                            <td>${od.doctor.name}</td>
                                            <td>${od.ordinalNumber}</td>
                                            <td>${od.medicalRecord.userName}</td>
                                            <td>${od.medicalRecord.phone}</td>
                                            <c:if test="${od.examStatus eq true}">
                                                <td>Complete</td>
                                            </c:if>
                                            <c:if test="${od.examStatus != true}">
                                                <td>Incomplete</td>
                                            </c:if>
                                        </tr>  
                                    </c:forEach>
                                </table>  
                            </td>
                            <td></td>
                        </tr>
                        <c:set var="index" value="${index+1}"></c:set>
                    </c:forEach>

                </table>
            </div>
        </div>

    </body>
    <script>
        
        if (${requestScope.notice eq true}) {
            setTimeout(delayLoad, 100);
        }
        function delayLoad() {
            alert("Status order details is incomplete");
        }
        function ChangeStatus() {
            var result = confirm("Confirm the customer has paid");
                 return result;
        }
        function SeeMore(oid) {
            if (document.getElementById("orderDetail" + oid).style.display === "none") {
                document.getElementById("orderDetail" + oid).style.display = "block";
            } else {
                document.getElementById("orderDetail" + oid).style.display = "none";
            }


        }
        function submit() {
            return true;
        }
    </script>
</html>
<jsp:include page="../component/bottom.jsp"></jsp:include>