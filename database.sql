USE [BaeCare]
GO
/****** Object:  Table [dbo].[Author]    Script Date: 7/25/2022 1:09:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Author](
	[a_id] [int] IDENTITY(1,1) NOT NULL,
	[a_name] [varchar](100) NOT NULL,
 CONSTRAINT [PK_Author] PRIMARY KEY CLUSTERED 
(
	[a_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cart]    Script Date: 7/25/2022 1:09:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cart](
	[email] [varchar](100) NOT NULL,
	[s_id] [int] NOT NULL,
	[addCartDate] [smalldatetime] NOT NULL,
	[dateExam] [date] NULL,
	[cart_id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_Cart] PRIMARY KEY CLUSTERED 
(
	[cart_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 7/25/2022 1:09:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[c_id] [int] NOT NULL,
	[c_name] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[c_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Doctor]    Script Date: 7/25/2022 1:09:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Doctor](
	[email] [varchar](100) NOT NULL,
	[position] [varchar](100) NOT NULL,
	[training_process] [ntext] NULL,
	[working_process] [ntext] NULL,
	[research_work] [ntext] NULL,
	[scientific_research_topics] [ntext] NULL,
	[bonus] [ntext] NULL,
	[t_id] [int] NOT NULL,
	[twitter] [ntext] NULL,
	[facebook] [ntext] NULL,
	[linkedin] [ntext] NULL,
	[instagram] [ntext] NULL,
 CONSTRAINT [PK_Doctor_1] PRIMARY KEY CLUSTERED 
(
	[email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[exam_medicine]    Script Date: 7/25/2022 1:09:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[exam_medicine](
	[od_id] [int] NOT NULL,
	[medicine_id] [nchar](20) NOT NULL,
	[quantity] [int] NULL,
	[prescription] [ntext] NULL,
	[added_date] [date] NULL,
 CONSTRAINT [PK_exam_medicine] PRIMARY KEY CLUSTERED 
(
	[od_id] ASC,
	[medicine_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Feature]    Script Date: 7/25/2022 1:09:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Feature](
	[f_id] [int] NOT NULL,
	[f_name] [varchar](100) NOT NULL,
	[url] [varchar](200) NOT NULL,
 CONSTRAINT [PK_Feature] PRIMARY KEY CLUSTERED 
(
	[f_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Feedback]    Script Date: 7/25/2022 1:09:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Feedback](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[photo] [ntext] NULL,
	[star] [int] NULL,
	[details] [ntext] NULL,
	[s_id] [int] NULL,
	[u_email] [varchar](100) NULL,
	[o_id] [int] NULL,
	[status] [bit] NULL,
 CONSTRAINT [PK_Feedback] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Medical_record]    Script Date: 7/25/2022 1:09:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Medical_record](
	[mr_id] [int] IDENTITY(1,1) NOT NULL,
	[full_name] [nvarchar](200) NULL,
	[gender] [bit] NULL,
	[address] [nvarchar](200) NULL,
	[date_of_examination] [smalldatetime] NULL,
	[medical_history] [ntext] NULL,
	[prehistory] [ntext] NULL,
	[first_exam_service] [int] NULL,
	[reason_for_hospitalization] [nvarchar](2000) NULL,
	[phone] [varchar](50) NULL,
	[age] [int] NULL,
 CONSTRAINT [PK_Medical_record] PRIMARY KEY CLUSTERED 
(
	[mr_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[medicine]    Script Date: 7/25/2022 1:09:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[medicine](
	[id] [nchar](20) NOT NULL,
	[name] [nvarchar](500) NULL,
	[producer] [nvarchar](200) NULL,
	[element] [ntext] NULL,
	[quantity] [int] NULL,
	[inUse] [bit] NULL,
	[price] [money] NULL,
	[unit] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Order]    Script Date: 7/25/2022 1:09:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order](
	[o_id] [int] IDENTITY(1,1) NOT NULL,
	[email_customer] [varchar](100) NULL,
	[dateOrdered] [date] NULL,
	[totalCost] [money] NULL,
	[status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[o_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Order_detail]    Script Date: 7/25/2022 1:09:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order_detail](
	[od_id] [int] IDENTITY(1,1) NOT NULL,
	[o_id] [int] NOT NULL,
	[s_id] [int] NOT NULL,
	[email_doctor] [varchar](100) NULL,
	[examineDate] [date] NULL,
	[exam_Status] [bit] NULL,
	[note] [ntext] NULL,
	[price] [money] NULL,
	[checkOutDate] [smalldatetime] NULL,
	[medical_Status] [bit] NULL,
	[mr_id] [int] NULL,
	[symptom] [ntext] NULL,
	[ordinal_number] [int] NULL,
 CONSTRAINT [PK_Order_detail] PRIMARY KEY CLUSTERED 
(
	[od_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Post]    Script Date: 7/25/2022 1:09:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Post](
	[postID] [int] IDENTITY(1,1) NOT NULL,
	[postTitle] [nvarchar](200) NOT NULL,
	[postDetail] [ntext] NOT NULL,
	[postImg] [char](300) NULL,
	[postDate] [smalldatetime] NULL,
	[onHomepage] [bit] NULL,
	[c_id] [int] NULL,
	[a_id] [int] NULL,
 CONSTRAINT [PK__Post__DD0C73BAA0E598D3] PRIMARY KEY CLUSTERED 
(
	[postID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 7/25/2022 1:09:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[r_id] [int] NOT NULL,
	[r_name] [varchar](200) NOT NULL,
 CONSTRAINT [PK_Group] PRIMARY KEY CLUSTERED 
(
	[r_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role_Feature]    Script Date: 7/25/2022 1:09:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role_Feature](
	[r_id] [int] NOT NULL,
	[f_id] [int] NOT NULL,
 CONSTRAINT [PK_Group_Feature] PRIMARY KEY CLUSTERED 
(
	[r_id] ASC,
	[f_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Service]    Script Date: 7/25/2022 1:09:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Service](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](200) NOT NULL,
	[status] [bit] NOT NULL,
	[original_price] [money] NULL,
	[sale_price] [money] NULL,
	[t_id] [int] NOT NULL,
	[details] [nvarchar](500) NOT NULL,
	[updated_date] [date] NOT NULL,
	[photo] [ntext] NULL,
 CONSTRAINT [PK_Service] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Service_Type]    Script Date: 7/25/2022 1:09:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Service_Type](
	[t_id] [int] IDENTITY(1,1) NOT NULL,
	[t_name] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_Service_Type] PRIMARY KEY CLUSTERED 
(
	[t_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Slide]    Script Date: 7/25/2022 1:09:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Slide](
	[slideID] [int] IDENTITY(1,1) NOT NULL,
	[slideTitle] [nvarchar](200) NOT NULL,
	[slideDetail] [ntext] NOT NULL,
	[slideImg] [ntext] NULL,
	[slideDate] [smalldatetime] NULL,
	[onHomepage] [bit] NULL,
 CONSTRAINT [PK__Slide__52D88E1FCDA9851E] PRIMARY KEY CLUSTERED 
(
	[slideID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 7/25/2022 1:09:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[email] [varchar](100) NOT NULL,
	[name] [nvarchar](200) NOT NULL,
	[gender] [bit] NOT NULL,
	[phone] [varchar](50) NOT NULL,
	[address] [varchar](200) NOT NULL,
	[dob] [date] NOT NULL,
	[photo] [ntext] NOT NULL,
	[password] [varchar](50) NULL,
	[status] [bit] NULL,
	[r_id] [int] NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Author] ON 

INSERT [dbo].[Author] ([a_id], [a_name]) VALUES (1, N'Nguyen Dinh Tho')
INSERT [dbo].[Author] ([a_id], [a_name]) VALUES (2, N'Vu Thi Ban Mai')
INSERT [dbo].[Author] ([a_id], [a_name]) VALUES (3, N'Nguyen Chi Tung')
INSERT [dbo].[Author] ([a_id], [a_name]) VALUES (4, N'Nguyen Khac Son')
INSERT [dbo].[Author] ([a_id], [a_name]) VALUES (5, N'Nguyen Van Nghia')
SET IDENTITY_INSERT [dbo].[Author] OFF
GO
SET IDENTITY_INSERT [dbo].[Cart] ON 

INSERT [dbo].[Cart] ([email], [s_id], [addCartDate], [dateExam], [cart_id]) VALUES (N'tkokoi12332@gmail.com', 1, CAST(N'2022-05-18T22:56:00' AS SmallDateTime), CAST(N'2022-06-30' AS Date), 4)
INSERT [dbo].[Cart] ([email], [s_id], [addCartDate], [dateExam], [cart_id]) VALUES (N'mai6@gmail.com', 1, CAST(N'2022-07-11T00:00:00' AS SmallDateTime), CAST(N'2022-07-12' AS Date), 15)
INSERT [dbo].[Cart] ([email], [s_id], [addCartDate], [dateExam], [cart_id]) VALUES (N'mai6@gmail.com', 2, CAST(N'2022-07-11T00:00:00' AS SmallDateTime), CAST(N'2022-07-11' AS Date), 16)
INSERT [dbo].[Cart] ([email], [s_id], [addCartDate], [dateExam], [cart_id]) VALUES (N'mai6@gmail.com', 3, CAST(N'2022-07-11T00:00:00' AS SmallDateTime), CAST(N'2022-07-12' AS Date), 17)
INSERT [dbo].[Cart] ([email], [s_id], [addCartDate], [dateExam], [cart_id]) VALUES (N'mai3@gmail.com', 5, CAST(N'2022-07-18T00:00:00' AS SmallDateTime), CAST(N'2022-07-19' AS Date), 27)
INSERT [dbo].[Cart] ([email], [s_id], [addCartDate], [dateExam], [cart_id]) VALUES (N'sonnkhe151341@fpt.edu.vn', 1, CAST(N'2022-07-21T00:00:00' AS SmallDateTime), CAST(N'2022-07-22' AS Date), 29)
SET IDENTITY_INSERT [dbo].[Cart] OFF
GO
INSERT [dbo].[Category] ([c_id], [c_name]) VALUES (1, N'Health Care')
INSERT [dbo].[Category] ([c_id], [c_name]) VALUES (2, N'Patient And Doctor')
INSERT [dbo].[Category] ([c_id], [c_name]) VALUES (3, N'Cardiovascular')
INSERT [dbo].[Category] ([c_id], [c_name]) VALUES (4, N'Digestive')
INSERT [dbo].[Category] ([c_id], [c_name]) VALUES (5, N'Infection')
INSERT [dbo].[Category] ([c_id], [c_name]) VALUES (6, N'Orthopaedic Trauma')
INSERT [dbo].[Category] ([c_id], [c_name]) VALUES (7, N'Traumatic Brain Injury')
INSERT [dbo].[Category] ([c_id], [c_name]) VALUES (8, N'Abdominal Trauma')
INSERT [dbo].[Category] ([c_id], [c_name]) VALUES (9, N'Eye')
INSERT [dbo].[Category] ([c_id], [c_name]) VALUES (10, N'Ear, Nose and Throat')
INSERT [dbo].[Category] ([c_id], [c_name]) VALUES (11, N'Dentomaxillofacial')
INSERT [dbo].[Category] ([c_id], [c_name]) VALUES (12, N'Neuropathy')
GO
INSERT [dbo].[Doctor] ([email], [position], [training_process], [working_process], [research_work], [scientific_research_topics], [bonus], [t_id], [twitter], [facebook], [linkedin], [instagram]) VALUES (N'anhvhhs153214@fpt.edu.vn', N'Medicine Deans', N'Hanoi Medical University, Hanoi, Vietnam, 2015', N'- Therapist, Cardiology Department, District 2 Hospital, Ho Chi Minh City, Vietnam, 2016-2017 <br>
- Senior Doctor, Cardiovascular Resuscitation Unit, Tam Duc Heart Hospital, Ho Chi Minh City, Vietnam, 2017-2020
 <br>
 - Senior Physician, Cardiology Department, FV Hospital, from 2021', N'- Cardiovascular care <br>
- Percutaneous coronary intervention <br> 
- Peripheral vascular intervention <br>
- Percutaneous valvular regurgitation for regurgitation and mitral stenosis <br> 
- Septic and ductal malformations', N'Antibiotics treatments.', N'Certificate of Merit from the Minister of Health for individuals who have made excellent achievements in successfully performing 15 organ transplants on August 12 - August 20, 2019', 1, N'https://twitter.com/MaiBan25661905', N'https://www.facebook.com/boconganh.001/', N'https://www.linkedin.com/in/mai-ban-9b7a04244/', N'https://www.instagram.com/banmai201/')
INSERT [dbo].[Doctor] ([email], [position], [training_process], [working_process], [research_work], [scientific_research_topics], [bonus], [t_id], [twitter], [facebook], [linkedin], [instagram]) VALUES (N'doctor1@gmail.com', N'Medicine Deans', N'Hanoi Medical University, Hanoi, Vietnam, 2015', N'- Therapist, Cardiology Department, District 2 Hospital, Ho Chi Minh City, Vietnam, 2016-2017
- Senior Doctor, Cardiovascular Resuscitation Unit, Tam Duc Heart Hospital, Ho Chi Minh City, Vietnam, 2017-2020
- Senior Physician, Cardiology Department, FV Hospital, from 2021', N'- Cardiovascular care
- Percutaneous coronary intervention
- Peripheral vascular intervention
- Percutaneous valvular regurgitation for regurgitation and mitral stenosis
- Septic and ductal malformations', N'Antibiotics treatments.', N'Certificate of Merit from the Minister of Health for individuals who have made excellent achievements in successfully performing 15 organ transplants on August 12 - August 20, 2019', 1, N'https://twitter.com/MaiBan25661905', N'https://www.facebook.com/boconganh.001/', N'https://www.linkedin.com/in/mai-ban-9b7a04244/', N'https://www.instagram.com/banmai201/')
INSERT [dbo].[Doctor] ([email], [position], [training_process], [working_process], [research_work], [scientific_research_topics], [bonus], [t_id], [twitter], [facebook], [linkedin], [instagram]) VALUES (N'doctor10@gmail.com', N'Medicine Deans', N'Hanoi Medical University, Hanoi, Vietnam, 2015', N'- Therapist, Cardiology Department, District 2 Hospital, Ho Chi Minh City, Vietnam, 2016-2017
- Senior Doctor, Cardiovascular Resuscitation Unit, Tam Duc Heart Hospital, Ho Chi Minh City, Vietnam, 2017-2020
- Senior Physician, Cardiology Department, FV Hospital, from 2021', N'- Cardiovascular care
- Percutaneous coronary intervention
- Peripheral vascular intervention
- Percutaneous valvular regurgitation for regurgitation and mitral stenosis
- Septic and ductal malformations', N'Antibiotics treatments.', N'Certificate of Merit from the Minister of Health for individuals who have made excellent achievements in successfully performing 15 organ transplants on August 12 - August 20, 2019', 2, N'https://twitter.com/MaiBan25661905', N'https://www.facebook.com/boconganh.001/', N'https://www.linkedin.com/in/mai-ban-9b7a04244/', N'https://www.instagram.com/banmai201/')
INSERT [dbo].[Doctor] ([email], [position], [training_process], [working_process], [research_work], [scientific_research_topics], [bonus], [t_id], [twitter], [facebook], [linkedin], [instagram]) VALUES (N'doctor11@gmail.com', N'Medicine Deans', N'Hanoi Medical University, Hanoi, Vietnam, 2015', N'- Therapist, Cardiology Department, District 2 Hospital, Ho Chi Minh City, Vietnam, 2016-2017
- Senior Doctor, Cardiovascular Resuscitation Unit, Tam Duc Heart Hospital, Ho Chi Minh City, Vietnam, 2017-2020
- Senior Physician, Cardiology Department, FV Hospital, from 2021', N'- Cardiovascular care
- Percutaneous coronary intervention
- Peripheral vascular intervention
- Percutaneous valvular regurgitation for regurgitation and mitral stenosis
- Septic and ductal malformations', N'Antibiotics treatments.', N'Certificate of Merit from the Minister of Health for individuals who have made excellent achievements in successfully performing 15 organ transplants on August 12 - August 20, 2019', 3, N'https://twitter.com/MaiBan25661905', N'https://www.facebook.com/boconganh.001/', N'https://www.linkedin.com/in/mai-ban-9b7a04244/', N'https://www.instagram.com/banmai201/')
INSERT [dbo].[Doctor] ([email], [position], [training_process], [working_process], [research_work], [scientific_research_topics], [bonus], [t_id], [twitter], [facebook], [linkedin], [instagram]) VALUES (N'doctor12@gmail.com', N'Medicine Deans', N'Hanoi Medical University, Hanoi, Vietnam, 2015', N'- Therapist, Cardiology Department, District 2 Hospital, Ho Chi Minh City, Vietnam, 2016-2017
- Senior Doctor, Cardiovascular Resuscitation Unit, Tam Duc Heart Hospital, Ho Chi Minh City, Vietnam, 2017-2020
- Senior Physician, Cardiology Department, FV Hospital, from 2021', N'- Cardiovascular care
- Percutaneous coronary intervention
- Peripheral vascular intervention
- Percutaneous valvular regurgitation for regurgitation and mitral stenosis
- Septic and ductal malformations', N'Antibiotics treatments.', N'Certificate of Merit from the Minister of Health for individuals who have made excellent achievements in successfully performing 15 organ transplants on August 12 - August 20, 2019', 4, N'https://twitter.com/MaiBan25661905', N'https://www.facebook.com/boconganh.001/', N'https://www.linkedin.com/in/mai-ban-9b7a04244/', N'https://www.instagram.com/banmai201/')
INSERT [dbo].[Doctor] ([email], [position], [training_process], [working_process], [research_work], [scientific_research_topics], [bonus], [t_id], [twitter], [facebook], [linkedin], [instagram]) VALUES (N'doctor13@gmail.com', N'Medicine Deans', N'Hanoi Medical University, Hanoi, Vietnam, 2015', N'- Therapist, Cardiology Department, District 2 Hospital, Ho Chi Minh City, Vietnam, 2016-2017
- Senior Doctor, Cardiovascular Resuscitation Unit, Tam Duc Heart Hospital, Ho Chi Minh City, Vietnam, 2017-2020
- Senior Physician, Cardiology Department, FV Hospital, from 2021', N'- Cardiovascular care
- Percutaneous coronary intervention
- Peripheral vascular intervention
- Percutaneous valvular regurgitation for regurgitation and mitral stenosis
- Septic and ductal malformations', N'Antibiotics treatments.', N'Certificate of Merit from the Minister of Health for individuals who have made excellent achievements in successfully performing 15 organ transplants on August 12 - August 20, 2019', 1, N'https://twitter.com/MaiBan25661905', N'https://www.facebook.com/boconganh.001/', N'https://www.linkedin.com/in/mai-ban-9b7a04244/', N'https://www.instagram.com/banmai201/')
INSERT [dbo].[Doctor] ([email], [position], [training_process], [working_process], [research_work], [scientific_research_topics], [bonus], [t_id], [twitter], [facebook], [linkedin], [instagram]) VALUES (N'doctor14@gmail.com', N'Medicine Deans', N'Hanoi Medical University, Hanoi, Vietnam, 2015', N'- Therapist, Cardiology Department, District 2 Hospital, Ho Chi Minh City, Vietnam, 2016-2017
- Senior Doctor, Cardiovascular Resuscitation Unit, Tam Duc Heart Hospital, Ho Chi Minh City, Vietnam, 2017-2020
- Senior Physician, Cardiology Department, FV Hospital, from 2021', N'- Cardiovascular care
- Percutaneous coronary intervention
- Peripheral vascular intervention
- Percutaneous valvular regurgitation for regurgitation and mitral stenosis
- Septic and ductal malformations', N'Antibiotics treatments.', N'Certificate of Merit from the Minister of Health for individuals who have made excellent achievements in successfully performing 15 organ transplants on August 12 - August 20, 2019', 2, N'https://twitter.com/MaiBan25661905', N'https://www.facebook.com/boconganh.001/', N'https://www.linkedin.com/in/mai-ban-9b7a04244/', N'https://www.instagram.com/banmai201/')
INSERT [dbo].[Doctor] ([email], [position], [training_process], [working_process], [research_work], [scientific_research_topics], [bonus], [t_id], [twitter], [facebook], [linkedin], [instagram]) VALUES (N'doctor15@gmail.com', N'Medicine Deans', N'Hanoi Medical University, Hanoi, Vietnam, 2015', N'- Therapist, Cardiology Department, District 2 Hospital, Ho Chi Minh City, Vietnam, 2016-2017
- Senior Doctor, Cardiovascular Resuscitation Unit, Tam Duc Heart Hospital, Ho Chi Minh City, Vietnam, 2017-2020
- Senior Physician, Cardiology Department, FV Hospital, from 2021', N'- Cardiovascular care
- Percutaneous coronary intervention
- Peripheral vascular intervention
- Percutaneous valvular regurgitation for regurgitation and mitral stenosis
- Septic and ductal malformations', N'Antibiotics treatments.', N'Certificate of Merit from the Minister of Health for individuals who have made excellent achievements in successfully performing 15 organ transplants on August 12 - August 20, 2019', 3, N'https://twitter.com/MaiBan25661905', N'https://www.facebook.com/boconganh.001/', N'https://www.linkedin.com/in/mai-ban-9b7a04244/', N'https://www.instagram.com/banmai201/')
INSERT [dbo].[Doctor] ([email], [position], [training_process], [working_process], [research_work], [scientific_research_topics], [bonus], [t_id], [twitter], [facebook], [linkedin], [instagram]) VALUES (N'doctor16@gmail.com', N'Medicine Deans', N'Hanoi Medical University, Hanoi, Vietnam, 2015', N'- Therapist, Cardiology Department, District 2 Hospital, Ho Chi Minh City, Vietnam, 2016-2017
- Senior Doctor, Cardiovascular Resuscitation Unit, Tam Duc Heart Hospital, Ho Chi Minh City, Vietnam, 2017-2020
- Senior Physician, Cardiology Department, FV Hospital, from 2021', N'- Cardiovascular care
- Percutaneous coronary intervention
- Peripheral vascular intervention
- Percutaneous valvular regurgitation for regurgitation and mitral stenosis
- Septic and ductal malformations', N'Antibiotics treatments.', N'Certificate of Merit from the Minister of Health for individuals who have made excellent achievements in successfully performing 15 organ transplants on August 12 - August 20, 2019', 4, N'https://twitter.com/MaiBan25661905', N'https://www.facebook.com/boconganh.001/', N'https://www.linkedin.com/in/mai-ban-9b7a04244/', N'https://www.instagram.com/banmai201/')
INSERT [dbo].[Doctor] ([email], [position], [training_process], [working_process], [research_work], [scientific_research_topics], [bonus], [t_id], [twitter], [facebook], [linkedin], [instagram]) VALUES (N'doctor17@gmail.com', N'Medicine Deans', N'Hanoi Medical University, Hanoi, Vietnam, 2015', N'- Therapist, Cardiology Department, District 2 Hospital, Ho Chi Minh City, Vietnam, 2016-2017
- Senior Doctor, Cardiovascular Resuscitation Unit, Tam Duc Heart Hospital, Ho Chi Minh City, Vietnam, 2017-2020
- Senior Physician, Cardiology Department, FV Hospital, from 2021', N'- Cardiovascular care
- Percutaneous coronary intervention
- Peripheral vascular intervention
- Percutaneous valvular regurgitation for regurgitation and mitral stenosis
- Septic and ductal malformations', N'Antibiotics treatments.', N'Certificate of Merit from the Minister of Health for individuals who have made excellent achievements in successfully performing 15 organ transplants on August 12 - August 20, 2019', 1, N'https://twitter.com/MaiBan25661905', N'https://www.facebook.com/boconganh.001/', N'https://www.linkedin.com/in/mai-ban-9b7a04244/', N'https://www.instagram.com/banmai201/')
INSERT [dbo].[Doctor] ([email], [position], [training_process], [working_process], [research_work], [scientific_research_topics], [bonus], [t_id], [twitter], [facebook], [linkedin], [instagram]) VALUES (N'doctor18@gmail.com', N'Medicine Deans', N'Hanoi Medical University, Hanoi, Vietnam, 2015', N'- Therapist, Cardiology Department, District 2 Hospital, Ho Chi Minh City, Vietnam, 2016-2017
- Senior Doctor, Cardiovascular Resuscitation Unit, Tam Duc Heart Hospital, Ho Chi Minh City, Vietnam, 2017-2020
- Senior Physician, Cardiology Department, FV Hospital, from 2021', N'- Cardiovascular care
- Percutaneous coronary intervention
- Peripheral vascular intervention
- Percutaneous valvular regurgitation for regurgitation and mitral stenosis
- Septic and ductal malformations', N'Antibiotics treatments.', N'Certificate of Merit from the Minister of Health for individuals who have made excellent achievements in successfully performing 15 organ transplants on August 12 - August 20, 2019', 2, N'https://twitter.com/MaiBan25661905', N'https://www.facebook.com/boconganh.001/', N'https://www.linkedin.com/in/mai-ban-9b7a04244/', N'https://www.instagram.com/banmai201/')
INSERT [dbo].[Doctor] ([email], [position], [training_process], [working_process], [research_work], [scientific_research_topics], [bonus], [t_id], [twitter], [facebook], [linkedin], [instagram]) VALUES (N'doctor19@gmail.com', N'Medicine Deans', N'Hanoi Medical University, Hanoi, Vietnam, 2015', N'- Therapist, Cardiology Department, District 2 Hospital, Ho Chi Minh City, Vietnam, 2016-2017
- Senior Doctor, Cardiovascular Resuscitation Unit, Tam Duc Heart Hospital, Ho Chi Minh City, Vietnam, 2017-2020
- Senior Physician, Cardiology Department, FV Hospital, from 2021', N'- Cardiovascular care
- Percutaneous coronary intervention
- Peripheral vascular intervention
- Percutaneous valvular regurgitation for regurgitation and mitral stenosis
- Septic and ductal malformations', N'Antibiotics treatments.', N'Certificate of Merit from the Minister of Health for individuals who have made excellent achievements in successfully performing 15 organ transplants on August 12 - August 20, 2019', 3, N'https://twitter.com/MaiBan25661905', N'https://www.facebook.com/boconganh.001/', N'https://www.linkedin.com/in/mai-ban-9b7a04244/', N'https://www.instagram.com/banmai201/')
INSERT [dbo].[Doctor] ([email], [position], [training_process], [working_process], [research_work], [scientific_research_topics], [bonus], [t_id], [twitter], [facebook], [linkedin], [instagram]) VALUES (N'doctor2@gmail.com', N'Medicine Deans', N'Hanoi Medical University, Hanoi, Vietnam, 2015', N'- Therapist, Cardiology Department, District 2 Hospital, Ho Chi Minh City, Vietnam, 2016-2017
- Senior Doctor, Cardiovascular Resuscitation Unit, Tam Duc Heart Hospital, Ho Chi Minh City, Vietnam, 2017-2020
- Senior Physician, Cardiology Department, FV Hospital, from 2021', N'- Cardiovascular care
- Percutaneous coronary intervention
- Peripheral vascular intervention
- Percutaneous valvular regurgitation for regurgitation and mitral stenosis
- Septic and ductal malformations', N'Antibiotics treatments.', N'Certificate of Merit from the Minister of Health for individuals who have made excellent achievements in successfully performing 15 organ transplants on August 12 - August 20, 2019', 2, N'https://twitter.com/MaiBan25661905', N'https://www.facebook.com/boconganh.001/', N'https://www.linkedin.com/in/mai-ban-9b7a04244/', N'https://www.instagram.com/banmai201/')
INSERT [dbo].[Doctor] ([email], [position], [training_process], [working_process], [research_work], [scientific_research_topics], [bonus], [t_id], [twitter], [facebook], [linkedin], [instagram]) VALUES (N'doctor20@gmail.com', N'Medicine Deans', N'Hanoi Medical University, Hanoi, Vietnam, 2015', N'- Therapist, Cardiology Department, District 2 Hospital, Ho Chi Minh City, Vietnam, 2016-2017
- Senior Doctor, Cardiovascular Resuscitation Unit, Tam Duc Heart Hospital, Ho Chi Minh City, Vietnam, 2017-2020
- Senior Physician, Cardiology Department, FV Hospital, from 2021', N'- Cardiovascular care
- Percutaneous coronary intervention
- Peripheral vascular intervention
- Percutaneous valvular regurgitation for regurgitation and mitral stenosis
- Septic and ductal malformations', N'Antibiotics treatments.', N'Certificate of Merit from the Minister of Health for individuals who have made excellent achievements in successfully performing 15 organ transplants on August 12 - August 20, 2019', 4, N'https://twitter.com/MaiBan25661905', N'https://www.facebook.com/boconganh.001/', N'https://www.linkedin.com/in/mai-ban-9b7a04244/', N'https://www.instagram.com/banmai201/')
INSERT [dbo].[Doctor] ([email], [position], [training_process], [working_process], [research_work], [scientific_research_topics], [bonus], [t_id], [twitter], [facebook], [linkedin], [instagram]) VALUES (N'doctor3@gmail.com', N'Medicine Deans', N'Hanoi Medical University, Hanoi, Vietnam, 2015', N'- Therapist, Cardiology Department, District 2 Hospital, Ho Chi Minh City, Vietnam, 2016-2017
- Senior Doctor, Cardiovascular Resuscitation Unit, Tam Duc Heart Hospital, Ho Chi Minh City, Vietnam, 2017-2020
- Senior Physician, Cardiology Department, FV Hospital, from 2021', N'- Cardiovascular care
- Percutaneous coronary intervention
- Peripheral vascular intervention
- Percutaneous valvular regurgitation for regurgitation and mitral stenosis
- Septic and ductal malformations', N'Antibiotics treatments.', N'Certificate of Merit from the Minister of Health for individuals who have made excellent achievements in successfully performing 15 organ transplants on August 12 - August 20, 2019', 3, N'https://twitter.com/MaiBan25661905', N'https://www.facebook.com/boconganh.001/', N'https://www.linkedin.com/in/mai-ban-9b7a04244/', N'https://www.instagram.com/banmai201/')
INSERT [dbo].[Doctor] ([email], [position], [training_process], [working_process], [research_work], [scientific_research_topics], [bonus], [t_id], [twitter], [facebook], [linkedin], [instagram]) VALUES (N'doctor4@gmail.com', N'Medicine Deans', N'Hanoi Medical University, Hanoi, Vietnam, 2015', N'- Therapist, Cardiology Department, District 2 Hospital, Ho Chi Minh City, Vietnam, 2016-2017
- Senior Doctor, Cardiovascular Resuscitation Unit, Tam Duc Heart Hospital, Ho Chi Minh City, Vietnam, 2017-2020
- Senior Physician, Cardiology Department, FV Hospital, from 2021', N'- Cardiovascular care
- Percutaneous coronary intervention
- Peripheral vascular intervention
- Percutaneous valvular regurgitation for regurgitation and mitral stenosis
- Septic and ductal malformations', N'Antibiotics treatments.', N'Certificate of Merit from the Minister of Health for individuals who have made excellent achievements in successfully performing 15 organ transplants on August 12 - August 20, 2019', 4, N'https://twitter.com/MaiBan25661905', N'https://www.facebook.com/boconganh.001/', N'https://www.linkedin.com/in/mai-ban-9b7a04244/', N'https://www.instagram.com/banmai201/')
INSERT [dbo].[Doctor] ([email], [position], [training_process], [working_process], [research_work], [scientific_research_topics], [bonus], [t_id], [twitter], [facebook], [linkedin], [instagram]) VALUES (N'doctor5@gmail.com', N'Medicine Deans', N'Hanoi Medical University, Hanoi, Vietnam, 2015', N'- Therapist, Cardiology Department, District 2 Hospital, Ho Chi Minh City, Vietnam, 2016-2017
- Senior Doctor, Cardiovascular Resuscitation Unit, Tam Duc Heart Hospital, Ho Chi Minh City, Vietnam, 2017-2020
- Senior Physician, Cardiology Department, FV Hospital, from 2021', N'- Cardiovascular care
- Percutaneous coronary intervention
- Peripheral vascular intervention
- Percutaneous valvular regurgitation for regurgitation and mitral stenosis
- Septic and ductal malformations', N'Antibiotics treatments.', N'Certificate of Merit from the Minister of Health for individuals who have made excellent achievements in successfully performing 15 organ transplants on August 12 - August 20, 2019', 1, N'https://twitter.com/MaiBan25661905', N'https://www.facebook.com/boconganh.001/', N'https://www.linkedin.com/in/mai-ban-9b7a04244/', N'https://www.instagram.com/banmai201/')
INSERT [dbo].[Doctor] ([email], [position], [training_process], [working_process], [research_work], [scientific_research_topics], [bonus], [t_id], [twitter], [facebook], [linkedin], [instagram]) VALUES (N'doctor6@gmail.com', N'Medicine Deans', N'Hanoi Medical University, Hanoi, Vietnam, 2015', N'- Therapist, Cardiology Department, District 2 Hospital, Ho Chi Minh City, Vietnam, 2016-2017
- Senior Doctor, Cardiovascular Resuscitation Unit, Tam Duc Heart Hospital, Ho Chi Minh City, Vietnam, 2017-2020
- Senior Physician, Cardiology Department, FV Hospital, from 2021', N'- Cardiovascular care
- Percutaneous coronary intervention
- Peripheral vascular intervention
- Percutaneous valvular regurgitation for regurgitation and mitral stenosis
- Septic and ductal malformations', N'Antibiotics treatments.', N'Certificate of Merit from the Minister of Health for individuals who have made excellent achievements in successfully performing 15 organ transplants on August 12 - August 20, 2019', 2, N'https://twitter.com/MaiBan25661905', N'https://www.facebook.com/boconganh.001/', N'https://www.linkedin.com/in/mai-ban-9b7a04244/', N'https://www.instagram.com/banmai201/')
INSERT [dbo].[Doctor] ([email], [position], [training_process], [working_process], [research_work], [scientific_research_topics], [bonus], [t_id], [twitter], [facebook], [linkedin], [instagram]) VALUES (N'doctor7@gmail.com', N'Medicine Deans', N'Hanoi Medical University, Hanoi, Vietnam, 2015', N'- Therapist, Cardiology Department, District 2 Hospital, Ho Chi Minh City, Vietnam, 2016-2017
- Senior Doctor, Cardiovascular Resuscitation Unit, Tam Duc Heart Hospital, Ho Chi Minh City, Vietnam, 2017-2020
- Senior Physician, Cardiology Department, FV Hospital, from 2021', N'- Cardiovascular care
- Percutaneous coronary intervention
- Peripheral vascular intervention
- Percutaneous valvular regurgitation for regurgitation and mitral stenosis
- Septic and ductal malformations', N'Antibiotics treatments.', N'Certificate of Merit from the Minister of Health for individuals who have made excellent achievements in successfully performing 15 organ transplants on August 12 - August 20, 2019', 3, N'https://twitter.com/MaiBan25661905', N'https://www.facebook.com/boconganh.001/', N'https://www.linkedin.com/in/mai-ban-9b7a04244/', N'https://www.instagram.com/banmai201/')
INSERT [dbo].[Doctor] ([email], [position], [training_process], [working_process], [research_work], [scientific_research_topics], [bonus], [t_id], [twitter], [facebook], [linkedin], [instagram]) VALUES (N'doctor8@gmail.com', N'Medicine Deans', N'Hanoi Medical University, Hanoi, Vietnam, 2015', N'- Therapist, Cardiology Department, District 2 Hospital, Ho Chi Minh City, Vietnam, 2016-2017
- Senior Doctor, Cardiovascular Resuscitation Unit, Tam Duc Heart Hospital, Ho Chi Minh City, Vietnam, 2017-2020
- Senior Physician, Cardiology Department, FV Hospital, from 2021', N'- Cardiovascular care
- Percutaneous coronary intervention
- Peripheral vascular intervention
- Percutaneous valvular regurgitation for regurgitation and mitral stenosis
- Septic and ductal malformations', N'Antibiotics treatments.', N'Certificate of Merit from the Minister of Health for individuals who have made excellent achievements in successfully performing 15 organ transplants on August 12 - August 20, 2019', 4, N'https://twitter.com/MaiBan25661905', N'https://www.facebook.com/boconganh.001/', N'https://www.linkedin.com/in/mai-ban-9b7a04244/', N'https://www.instagram.com/banmai201/')
INSERT [dbo].[Doctor] ([email], [position], [training_process], [working_process], [research_work], [scientific_research_topics], [bonus], [t_id], [twitter], [facebook], [linkedin], [instagram]) VALUES (N'doctor9@gmail.com', N'Medicine Deans', N'Hanoi Medical University, Hanoi, Vietnam, 2015', N'- Therapist, Cardiology Department, District 2 Hospital, Ho Chi Minh City, Vietnam, 2016-2017
- Senior Doctor, Cardiovascular Resuscitation Unit, Tam Duc Heart Hospital, Ho Chi Minh City, Vietnam, 2017-2020
- Senior Physician, Cardiology Department, FV Hospital, from 2021', N'- Cardiovascular care
- Percutaneous coronary intervention
- Peripheral vascular intervention
- Percutaneous valvular regurgitation for regurgitation and mitral stenosis
- Septic and ductal malformations', N'Antibiotics treatments.', N'Certificate of Merit from the Minister of Health for individuals who have made excellent achievements in successfully performing 15 organ transplants on August 12 - August 20, 2019', 1, N'https://twitter.com/MaiBan25661905', N'https://www.facebook.com/boconganh.001/', N'https://www.linkedin.com/in/mai-ban-9b7a04244/', N'https://www.instagram.com/banmai201/')
INSERT [dbo].[Doctor] ([email], [position], [training_process], [working_process], [research_work], [scientific_research_topics], [bonus], [t_id], [twitter], [facebook], [linkedin], [instagram]) VALUES (N'nampxhhe151338@fpt.edu.vn', N'Medicine Deans', N'Hanoi Medical University, Hanoi, Vietnam, 2015', N'- Therapist, Cardiology Department, District 2 Hospital, Ho Chi Minh City, Vietnam, 2016-2017
- Senior Doctor, Cardiovascular Resuscitation Unit, Tam Duc Heart Hospital, Ho Chi Minh City, Vietnam, 2017-2020
- Senior Physician, Cardiology Department, FV Hospital, from 2021', N'- Cardiovascular care
- Percutaneous coronary intervention
- Peripheral vascular intervention
- Percutaneous valvular regurgitation for regurgitation and mitral stenosis
- Septic and ductal malformations', N'Antibiotics treatments.', N'Certificate of Merit from the Minister of Health for individuals who have made excellent achievements in successfully performing 15 organ transplants on August 12 - August 20, 2019', 2, N'https://twitter.com/MaiBan25661905', N'https://www.facebook.com/boconganh.001/', N'https://www.linkedin.com/in/mai-ban-9b7a04244/', N'https://www.instagram.com/banmai201/')
INSERT [dbo].[Doctor] ([email], [position], [training_process], [working_process], [research_work], [scientific_research_topics], [bonus], [t_id], [twitter], [facebook], [linkedin], [instagram]) VALUES (N'tho', N'Medicine Deans', N'Hanoi Medical University, Hanoi, Vietnam, 2015', N'- Therapist, Cardiology Department, District 2 Hospital, Ho Chi Minh City, Vietnam, 2016-2017
- Senior Doctor, Cardiovascular Resuscitation Unit, Tam Duc Heart Hospital, Ho Chi Minh City, Vietnam, 2017-2020
- Senior Physician, Cardiology Department, FV Hospital, from 2021', N'- Cardiovascular care
- Percutaneous coronary intervention
- Peripheral vascular intervention
- Percutaneous valvular regurgitation for regurgitation and mitral stenosis
- Septic and ductal malformations', N'Antibiotics treatments.', N'Certificate of Merit from the Minister of Health for individuals who have made excellent achievements in successfully performing 15 organ transplants on August 12 - August 20, 2019', 3, N'https://twitter.com/MaiBan25661905', N'https://www.facebook.com/boconganh.001/', N'https://www.linkedin.com/in/mai-ban-9b7a04244/', N'https://www.instagram.com/banmai201/')
INSERT [dbo].[Doctor] ([email], [position], [training_process], [working_process], [research_work], [scientific_research_topics], [bonus], [t_id], [twitter], [facebook], [linkedin], [instagram]) VALUES (N'thondhe@fpt.edu.vn', N'Medicine Deans', N'Hanoi Medical University, Hanoi, Vietnam, 2015', N'- Therapist, Cardiology Department, District 2 Hospital, Ho Chi Minh City, Vietnam, 2016-2017
- Senior Doctor, Cardiovascular Resuscitation Unit, Tam Duc Heart Hospital, Ho Chi Minh City, Vietnam, 2017-2020
- Senior Physician, Cardiology Department, FV Hospital, from 2021', N'- Cardiovascular care
- Percutaneous coronary intervention
- Peripheral vascular intervention
- Percutaneous valvular regurgitation for regurgitation and mitral stenosis
- Septic and ductal malformations', N'Antibiotics treatments.', N'Certificate of Merit from the Minister of Health for individuals who have made excellent achievements in successfully performing 15 organ transplants on August 12 - August 20, 2019', 4, N'https://twitter.com/MaiBan25661905', N'https://www.facebook.com/boconganh.001/', N'https://www.linkedin.com/in/mai-ban-9b7a04244/', N'https://www.instagram.com/banmai201/')
INSERT [dbo].[Doctor] ([email], [position], [training_process], [working_process], [research_work], [scientific_research_topics], [bonus], [t_id], [twitter], [facebook], [linkedin], [instagram]) VALUES (N'thondhe1150527@fpt.edu.vn', N'Medicine Deans', N'Hanoi Medical University, Hanoi, Vietnam, 2015', N'- Therapist, Cardiology Department, District 2 Hospital, Ho Chi Minh City, Vietnam, 2016-2017
- Senior Doctor, Cardiovascular Resuscitation Unit, Tam Duc Heart Hospital, Ho Chi Minh City, Vietnam, 2017-2020
- Senior Physician, Cardiology Department, FV Hospital, from 2021', N'- Cardiovascular care
- Percutaneous coronary intervention
- Peripheral vascular intervention
- Percutaneous valvular regurgitation for regurgitation and mitral stenosis
- Septic and ductal malformations', N'Antibiotics treatments.', N'Certificate of Merit from the Minister of Health for individuals who have made excellent achievements in successfully performing 15 organ transplants on August 12 - August 20, 2019', 1, N'https://twitter.com/MaiBan25661905', N'https://www.facebook.com/boconganh.001/', N'https://www.linkedin.com/in/mai-ban-9b7a04244/', N'https://www.instagram.com/banmai201/')
INSERT [dbo].[Doctor] ([email], [position], [training_process], [working_process], [research_work], [scientific_research_topics], [bonus], [t_id], [twitter], [facebook], [linkedin], [instagram]) VALUES (N'tungnguyenchi289@gmail.com', N'Medicine Deans', N'Hanoi Medical University, Hanoi, Vietnam, 2015', N'- Therapist, Cardiology Department, District 2 Hospital, Ho Chi Minh City, Vietnam, 2016-2017
- Senior Doctor, Cardiovascular Resuscitation Unit, Tam Duc Heart Hospital, Ho Chi Minh City, Vietnam, 2017-2020
- Senior Physician, Cardiology Department, FV Hospital, from 2021', N'- Cardiovascular care
- Percutaneous coronary intervention
- Peripheral vascular intervention
- Percutaneous valvular regurgitation for regurgitation and mitral stenosis
- Septic and ductal malformations', N'Antibiotics treatments.', N'Certificate of Merit from the Minister of Health for individuals who have made excellent achievements in successfully performing 15 organ transplants on August 12 - August 20, 2019', 2, N'https://twitter.com/MaiBan25661905', N'https://www.facebook.com/boconganh.001/', N'https://www.linkedin.com/in/mai-ban-9b7a04244/', N'https://www.instagram.com/banmai201/')
GO
INSERT [dbo].[exam_medicine] ([od_id], [medicine_id], [quantity], [prescription], [added_date]) VALUES (3, N'MDC00017            ', 1, N'g', CAST(N'2022-06-18' AS Date))
INSERT [dbo].[exam_medicine] ([od_id], [medicine_id], [quantity], [prescription], [added_date]) VALUES (11, N'MDC00016            ', 1, N'f', CAST(N'2022-06-18' AS Date))
INSERT [dbo].[exam_medicine] ([od_id], [medicine_id], [quantity], [prescription], [added_date]) VALUES (11, N'MDC00019            ', 1, N'f', CAST(N'2022-06-18' AS Date))
GO
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (1, N'AddFeedback', N'/feedback/add')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (2, N'EditFeedback', N'/feedback/edit')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (3, N'FeedbackListController', N'/feedback/list')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (4, N'PostController', N'/post')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (5, N'PostDetal', N'/postDetal')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (6, N'SlideController', N'/slideController')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (7, N'ChangePasswordController', N'/changePassword')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (8, N'EnterNewPass', N'/EnterNewPass')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (9, N'Login', N'/login')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (10, N'Logout', N'/logout')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (11, N'Registration', N'/Registration')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (12, N'ResetPass', N'/ResetPass')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (13, N'VerificationEmail', N'/verificationRegister')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (14, N'AddPostController', N'/manager/addpost')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (15, N'DeletePostController', N'/manager/deletepost')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (16, N'EditPostController', N'/manager/editpost')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (17, N'PostDetailController', N'/manager/postdetail')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (18, N'PostsListController', N'/manager/postslist')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (19, N'MyReservation', N'/myorder/list')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (20, N'ReservationContactController', N'/reservation/contact')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (21, N'ReservationController', N'/Reservation')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (22, N'ReservationDetail', N'/myorder/detail')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (23, N'reservationCompletion', N'/reservation/completion')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (24, N'AddService', N'/service/add')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (25, N'DoctorProfile', N'/DoctorProfile')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (26, N'EditService', N'/service/edit')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (27, N'ServiceDetail', N'/service/detail')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (28, N'ServiceDetailForUser', N'/servicedetail')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (29, N'ServiceForUser', N'/service')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (30, N'ServiceListController', N'/service/list')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (31, N'addCart', N'/addCart')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (32, N'UserDetailsController', N'/user/details')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (33, N'UserListController', N'/user/list')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (34, N'UserProfileController', N'/profile')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (35, N'UserSearchController', N'/user/search')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (36, N'SlideDetail', N'/slide')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (37, N'DeleteReservation', N'/deleteCart')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (38, N'Order complite', N'/Order')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (39, N'List all order ', N'/reservation/list')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (40, N'medical examination', N'/doctor/exam')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (41, N'ManageFeedback', N'/mfeedback/list')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (42, N'Manager Detail', N'/mfeedback/detail')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (43, N'MedicalControlForUser', N'/MedicalControlForUser')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (44, N'CustomersListController', N'/manager/customerslist')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (45, N'CustomerDetail', N'/manager/customerdetails')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (46, N'Add order by receptionist', N'/reservation/add')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (47, N'Order result customer ', N'/order/result')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (48, N'Medical list', N'/medicine/list')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (49, N'Medical add', N'/medicine/add')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (50, N'Medical delete', N'/medicine/delete')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (51, N'list doctor', N'/manager/doctor')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (52, N'DoctorDetail', N'/manager/doctorDetail')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (53, N'Edit Doctor', N'/manager/editDoctor')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (54, N'EditMedicine', N'/medicine/edit')
INSERT [dbo].[Feature] ([f_id], [f_name], [url]) VALUES (55, N'feedback not yet with ordetails have status is true', N'/notYetFeedback')
GO
SET IDENTITY_INSERT [dbo].[Feedback] ON 

INSERT [dbo].[Feedback] ([id], [photo], [star], [details], [s_id], [u_email], [o_id], [status]) VALUES (3, N'frontend\assert\img\service\FB_IMG_1551272243667.jpg', 3, N'Quao ,omaigot qua la mot dich vu tuyet voi', 1, N'mai', 2, 1)
INSERT [dbo].[Feedback] ([id], [photo], [star], [details], [s_id], [u_email], [o_id], [status]) VALUES (4, N'frontend\assert\img\service\FB_IMG_1551272243667.jpg', 4, N'Quao ,omaigot qua la mot dich vu tuyet voi', 2, N'mai', 2, 1)
INSERT [dbo].[Feedback] ([id], [photo], [star], [details], [s_id], [u_email], [o_id], [status]) VALUES (5, N'frontend\assert\img\service\FB_IMG_1551272243667.jpg', 5, N'Quao, Omai got', 3, N'thondhe2@fpt.edu.vn', 1, 0)
INSERT [dbo].[Feedback] ([id], [photo], [star], [details], [s_id], [u_email], [o_id], [status]) VALUES (6, N'frontend\assert\img\service\FB_IMG_1551272243667.jpg', 5, N'Quao, Omai got', 4, N'thondhe2@fpt.edu.vn', 1, 1)
INSERT [dbo].[Feedback] ([id], [photo], [star], [details], [s_id], [u_email], [o_id], [status]) VALUES (7, N'frontend\assert\img\service\FB_IMG_1551272243667.jpg', 5, N'Quao, Omai got', 5, N'thondhe2@fpt.edu.vn', 1, 1)
INSERT [dbo].[Feedback] ([id], [photo], [star], [details], [s_id], [u_email], [o_id], [status]) VALUES (8, N'frontend\assert\img\service\FB_IMG_1551272243667.jpg', 5, N'Quao, Omai got', 1, N'thondhe2@fpt.edu.vn', 1, 1)
INSERT [dbo].[Feedback] ([id], [photo], [star], [details], [s_id], [u_email], [o_id], [status]) VALUES (9, N'frontend\assert\img\service\FB_IMG_1551272243667.jpg', 5, N'Quao, Omai got', 2, N'thondhe2@fpt.edu.vn', 1, 1)
INSERT [dbo].[Feedback] ([id], [photo], [star], [details], [s_id], [u_email], [o_id], [status]) VALUES (10, N'frontend\assert\img\service\Pomodoro_Technique.jpg', 5, N'Quao, Omai got that la tuyet voi
', 3, N'sonnkhe151341@fpt.edu.vn', 4, 1)
INSERT [dbo].[Feedback] ([id], [photo], [star], [details], [s_id], [u_email], [o_id], [status]) VALUES (11, N'frontend/assert/img/service/about.jpg', 4, N'Quao, Omai got', 5, N'sonnkhe151341@fpt.edu.vn', 5, 1)
SET IDENTITY_INSERT [dbo].[Feedback] OFF
GO
SET IDENTITY_INSERT [dbo].[Medical_record] ON 

INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (2, N'sadf a ', 1, N'asd as', NULL, NULL, NULL, NULL, N'aa', N'0349019012', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (3, N'd asd', 1, N' asd', NULL, NULL, NULL, NULL, N'aa', N'0349019012', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (7, N'asd as da', 1, N' asd  as', NULL, NULL, NULL, NULL, N'abc', N'0349019012', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (8, N' asd asd', 1, N' asd asd', NULL, NULL, NULL, NULL, N'aa', N'0349019012', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (9, N'asdf as d', 1, N'asd ', NULL, NULL, NULL, NULL, N'aa', N'0349019012', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (10, N'asd a sd', 1, N' asd ', NULL, NULL, NULL, NULL, N'aa', N'0349019012', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (11, N'1', 1, N'1', NULL, N'medicalHistory', N'f', NULL, N'aa', N'0349019012', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (12, N'b  b b', 1, N' b b', NULL, NULL, NULL, NULL, N' b b ', N'0349019012', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (13, N' bb b ', 1, N' b bb  b ', NULL, NULL, NULL, NULL, N'b b b', N'0349019012', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (14, N'b  b b', 1, N' b b', NULL, NULL, NULL, NULL, N' b b ', N'0349019012', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (15, N' bb b ', 1, N' b bb  b ', NULL, NULL, NULL, NULL, N'b b b', N'0349019012', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (16, N'Dao Anh Hien', 1, N'Ha noi', NULL, NULL, NULL, NULL, N'Xo mui', N'0349019012', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (17, N'adfas a', 1, N' asd ', NULL, NULL, NULL, NULL, N'as d', NULL, NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (18, N'asdf ', 1, N'ad ', NULL, NULL, NULL, NULL, N'asd asd', N'0349019012', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (19, N'a sd ', 1, N'asd ', NULL, NULL, NULL, NULL, N'asd asd', N'0349019012', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (20, N'nnnn  nn ', 1, N'nnn', NULL, NULL, NULL, NULL, N'nnn', N'0349019012', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (21, N'nnn n', 1, N' nnn', NULL, NULL, NULL, NULL, N'nn n', N'0349019012', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (22, N's sd', 1, N'f sdf ', NULL, NULL, NULL, NULL, N'f sdf ', N'0349019012', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (23, N'd  sd ', 1, N'f sd s', NULL, NULL, NULL, NULL, N'sd ', N'0349019012', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (24, N'yy y y', 1, N'yy y', NULL, NULL, NULL, NULL, N' y yy ', N'0349019012', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (25, N' yyy  y y', 1, N' yyyy y y', NULL, NULL, NULL, NULL, N'y yy y', N'0349019012', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (26, N'bbb b b', 1, N' bb b', NULL, NULL, NULL, NULL, N' b bb b', N'0349019012', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (27, N'bb bb ', 1, N' b bb b ', NULL, NULL, NULL, NULL, N'b bb b', N'0349019012', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (28, N'ttt tt t', 1, N' tt ', NULL, NULL, NULL, NULL, N' ttt', N'0349019012', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (29, N't tt t', 1, N' tt t t ', NULL, NULL, NULL, NULL, N' t t tt', N'0349019012', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (30, N'tho ok', 1, N'asdf as', NULL, NULL, NULL, NULL, N'asd asd ', N'd asd ', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (31, N'tho ok', 1, N'a sd', NULL, NULL, NULL, NULL, N' asd aasd ', N'a sd', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (32, N'th o', 1, N'th o', NULL, NULL, NULL, NULL, N'a th o', N'0349019012', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (33, N'th o', 1, N'th o', NULL, NULL, NULL, NULL, N'th o', N'0349019012', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (34, N'asdfa sd', 1, N'a  a ', NULL, NULL, NULL, NULL, N' aa', N'0349019012', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (35, N' asd asd', 1, N'd asd', NULL, NULL, NULL, NULL, N'a sa sa s', N'0349019012', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (36, N'a a ', 1, N' a a', NULL, NULL, NULL, NULL, N' a a', N'0349019012', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (37, N'a a ', 1, N'a a ', NULL, NULL, NULL, NULL, N'a a ', N'0349019012', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (38, N'a a a', 1, N'a a ', NULL, NULL, NULL, NULL, N'a a ', N'0349019012', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (39, N'a  aa', 1, N'a  aa ', NULL, NULL, NULL, NULL, N'a a ', N'0349019012', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (40, N'Tho nguyen', 1, N'ha noi', NULL, NULL, NULL, NULL, N'dau bung', N'0349019012', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (41, N'Tung nguyen ', 1, N'ha noi', NULL, NULL, NULL, NULL, N'bi om', N'0989373890', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (42, N'aa a a', 1, N'aa a ', NULL, NULL, NULL, NULL, N'a a a', N'0349019012', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (43, N'a aa ', 1, N'a a a', NULL, NULL, NULL, NULL, N' a a ', N'0349019012', NULL)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (1043, N'Linh Chi', 0, N'Nghe an', NULL, NULL, NULL, NULL, N'Xo mui', N'0349019012', 3)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (1044, N'Linh Chiii', 1, N'My luong', NULL, NULL, NULL, NULL, N'Xo mui', N'0349019012', 12)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (1045, N'HIii hiiii', 1, N'a  a', NULL, NULL, NULL, NULL, N' aa', N'0349019012', 123)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (1046, N'Cai Thao', 1, N'a a a', NULL, NULL, NULL, NULL, N'Xo mui', N'0349019012', 12)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (1047, N'Ci CI', 1, N'Xu nghe', NULL, NULL, NULL, NULL, N'at xi hoiii', N'0349019012', 3)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (1048, N'Ci CI', 1, N'Xu nghe', NULL, NULL, NULL, NULL, N'at xi hoiii', N'0349019012', 3)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (1049, N'Ci CI', 1, N'Xu nghe', NULL, NULL, NULL, NULL, N'at xi hoiii', N'0349019012', 3)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (1050, N'Ci CI', 1, N'Xu nghe', NULL, NULL, NULL, NULL, N'at xi hoiii', N'0349019012', 3)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (1051, N'Hi hi', 1, N'a a', NULL, NULL, NULL, NULL, N'Xo mui', N'0349019012', 13)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (1052, N'Thodz a', 1, N'My luong', NULL, NULL, NULL, NULL, N'Xo mui', N'0349019012', 3)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (1053, N'test again', 1, N'My luong', NULL, NULL, NULL, NULL, N'a a a', N'0349019012', 1)
INSERT [dbo].[Medical_record] ([mr_id], [full_name], [gender], [address], [date_of_examination], [medical_history], [prehistory], [first_exam_service], [reason_for_hospitalization], [phone], [age]) VALUES (1054, N'Test test test ', 1, N'My luong', NULL, NULL, NULL, NULL, N'Xo mui', N'0349019012', 12)
SET IDENTITY_INSERT [dbo].[Medical_record] OFF
GO
INSERT [dbo].[medicine] ([id], [name], [producer], [element], [quantity], [inUse], [price], [unit]) VALUES (N'MDC00001            ', N'Acetylcystein', N'Group 4 Company', N'magnesisi', 1000, 1, 1.5000, N'Tablets')
INSERT [dbo].[medicine] ([id], [name], [producer], [element], [quantity], [inUse], [price], [unit]) VALUES (N'MDC00002            ', N'Acetylleucin', N'Group 4 Company', N'magnesisi', 1000, 1, 1.5000, N'Tablets')
INSERT [dbo].[medicine] ([id], [name], [producer], [element], [quantity], [inUse], [price], [unit]) VALUES (N'MDC00003            ', N'Acid acetylsalicylic ', N'Group 4 Company', N'magnesisi', 1000, 1, 1.5000, N'Tablets')
INSERT [dbo].[medicine] ([id], [name], [producer], [element], [quantity], [inUse], [price], [unit]) VALUES (N'MDC00004            ', N'Acid alginic', N'Group 4 Company', N'magnesisi', 1000, 1, 1.5000, N'Tablets')
INSERT [dbo].[medicine] ([id], [name], [producer], [element], [quantity], [inUse], [price], [unit]) VALUES (N'MDC00005            ', N'Acid aminobenzoic', N'Group 4 Company', N'magnesisi', 1000, 1, 1.5000, N'Tablets')
INSERT [dbo].[medicine] ([id], [name], [producer], [element], [quantity], [inUse], [price], [unit]) VALUES (N'MDC00006            ', N'Acid cromoglicic', N'Group 4 Company', N'magnesisi', 1000, 1, 1.5000, N'Tablets')
INSERT [dbo].[medicine] ([id], [name], [producer], [element], [quantity], [inUse], [price], [unit]) VALUES (N'MDC00007            ', N'Acid dimecrotic', N'Group 4 Company', N'magnesisi', 1000, 1, 1.5000, N'Tablets')
INSERT [dbo].[medicine] ([id], [name], [producer], [element], [quantity], [inUse], [price], [unit]) VALUES (N'MDC00008            ', N'Acid glycyrrhizinic', N'Group 4 Company', N'magnesisi', 1000, 1, 1.5000, N'Tablets')
INSERT [dbo].[medicine] ([id], [name], [producer], [element], [quantity], [inUse], [price], [unit]) VALUES (N'MDC00009            ', N'Acid lactic', N'Group 4 Company', N'magnesisi', 1000, 1, 1.5000, N'Tablets')
INSERT [dbo].[medicine] ([id], [name], [producer], [element], [quantity], [inUse], [price], [unit]) VALUES (N'MDC00010            ', N'Acid mefenamic', N'Group 4 Company', N'magnesisi', 1000, 1, 1.5000, N'Tablets')
INSERT [dbo].[medicine] ([id], [name], [producer], [element], [quantity], [inUse], [price], [unit]) VALUES (N'MDC00011            ', N'Acyclovir', N'Group 4 Company', N'magnesisi', 1000, 1, 1.5000, N'Tablets')
INSERT [dbo].[medicine] ([id], [name], [producer], [element], [quantity], [inUse], [price], [unit]) VALUES (N'MDC00012            ', N'Albendazol', N'Group 4 Company', N'magnesisi', 1000, 1, 1.5000, N'Tablets')
INSERT [dbo].[medicine] ([id], [name], [producer], [element], [quantity], [inUse], [price], [unit]) VALUES (N'MDC00013            ', N'Alimemazin tartrat', N'Group 4 Company', N'magnesisi', 1000, 1, 1.5000, N'Tablets')
INSERT [dbo].[medicine] ([id], [name], [producer], [element], [quantity], [inUse], [price], [unit]) VALUES (N'MDC00014            ', N'Aspartam', N'Group 4 Company', N'magnesisi', 1000, 1, 1.5000, N'Tablets')
INSERT [dbo].[medicine] ([id], [name], [producer], [element], [quantity], [inUse], [price], [unit]) VALUES (N'MDC00015            ', N'Beclomethason dipropionat', N'Group 4 Company', N'magnesisi', 1000, 1, 1.5000, N'Tablets')
INSERT [dbo].[medicine] ([id], [name], [producer], [element], [quantity], [inUse], [price], [unit]) VALUES (N'MDC00016            ', N'Benzalkonium ', N'Group 4 Company', N'magnesisi', 1000, 1, 1.5000, N'Tablets')
INSERT [dbo].[medicine] ([id], [name], [producer], [element], [quantity], [inUse], [price], [unit]) VALUES (N'MDC00017            ', N'Benzydamin Hydrochlorid', N'Group 4 Company', N'magnesisi', 1000, 1, 1.5000, N'Tablets')
INSERT [dbo].[medicine] ([id], [name], [producer], [element], [quantity], [inUse], [price], [unit]) VALUES (N'MDC00018            ', N'Bisacodyl', N'Group 4 Company', N'magnesisi', 1000, 1, 1.5000, N'Tablets')
INSERT [dbo].[medicine] ([id], [name], [producer], [element], [quantity], [inUse], [price], [unit]) VALUES (N'MDC00019            ', N'Budesonid', N'Group 4 Company', N'magnesisi', 1000, 1, 1.5000, N'Tablets')
GO
SET IDENTITY_INSERT [dbo].[Order] ON 

INSERT [dbo].[Order] ([o_id], [email_customer], [dateOrdered], [totalCost], [status]) VALUES (1, N'thondhe2@fpt.edu.vn', CAST(N'2022-06-12' AS Date), 20000.0000, 1)
INSERT [dbo].[Order] ([o_id], [email_customer], [dateOrdered], [totalCost], [status]) VALUES (2, N'sonnkhe151341@fpt.edu.vn', CAST(N'2022-07-12' AS Date), 2000.0000, 1)
INSERT [dbo].[Order] ([o_id], [email_customer], [dateOrdered], [totalCost], [status]) VALUES (3, N'thondhe2@fpt.edu.vn', CAST(N'2022-06-13' AS Date), 400000.0000, 0)
INSERT [dbo].[Order] ([o_id], [email_customer], [dateOrdered], [totalCost], [status]) VALUES (4, N'sonnkhe151341@fpt.edu.vn', CAST(N'2021-05-24' AS Date), 1500000.0000, 1)
INSERT [dbo].[Order] ([o_id], [email_customer], [dateOrdered], [totalCost], [status]) VALUES (5, N'sonnkhe151341@fpt.edu.vn', CAST(N'2021-12-12' AS Date), 250000.0000, 0)
INSERT [dbo].[Order] ([o_id], [email_customer], [dateOrdered], [totalCost], [status]) VALUES (6, N'sonnkhe151341@fpt.edu.vn', CAST(N'2022-07-04' AS Date), 60000.0000, 0)
INSERT [dbo].[Order] ([o_id], [email_customer], [dateOrdered], [totalCost], [status]) VALUES (7, N'sonnkhe151341@fpt.edu.vn', CAST(N'2022-07-04' AS Date), 60000.0000, 0)
INSERT [dbo].[Order] ([o_id], [email_customer], [dateOrdered], [totalCost], [status]) VALUES (10, NULL, CAST(N'2022-07-04' AS Date), 10000.0000, 0)
INSERT [dbo].[Order] ([o_id], [email_customer], [dateOrdered], [totalCost], [status]) VALUES (11, N'sonnkhe151341@fpt.edu.vn', CAST(N'2022-07-07' AS Date), 60000.0000, 0)
INSERT [dbo].[Order] ([o_id], [email_customer], [dateOrdered], [totalCost], [status]) VALUES (12, N'sonnkhe151341@fpt.edu.vn', CAST(N'2022-07-07' AS Date), 60000.0000, 0)
INSERT [dbo].[Order] ([o_id], [email_customer], [dateOrdered], [totalCost], [status]) VALUES (13, N'sonnkhe151341@fpt.edu.vn', CAST(N'2022-07-07' AS Date), 60000.0000, 0)
INSERT [dbo].[Order] ([o_id], [email_customer], [dateOrdered], [totalCost], [status]) VALUES (14, N'sonnkhe151341@fpt.edu.vn', CAST(N'2022-07-09' AS Date), 40000.0000, 0)
INSERT [dbo].[Order] ([o_id], [email_customer], [dateOrdered], [totalCost], [status]) VALUES (15, N'sonnkhe151341@fpt.edu.vn', CAST(N'2022-07-09' AS Date), 40000.0000, 0)
INSERT [dbo].[Order] ([o_id], [email_customer], [dateOrdered], [totalCost], [status]) VALUES (16, N'sonnkhe151341@fpt.edu.vn', CAST(N'2022-07-09' AS Date), 40000.0000, 0)
INSERT [dbo].[Order] ([o_id], [email_customer], [dateOrdered], [totalCost], [status]) VALUES (17, N'sonnkhe151341@fpt.edu.vn', CAST(N'2022-07-09' AS Date), 40000.0000, 0)
INSERT [dbo].[Order] ([o_id], [email_customer], [dateOrdered], [totalCost], [status]) VALUES (18, N'mai6@gmail.com', CAST(N'2022-07-11' AS Date), 40000.0000, 0)
INSERT [dbo].[Order] ([o_id], [email_customer], [dateOrdered], [totalCost], [status]) VALUES (19, N'mai2@gmail.com', CAST(N'2022-07-11' AS Date), 80000.0000, 0)
INSERT [dbo].[Order] ([o_id], [email_customer], [dateOrdered], [totalCost], [status]) VALUES (20, N'mai2@gmail.com', CAST(N'2022-07-11' AS Date), 20000.0000, 0)
INSERT [dbo].[Order] ([o_id], [email_customer], [dateOrdered], [totalCost], [status]) VALUES (21, N'mai2@gmail.com', CAST(N'2022-07-11' AS Date), 20000.0000, 0)
INSERT [dbo].[Order] ([o_id], [email_customer], [dateOrdered], [totalCost], [status]) VALUES (22, N'mai2@gmail.com', CAST(N'2022-07-12' AS Date), 60000.0000, 0)
INSERT [dbo].[Order] ([o_id], [email_customer], [dateOrdered], [totalCost], [status]) VALUES (23, N'sonnkhe151341@fpt.edu.vn', CAST(N'2022-07-12' AS Date), 40000.0000, 0)
INSERT [dbo].[Order] ([o_id], [email_customer], [dateOrdered], [totalCost], [status]) VALUES (24, N'mai3@gmail.com', CAST(N'2022-07-18' AS Date), 80000.0000, 0)
INSERT [dbo].[Order] ([o_id], [email_customer], [dateOrdered], [totalCost], [status]) VALUES (25, N'mai2@gmail.com', CAST(N'2022-07-18' AS Date), 40000.0000, 0)
INSERT [dbo].[Order] ([o_id], [email_customer], [dateOrdered], [totalCost], [status]) VALUES (26, N'sonnkhe151341@fpt.edu.vn', CAST(N'2022-07-20' AS Date), 40000.0000, 0)
INSERT [dbo].[Order] ([o_id], [email_customer], [dateOrdered], [totalCost], [status]) VALUES (1026, NULL, CAST(N'2022-07-25' AS Date), 20000.0000, 0)
INSERT [dbo].[Order] ([o_id], [email_customer], [dateOrdered], [totalCost], [status]) VALUES (1027, NULL, CAST(N'2022-07-25' AS Date), 40000.0000, 0)
INSERT [dbo].[Order] ([o_id], [email_customer], [dateOrdered], [totalCost], [status]) VALUES (1028, NULL, CAST(N'2022-07-25' AS Date), 40000.0000, 0)
INSERT [dbo].[Order] ([o_id], [email_customer], [dateOrdered], [totalCost], [status]) VALUES (1029, NULL, CAST(N'2022-07-25' AS Date), 40000.0000, 0)
INSERT [dbo].[Order] ([o_id], [email_customer], [dateOrdered], [totalCost], [status]) VALUES (1030, NULL, CAST(N'2022-07-25' AS Date), 40000.0000, 0)
INSERT [dbo].[Order] ([o_id], [email_customer], [dateOrdered], [totalCost], [status]) VALUES (1031, NULL, CAST(N'2022-07-25' AS Date), 40000.0000, 0)
INSERT [dbo].[Order] ([o_id], [email_customer], [dateOrdered], [totalCost], [status]) VALUES (1032, NULL, CAST(N'2022-07-25' AS Date), 40000.0000, 0)
INSERT [dbo].[Order] ([o_id], [email_customer], [dateOrdered], [totalCost], [status]) VALUES (1033, NULL, CAST(N'2022-07-25' AS Date), 40000.0000, 0)
INSERT [dbo].[Order] ([o_id], [email_customer], [dateOrdered], [totalCost], [status]) VALUES (1034, NULL, CAST(N'2022-07-25' AS Date), 40000.0000, 0)
INSERT [dbo].[Order] ([o_id], [email_customer], [dateOrdered], [totalCost], [status]) VALUES (1035, NULL, CAST(N'2022-07-25' AS Date), 60000.0000, 0)
SET IDENTITY_INSERT [dbo].[Order] OFF
GO
SET IDENTITY_INSERT [dbo].[Order_detail] ON 

INSERT [dbo].[Order_detail] ([od_id], [o_id], [s_id], [email_doctor], [examineDate], [exam_Status], [note], [price], [checkOutDate], [medical_Status], [mr_id], [symptom], [ordinal_number]) VALUES (1, 1, 1, N'tungnguyenchi289@gmail.com', CAST(N'2022-07-08' AS Date), 1, N'abc', 10000.0000, CAST(N'2022-06-12T00:00:00' AS SmallDateTime), 0, 7, N'abc', NULL)
INSERT [dbo].[Order_detail] ([od_id], [o_id], [s_id], [email_doctor], [examineDate], [exam_Status], [note], [price], [checkOutDate], [medical_Status], [mr_id], [symptom], [ordinal_number]) VALUES (2, 1, 2, N'tungnguyenchi289@gmail.com', CAST(N'2022-07-08' AS Date), 1, N'bcd', 20000.0000, CAST(N'2022-06-12T00:00:00' AS SmallDateTime), 0, 7, N'abc', NULL)
INSERT [dbo].[Order_detail] ([od_id], [o_id], [s_id], [email_doctor], [examineDate], [exam_Status], [note], [price], [checkOutDate], [medical_Status], [mr_id], [symptom], [ordinal_number]) VALUES (3, 1, 3, N'tungnguyenchi289@gmail.com', CAST(N'2022-07-08' AS Date), 1, N'cde', 150000.0000, CAST(N'2022-06-12T00:00:00' AS SmallDateTime), 1, 8, N'abc', NULL)
INSERT [dbo].[Order_detail] ([od_id], [o_id], [s_id], [email_doctor], [examineDate], [exam_Status], [note], [price], [checkOutDate], [medical_Status], [mr_id], [symptom], [ordinal_number]) VALUES (4, 2, 1, N'tungnguyenchi289@gmail.com', CAST(N'2022-06-12' AS Date), 1, N'abc', 1000.0000, CAST(N'2022-06-12T00:00:00' AS SmallDateTime), 0, 8, N'abc', NULL)
INSERT [dbo].[Order_detail] ([od_id], [o_id], [s_id], [email_doctor], [examineDate], [exam_Status], [note], [price], [checkOutDate], [medical_Status], [mr_id], [symptom], [ordinal_number]) VALUES (5, 2, 2, N'tungnguyenchi289@gmail.com', CAST(N'2022-06-14' AS Date), 1, N'ksạkdj', 120000.0000, CAST(N'2022-06-14T00:00:00' AS SmallDateTime), 0, 10, N'abc', NULL)
INSERT [dbo].[Order_detail] ([od_id], [o_id], [s_id], [email_doctor], [examineDate], [exam_Status], [note], [price], [checkOutDate], [medical_Status], [mr_id], [symptom], [ordinal_number]) VALUES (6, 4, 3, N'tungnguyenchi289@gmail.com', CAST(N'2022-06-15' AS Date), 1, N'kokokok', 30000.0000, CAST(N'2022-06-15T00:00:00' AS SmallDateTime), 0, 10, N'abc', NULL)
INSERT [dbo].[Order_detail] ([od_id], [o_id], [s_id], [email_doctor], [examineDate], [exam_Status], [note], [price], [checkOutDate], [medical_Status], [mr_id], [symptom], [ordinal_number]) VALUES (7, 5, 5, N'anhvhhs153214@fpt.edu.vn', CAST(N'2022-06-15' AS Date), 0, N'kekekekek', 20000.0000, CAST(N'2022-06-12T00:00:00' AS SmallDateTime), 0, 12, N'abc', NULL)
INSERT [dbo].[Order_detail] ([od_id], [o_id], [s_id], [email_doctor], [examineDate], [exam_Status], [note], [price], [checkOutDate], [medical_Status], [mr_id], [symptom], [ordinal_number]) VALUES (8, 1, 4, N'tungnguyenchi289@gmail.com', CAST(N'2022-07-08' AS Date), 1, N'oke', 1000.0000, CAST(N'2022-06-15T00:00:00' AS SmallDateTime), 0, 12, N'abc', NULL)
INSERT [dbo].[Order_detail] ([od_id], [o_id], [s_id], [email_doctor], [examineDate], [exam_Status], [note], [price], [checkOutDate], [medical_Status], [mr_id], [symptom], [ordinal_number]) VALUES (10, 1, 5, N'tungnguyenchi289@gmail.com', CAST(N'2022-07-08' AS Date), 1, N'ok', 1000.0000, CAST(N'2022-06-15T00:00:00' AS SmallDateTime), 0, 11, N'abc', NULL)
INSERT [dbo].[Order_detail] ([od_id], [o_id], [s_id], [email_doctor], [examineDate], [exam_Status], [note], [price], [checkOutDate], [medical_Status], [mr_id], [symptom], [ordinal_number]) VALUES (11, 10, 1, N'tungnguyenchi289@gmail.com', CAST(N'2022-06-12' AS Date), 1, N'ok', 1000.0000, CAST(N'2022-06-15T00:00:00' AS SmallDateTime), 1, 16, N'abc', NULL)
INSERT [dbo].[Order_detail] ([od_id], [o_id], [s_id], [email_doctor], [examineDate], [exam_Status], [note], [price], [checkOutDate], [medical_Status], [mr_id], [symptom], [ordinal_number]) VALUES (12, 22, 1, N'anhvhhs153214@fpt.edu.vn', CAST(N'2022-07-13' AS Date), 0, NULL, 20000.0000, NULL, 0, 36, N' aa ', 1)
INSERT [dbo].[Order_detail] ([od_id], [o_id], [s_id], [email_doctor], [examineDate], [exam_Status], [note], [price], [checkOutDate], [medical_Status], [mr_id], [symptom], [ordinal_number]) VALUES (13, 22, 2, N'anhvhhs153214@fpt.edu.vn', CAST(N'2022-07-12' AS Date), 0, NULL, 20000.0000, NULL, 0, 36, N' aa a ', -1)
INSERT [dbo].[Order_detail] ([od_id], [o_id], [s_id], [email_doctor], [examineDate], [exam_Status], [note], [price], [checkOutDate], [medical_Status], [mr_id], [symptom], [ordinal_number]) VALUES (14, 22, 2, N'anhvhhs153214@fpt.edu.vn', CAST(N'2022-07-13' AS Date), 0, NULL, 20000.0000, NULL, 0, 37, N'a  a a', -1)
INSERT [dbo].[Order_detail] ([od_id], [o_id], [s_id], [email_doctor], [examineDate], [exam_Status], [note], [price], [checkOutDate], [medical_Status], [mr_id], [symptom], [ordinal_number]) VALUES (15, 23, 2, N'anhvhhs153214@fpt.edu.vn', CAST(N'2022-06-15' AS Date), 0, NULL, 20000.0000, NULL, 0, 39, N'a a a ', 1)
INSERT [dbo].[Order_detail] ([od_id], [o_id], [s_id], [email_doctor], [examineDate], [exam_Status], [note], [price], [checkOutDate], [medical_Status], [mr_id], [symptom], [ordinal_number]) VALUES (16, 23, 3, N'anhvhhs153214@fpt.edu.vn', CAST(N'2022-06-15' AS Date), 0, NULL, 20000.0000, NULL, 0, 38, N'a a a a', -1)
INSERT [dbo].[Order_detail] ([od_id], [o_id], [s_id], [email_doctor], [examineDate], [exam_Status], [note], [price], [checkOutDate], [medical_Status], [mr_id], [symptom], [ordinal_number]) VALUES (17, 24, 1, N'anhvhhs153214@fpt.edu.vn', CAST(N'2022-07-19' AS Date), 0, NULL, 20000.0000, NULL, 0, 41, N'av cc ', 1)
INSERT [dbo].[Order_detail] ([od_id], [o_id], [s_id], [email_doctor], [examineDate], [exam_Status], [note], [price], [checkOutDate], [medical_Status], [mr_id], [symptom], [ordinal_number]) VALUES (18, 24, 3, N'anhvhhs153214@fpt.edu.vn', CAST(N'2022-07-19' AS Date), 0, NULL, 20000.0000, NULL, 0, 41, N'aa a a', -1)
INSERT [dbo].[Order_detail] ([od_id], [o_id], [s_id], [email_doctor], [examineDate], [exam_Status], [note], [price], [checkOutDate], [medical_Status], [mr_id], [symptom], [ordinal_number]) VALUES (19, 24, 4, N'anhvhhs153214@fpt.edu.vn', CAST(N'2022-07-19' AS Date), 0, NULL, 20000.0000, NULL, 0, 40, N' aa ', -1)
INSERT [dbo].[Order_detail] ([od_id], [o_id], [s_id], [email_doctor], [examineDate], [exam_Status], [note], [price], [checkOutDate], [medical_Status], [mr_id], [symptom], [ordinal_number]) VALUES (20, 24, 6, N'anhvhhs153214@fpt.edu.vn', CAST(N'2022-07-19' AS Date), 0, NULL, 20000.0000, NULL, 0, 40, N'a a a', -1)
INSERT [dbo].[Order_detail] ([od_id], [o_id], [s_id], [email_doctor], [examineDate], [exam_Status], [note], [price], [checkOutDate], [medical_Status], [mr_id], [symptom], [ordinal_number]) VALUES (21, 25, 2, N'anhvhhs153214@fpt.edu.vn', CAST(N'2022-07-13' AS Date), 0, NULL, 20000.0000, NULL, 0, 42, N'a a a', 2)
INSERT [dbo].[Order_detail] ([od_id], [o_id], [s_id], [email_doctor], [examineDate], [exam_Status], [note], [price], [checkOutDate], [medical_Status], [mr_id], [symptom], [ordinal_number]) VALUES (22, 25, 1, N'anhvhhs153214@fpt.edu.vn', CAST(N'2022-07-19' AS Date), 0, NULL, 20000.0000, NULL, 0, 42, N' a a ', -1)
INSERT [dbo].[Order_detail] ([od_id], [o_id], [s_id], [email_doctor], [examineDate], [exam_Status], [note], [price], [checkOutDate], [medical_Status], [mr_id], [symptom], [ordinal_number]) VALUES (23, 26, 4, N'anhvhhs153214@fpt.edu.vn', CAST(N'2022-06-30' AS Date), 0, NULL, 20000.0000, NULL, 0, 43, N'ok ', 1)
INSERT [dbo].[Order_detail] ([od_id], [o_id], [s_id], [email_doctor], [examineDate], [exam_Status], [note], [price], [checkOutDate], [medical_Status], [mr_id], [symptom], [ordinal_number]) VALUES (24, 26, 1, N'anhvhhs153214@fpt.edu.vn', CAST(N'2022-07-13' AS Date), 0, NULL, 20000.0000, NULL, 0, 43, N'ok ', -1)
INSERT [dbo].[Order_detail] ([od_id], [o_id], [s_id], [email_doctor], [examineDate], [exam_Status], [note], [price], [checkOutDate], [medical_Status], [mr_id], [symptom], [ordinal_number]) VALUES (1027, 1031, 1, N'anhvhhs153214@fpt.edu.vn', CAST(N'2022-07-25' AS Date), 0, NULL, 20000.0000, NULL, 0, 1050, N'a a a', -1)
INSERT [dbo].[Order_detail] ([od_id], [o_id], [s_id], [email_doctor], [examineDate], [exam_Status], [note], [price], [checkOutDate], [medical_Status], [mr_id], [symptom], [ordinal_number]) VALUES (1028, 1031, 2, N'anhvhhs153214@fpt.edu.vn', CAST(N'2022-07-25' AS Date), 0, NULL, 20000.0000, NULL, 0, 1050, N'a a a', -1)
INSERT [dbo].[Order_detail] ([od_id], [o_id], [s_id], [email_doctor], [examineDate], [exam_Status], [note], [price], [checkOutDate], [medical_Status], [mr_id], [symptom], [ordinal_number]) VALUES (1029, 1032, 1, N'anhvhhs153214@fpt.edu.vn', CAST(N'2022-07-25' AS Date), 0, NULL, 20000.0000, NULL, 0, 1051, N'aa a ', -1)
INSERT [dbo].[Order_detail] ([od_id], [o_id], [s_id], [email_doctor], [examineDate], [exam_Status], [note], [price], [checkOutDate], [medical_Status], [mr_id], [symptom], [ordinal_number]) VALUES (1030, 1032, 2, N'anhvhhs153214@fpt.edu.vn', CAST(N'2022-07-25' AS Date), 0, NULL, 20000.0000, NULL, 0, 1051, N'aa a ', -1)
INSERT [dbo].[Order_detail] ([od_id], [o_id], [s_id], [email_doctor], [examineDate], [exam_Status], [note], [price], [checkOutDate], [medical_Status], [mr_id], [symptom], [ordinal_number]) VALUES (1031, 1033, 1, N'anhvhhs153214@fpt.edu.vn', CAST(N'2022-07-25' AS Date), 0, NULL, 20000.0000, NULL, 0, 1052, N'a a ', -1)
INSERT [dbo].[Order_detail] ([od_id], [o_id], [s_id], [email_doctor], [examineDate], [exam_Status], [note], [price], [checkOutDate], [medical_Status], [mr_id], [symptom], [ordinal_number]) VALUES (1032, 1033, 2, N'anhvhhs153214@fpt.edu.vn', CAST(N'2022-07-25' AS Date), 0, NULL, 20000.0000, NULL, 0, 1052, N'a a ', -1)
INSERT [dbo].[Order_detail] ([od_id], [o_id], [s_id], [email_doctor], [examineDate], [exam_Status], [note], [price], [checkOutDate], [medical_Status], [mr_id], [symptom], [ordinal_number]) VALUES (1033, 1034, 1, N'anhvhhs153214@fpt.edu.vn', CAST(N'2022-07-25' AS Date), 0, NULL, 20000.0000, NULL, 0, 1053, N'aa a', -1)
INSERT [dbo].[Order_detail] ([od_id], [o_id], [s_id], [email_doctor], [examineDate], [exam_Status], [note], [price], [checkOutDate], [medical_Status], [mr_id], [symptom], [ordinal_number]) VALUES (1034, 1034, 3, N'anhvhhs153214@fpt.edu.vn', CAST(N'2022-07-25' AS Date), 0, NULL, 20000.0000, NULL, 0, 1053, N'aa a', -1)
SET IDENTITY_INSERT [dbo].[Order_detail] OFF
GO
SET IDENTITY_INSERT [dbo].[Post] ON 

INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (3, N'Healthy Diet', N'A healthy diet is essential for good health and nutrition.

It protects you against many chronic noncommunicable diseases, such as heart disease, diabetes and cancer. Eating a variety of foods and consuming less salt, sugars and saturated and industrially-produced trans-fats, are essential for healthy diet.

A healthy diet comprises a combination of different foods. These include:

Staples like cereals (wheat, barley, rye, maize or rice) or starchy tubers or roots (potato, yam, taro or cassava).
Legumes (lentils and beans).
Fruit and vegetables.
Foods from animal sources (meat, fish, eggs and milk).
Here is some useful information, based on WHO recommendations, to follow a healthy diet, and the benefits of doing so.

Breastfeed babies and young children:
A healthy diet starts early in life - breastfeeding fosters healthy growth, and may have longer-term health benefits, like reducing the risk of becoming overweight or obese and developing noncommunicable diseases later in life.
Feeding babies exclusively with breast milk from birth to 6 months of life is important for a healthy diet. It is also important to introduce a variety of safe and nutritious complementary foods at 6 months of age, while continuing to breastfeed until your child is two years old and beyond.
 

Eat plenty of vegetables and fruit:
They are important sources of vitamins, minerals, dietary fibre, plant protein and antioxidants.
People with diets rich in vegetables and fruit have a significantly lower risk of obesity, heart disease, stroke, diabetes and certain types of cancer.
 

Eat less fat:
Fats and oils and concentrated sources of energy. Eating too much, particularly the wrong kinds of fat, like saturated and industrially-produced trans-fat, can increase the risk of heart disease and stroke.
Using unsaturated vegetable oils (olive, soy, sunflower or corn oil) rather than animal fats or oils high in saturated fats (butter, ghee, lard, coconut and palm oil) will help consume healthier fats.
To avoid unhealthy weight gain, consumption of total fat should not exceed 30% of a person''s overall energy intake.
 

Limit intake of sugars:
For a healthy diet, sugars should represent less than 10% of your total energy intake. Reducing even further to under 5% has additional health benefits.
Choosing fresh fruits instead of sweet snacks such as cookies, cakes and chocolate helps reduce consumption of sugars.
Limiting intake of soft drinks, soda and other drinks high in sugars (fruit juices, cordials and syrups, flavoured milks and yogurt drinks) also helps reduce intake of sugars.
 

Reduce salt intake:
Keeping your salt intake to less than 5h per day helps prevent hypertension and reduces the risk of heart disease and stroke in the adult population.
Limiting the amount of salt and high-sodium condiments (soy sauce and fish sauce) when cooking and preparing foods helps reduce salt intake.', N'post/Healthy-Food-e1635355055520.gif                                                                                                                                                                                                                                                                        ', CAST(N'2022-05-19T16:57:00' AS SmallDateTime), 1, 1, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (4, N'Looking for a way to boost your immune system this cold & flu season?', N'We could all benefit from a more proactive rather than reactive approach to health, especially when it comes to our immune system during the colder winter months. 

This scrumptious soup recipe to be rich in immune supportive ingredients including ginger, turmeric, leek, garlic, carrot and cauliflower and some zinc rich pepitas for the crunch factor on top.

Get ahead and try this immune boosting soup to guard your families immune system before the winter kicks in.

Carrot Cauliflower Soup with Crunchy Tamari Pepitas
Serves: 4-6
Gluten free, Dairy free and Vegan

Soup:
2 tablespoons (40mL) extra virgin olive oil
1 large (150g) leek
4 cloves garlic, finely sliced
2 tablespoons ginger, sliced into thin sticks
2 teaspoons ground turmeric
2 teaspoons ground cumin
2 teaspoons sweet paprika
1.2kg carrots, trimmed and chopped
1 head (1.3kg) cauliflower, cut into florets
1.8L vegetable stock or broth
Sea salt and black pepper
To serve: fresh chopped coriander leaves and stalks

Tamari Pepitas:
1/2 cup pepitas
2 teaspoons (10mL) tamari
2 teaspoons (10mL) extra virgin olive oil

1. Heat a large heavy based saucepan on medium heat and add extra virgin olive oil, leek and garlic, and ginger, cover and saute for 10 minutes, stirring frequently.

2. Add turmeric, cumin, paprika and cook stirring for 1 minute, then add carrots, cauliflower florets, stock and season with sea salt and black pepper.

3. Cover and bring to the boil, then remove lid, reduce heat to simmer, and cook a further 40 minutes.

4. Whilst the soup is cooking make the tamari pepitas by heating oven to 180C and lining a baking tray with greaseproof paper.

5. Combine tamari and extra virgin olive oil in a bowl, add pepitas and toss together to coat, then spread evenly over the baking tray and place in oven to bake for 8-10 minutes, tossing once throughout the cooking process.

6. Cool soup slightly then blend until smooth.

7. Serve in bowls topped with tamari pepitas and fresh coriander. Store in fridge up to 1 week or freezer up to 2 months. Pepitas best stored in an airtight container for up to 2 weeks.', N'post/post4.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-05-19T01:57:00' AS SmallDateTime), 1, 1, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (5, N'Everything You Need to Know About Heart Disease', N'Who gets heart disease?
Heart disease is the leading cause of death in the United States, according to the Centers for Disease Control and Prevention (CDC)Trusted Source. In the United States, 1 in every 4 deaths in is the result of a heart disease. That’s about 610,000 people who die from the condition each year.

Heart disease doesn’t discriminate. It’s the leading cause of death for several populations, including white people, Hispanics, and Black people. Almost half of Americans are at risk for heart disease, and the numbers are rising. Learn more about the increase in heart disease rates.

While heart disease can be deadly, it’s also preventable in most people. By adopting healthy lifestyle habits early, you can potentially live longer with a healthier heart.

What are the different types of heart disease?
Heart disease encompasses a wide range of cardiovascular problems. Several diseases and conditions fall under the umbrella of heart disease. Types of heart disease include:

Arrhythmia. An arrhythmia is a heart rhythm abnormality.
Atherosclerosis. Atherosclerosis is a hardening of the arteries.
Cardiomyopathy. This condition causes the heart’s muscles to harden or grow weak.
Congenital heart defects. Congenital heart defects are heart irregularities that are present at birth.
Coronary artery disease (CAD). CAD is caused by the buildup of plaque in the heart’s arteries. It’s sometimes called ischemic heart disease.
Heart infections. Heart infections may be caused by bacteria, viruses, or parasites.
The term cardiovascular disease may be used to refer to heart conditions that specifically affect the blood vessels.

What are the symptoms of heart disease?
Different types of heart disease may result in a variety of different symptoms.

Arrhythmias
Arrhythmias are abnormal heart rhythms. The symptoms you experience may depend on the type of arrhythmia you have — heartbeats that are too fast or too slow. Symptoms of an arrhythmia include:

lightheadedness
fluttering heart or racing heartbeat
slow pulse
fainting spells
dizziness
chest pain
Atherosclerosis
Atherosclerosis reduces blood supply to your extremities. In addition to chest pain and shortness of breath, symptoms of atherosclerosis include:

coldness, especially in the limbs
numbness, especially in the limbs
unusual or unexplained pain
weakness in your legs and arms
Congenital heart defects
Congenital heart defects are heart problems that develop when a fetus is growing. Some heart defects are never diagnosed. Others may be found when they cause symptoms, such as:

blue-tinged skin
swelling of the extremities
shortness of breath or difficulty breathing
fatigue and low energy
irregular heart rhythm
Coronary artery disease (CAD)
CAD is plaque buildup in the arteries that move oxygen-rich blood through the heart and lungs. Symptoms of CAD include:

chest pain or discomfort
a feeling of pressure or squeezing in the chest
shortness of breath
nausea
feelings of indigestion or gas
Cardiomyopathy
Cardiomyopathy is a disease that causes the muscles of the heart to grow larger and turn rigid, thick, or weak. Symptoms of this condition include:

fatigue
bloating
swollen legs, especially ankles and feet
shortness of breath
pounding or rapid pulse
Heart infections
The term heart infection may be used to describe conditions such as endocarditis or myocarditis. Symptoms of a heart infection include:

chest pain
chest congestion or coughing
fever
chills
skin rash
Read more about the signs and symptoms of heart disease.', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-05-18T19:57:00' AS SmallDateTime), 1, 2, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (6, N'Dental Treatment', N'Permanent teeth can last a lifetime with good care. The risk of tooth decay, gum disease and tooth loss can be reduced with good oral hygiene, a low-sugar and acid diet, use of a mouthguard when playing sport, and regular dental visits.. It is recommended that everyone, including young children, visit the dentist twice a year. Modern techniques mean that dental treatment can be carried out with no, or very little, discomfort.

Dental check-ups
When you go for a dental check-up, your dentist should ask questions about your general health and medications. Many health conditions have an effect on your oral health and vice versa. Some medicines can affect your mouth or need to be taken into consideration before dental treatment.

Your dentist will check each tooth using small instruments, such as a mirror and probe (a fine, pick-like tool). The dentist looks for issues such as tooth decay, gum disease and other conditions. 

Soft tissues in the mouth (gums, tongue, lips, cheeks and palate) are also checked for signs of oral cancer and other possible problems. Your dentist may also check your jaw joints and the lymph nodes in your neck. 

If a suspected dental problem is difficult to see (for example, possible decay between two touching teeth, or an infection), x-rays may be needed. If there’s a problem, your dentist will explain the treatment options and give you an estimate of the cost and the time it will take.

Professional teeth cleaning
Professional cleaning removes built-up debris from the teeth. This may include food particles, soft plaque (bacterial growth) or hard calculus (caused by the mineral deposits from saliva onto the soft plaque , sometimes called tartar). Plaque and calculus are the main causes of gum disease. 

The dental professional then cleans and polishes your teeth using a rotating brush with a polishing paste. Professional cleaning helps treat and prevent gum disease.

Your dental professional can give you advice on how to keep up your oral hygiene between appointments to maintain healthy teeth and gums.

Dental fissure sealants
Sealants protect teeth from decay. Any tooth that has deep grooves or fissures can be treated, but fissure sealants are usually placed on molars and premolars rather than front teeth.

A sealant is painted on to the biting surface of a cleaned tooth, and sets as a durable plastic material. It creates a barrier that stops food and other bacteria from collecting in the grooves of the tooth and causing decay. Fissure sealants are often recommended for children, as they reduce the risk of decay in permanent teeth.

Dental fillings
Dental fillings are used to treat tooth decay that has caused a cavity (hole). The dentist removes decay from the tooth, cleans the cavity, dries and seals it with a filling material.

A variety of materials can be used to fill the cavity. You will be given advice on the most suitable material based on the size, shape and location of the filling. A common choice is tooth-coloured filling material (such as resin composite or ceramic), which can restore the appearance of the tooth, as well as its shape and function.

Dental treatment for restoring teeth
Your dentist can suggest various treatments to restore damaged teeth. These treatments help restore the appearance, shape and function of your teeth. They include:

Composite resins -  – chipped,  discoloured or oddly shaped teeth can be treated  by bonding a tooth-coloured resin filling to improve the appearance of the affected tooth. This resin may need to be replaced in future  if it chips, wears or stains.
Veneers – a veneer is a thin layer of resin or porcelain that is permanently glued to the front of the tooth. Sometimes the tooth needs to be slightly ground down to allow space for the veneer.
Crowns or onlays – these are caps that are permanently cemented or bonded to a tooth. Crowns cover the whole tooth and can be made of porcelain, metal, or a combination of both depending on the area and the aesthetic or functional needs. Onlays cover the tooth less extensively than a crown and may be used if there is enough tooth structure remaining to support it.
Root canal treatment
Root canal treatment involves replacing a tooth’s damaged or infected pulp with a filling. The ‘pulp'' (often called the nerve of the tooth) is contained within the hollow centre of the tooth and consists of blood vessels and nerve fibres that  supply oxygen, nutrients and feeling to the tooth. 

Injuries to the tooth or advanced decay can irreversibly damage tooth pulp and it can become infected.
During root canal treatment, the damaged pulp is removed. The dentist cleans and shapes the root canals with a drill and small files. The tooth’s interior is cleaned, dried and packed with a filling material that goes all the way down to the end of the root. A root canal may need to be performed in stages over a few appointments.

After root canal treatment is completed, the biting surface is covered with filling material or a crown. This also protects the tooth from breaking after root canal treatment. 

Tooth removal (extraction)
Dental practitioners aim to preserve natural teeth. However, extensively damaged or badly decayed teeth may need to be removed (extracted). Dentists may also recommend removing wisdom teeth that are causing problems.

Wisdom teeth can contribute to various dental problems if they are below the gum (impacted), where the wisdom tooth grows at an angle and butts into the tooth next to it or the gum.Wisdom teeth may be removed with local anaesthetic only (only the area around the tooth is numbed and the person is conscious). However, for more difficult procedures, sedation or general anaesthesia may also be offered where the person is partially or fully unconscious.

Dentures
Dentures (also known as ''false teeth'') are removable artificial teeth that replace some or all of your natural teeth. An ''immediate'' denture can be made while you still have some of your teeth, which is fitted on the day teeth are removed. However, changes to the jawbone during the healing process may cause the denture to gradually loosen. Within a few months, the immediate denture may need relining to improve the fit. 

Alternatively, a denture can be made a few months after teeth are removed. This allows time for the jawbone to heal and means that the denture should have a better fit.

Dentures need to be removed and cleaned every day. It is recommended that you do not sleep with your dentures in.

Mouthguards
Mouthguards protect the teeth, gums, lips, tongue and jaws from injury. They are used while playing sport to prevent damage from accidental or deliberate knocks to the face. 

A dental professional can take a mould (impressions) of your teeth and make a well-fitting, comfortable mouthguard for your protection. Custom made mouthguards offer a better fit and protection against dental injuries than ready-made mouthguards.

Dental implants
Dental implants can be used to replace missing teeth. An implant is an artificial screw-shaped device made of titanium. It is surgically fixed into the jaw and an artificial tooth can be fitted on top of it. Several dental appointments are required for treatment planning, design and fitting of implants.

Titanium is a safe material that allows bone to grow around it. Implants generally have high success rates but they require an additional level of training and expertise so you may need to be referred to another dentist or specialist.

Orthodontic treatment
Orthodontic treatment is often recommended to correct abnormalities in jaw and tooth position, such as crowding, protruding (‘buck’) upper teeth or protruding lower teeth. Your dentist may be able to treat these problems or refer you to a specialist orthodontist for treatment. Corrective treatment may include braces or a removable device. After orthodontic treatment, a retainer appliance is needed to maintain correct tooth positions. Jaw surgery might be necessary where extensive correction is needed for best results, which requires referral to a oral maxillofacial surgeon.

Referral to specialist for dental treatment
For treatment in difficult or complex cases, your dentist may refer you to a specialist dentist. 

Subsidised dental treatment
All children aged up to 12 years are eligible for public dental services (non-concession card holders are eligible for general and denture care only). Children aged up to 17 years old may be eligible for the Medicare Child Dental Benefits Schedule if they (or their parents) are receiving certain Centrelink payments.

You may also be eligible for public dental services if you fall into one of the following categories:

Young people aged 13 to 17 years who are healthcare card holders or dependants of concession card holders
All children and young people up to 18 years of age in residential care provided by the Children Youth and Families division of the Department of Health and Human Services
All youth justice clients in custodial care, up to 18 years of age
People aged 18 years and over, who are healthcare or pensioner concession card holders or dependants of concession card holders
All refugees and asylum seekers
All Aboriginal and Torres Strait Islander people who are treated at The Royal Dental Hospital of Melbourne.', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-05-18T16:57:00' AS SmallDateTime), 1, 2, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (7, N'Dental Treatment', N'Permanent teeth can last a lifetime with good care. The risk of tooth decay, gum disease and tooth loss can be reduced with good oral hygiene, a low-sugar and acid diet, use of a mouthguard when playing sport, and regular dental visits.. It is recommended that everyone, including young children, visit the dentist twice a year. Modern techniques mean that dental treatment can be carried out with no, or very little, discomfort.

Dental check-ups
When you go for a dental check-up, your dentist should ask questions about your general health and medications. Many health conditions have an effect on your oral health and vice versa. Some medicines can affect your mouth or need to be taken into consideration before dental treatment.

Your dentist will check each tooth using small instruments, such as a mirror and probe (a fine, pick-like tool). The dentist looks for issues such as tooth decay, gum disease and other conditions. 

Soft tissues in the mouth (gums, tongue, lips, cheeks and palate) are also checked for signs of oral cancer and other possible problems. Your dentist may also check your jaw joints and the lymph nodes in your neck. 

If a suspected dental problem is difficult to see (for example, possible decay between two touching teeth, or an infection), x-rays may be needed. If there’s a problem, your dentist will explain the treatment options and give you an estimate of the cost and the time it will take.

Professional teeth cleaning
Professional cleaning removes built-up debris from the teeth. This may include food particles, soft plaque (bacterial growth) or hard calculus (caused by the mineral deposits from saliva onto the soft plaque , sometimes called tartar). Plaque and calculus are the main causes of gum disease. 

The dental professional then cleans and polishes your teeth using a rotating brush with a polishing paste. Professional cleaning helps treat and prevent gum disease.

Your dental professional can give you advice on how to keep up your oral hygiene between appointments to maintain healthy teeth and gums.

Dental fissure sealants
Sealants protect teeth from decay. Any tooth that has deep grooves or fissures can be treated, but fissure sealants are usually placed on molars and premolars rather than front teeth.

A sealant is painted on to the biting surface of a cleaned tooth, and sets as a durable plastic material. It creates a barrier that stops food and other bacteria from collecting in the grooves of the tooth and causing decay. Fissure sealants are often recommended for children, as they reduce the risk of decay in permanent teeth.

Dental fillings
Dental fillings are used to treat tooth decay that has caused a cavity (hole). The dentist removes decay from the tooth, cleans the cavity, dries and seals it with a filling material.

A variety of materials can be used to fill the cavity. You will be given advice on the most suitable material based on the size, shape and location of the filling. A common choice is tooth-coloured filling material (such as resin composite or ceramic), which can restore the appearance of the tooth, as well as its shape and function.

Dental treatment for restoring teeth
Your dentist can suggest various treatments to restore damaged teeth. These treatments help restore the appearance, shape and function of your teeth. They include:

Composite resins -  – chipped,  discoloured or oddly shaped teeth can be treated  by bonding a tooth-coloured resin filling to improve the appearance of the affected tooth. This resin may need to be replaced in future  if it chips, wears or stains.
Veneers – a veneer is a thin layer of resin or porcelain that is permanently glued to the front of the tooth. Sometimes the tooth needs to be slightly ground down to allow space for the veneer.
Crowns or onlays – these are caps that are permanently cemented or bonded to a tooth. Crowns cover the whole tooth and can be made of porcelain, metal, or a combination of both depending on the area and the aesthetic or functional needs. Onlays cover the tooth less extensively than a crown and may be used if there is enough tooth structure remaining to support it.
Root canal treatment
Root canal treatment involves replacing a tooth’s damaged or infected pulp with a filling. The ‘pulp'' (often called the nerve of the tooth) is contained within the hollow centre of the tooth and consists of blood vessels and nerve fibres that  supply oxygen, nutrients and feeling to the tooth. 

Injuries to the tooth or advanced decay can irreversibly damage tooth pulp and it can become infected.
During root canal treatment, the damaged pulp is removed. The dentist cleans and shapes the root canals with a drill and small files. The tooth’s interior is cleaned, dried and packed with a filling material that goes all the way down to the end of the root. A root canal may need to be performed in stages over a few appointments.

After root canal treatment is completed, the biting surface is covered with filling material or a crown. This also protects the tooth from breaking after root canal treatment. 

Tooth removal (extraction)
Dental practitioners aim to preserve natural teeth. However, extensively damaged or badly decayed teeth may need to be removed (extracted). Dentists may also recommend removing wisdom teeth that are causing problems.

Wisdom teeth can contribute to various dental problems if they are below the gum (impacted), where the wisdom tooth grows at an angle and butts into the tooth next to it or the gum.Wisdom teeth may be removed with local anaesthetic only (only the area around the tooth is numbed and the person is conscious). However, for more difficult procedures, sedation or general anaesthesia may also be offered where the person is partially or fully unconscious.

Dentures
Dentures (also known as ''false teeth'') are removable artificial teeth that replace some or all of your natural teeth. An ''immediate'' denture can be made while you still have some of your teeth, which is fitted on the day teeth are removed. However, changes to the jawbone during the healing process may cause the denture to gradually loosen. Within a few months, the immediate denture may need relining to improve the fit. 

Alternatively, a denture can be made a few months after teeth are removed. This allows time for the jawbone to heal and means that the denture should have a better fit.

Dentures need to be removed and cleaned every day. It is recommended that you do not sleep with your dentures in.

Mouthguards
Mouthguards protect the teeth, gums, lips, tongue and jaws from injury. They are used while playing sport to prevent damage from accidental or deliberate knocks to the face. 

A dental professional can take a mould (impressions) of your teeth and make a well-fitting, comfortable mouthguard for your protection. Custom made mouthguards offer a better fit and protection against dental injuries than ready-made mouthguards.

Dental implants
Dental implants can be used to replace missing teeth. An implant is an artificial screw-shaped device made of titanium. It is surgically fixed into the jaw and an artificial tooth can be fitted on top of it. Several dental appointments are required for treatment planning, design and fitting of implants.

Titanium is a safe material that allows bone to grow around it. Implants generally have high success rates but they require an additional level of training and expertise so you may need to be referred to another dentist or specialist.

Orthodontic treatment
Orthodontic treatment is often recommended to correct abnormalities in jaw and tooth position, such as crowding, protruding (‘buck’) upper teeth or protruding lower teeth. Your dentist may be able to treat these problems or refer you to a specialist orthodontist for treatment. Corrective treatment may include braces or a removable device. After orthodontic treatment, a retainer appliance is needed to maintain correct tooth positions. Jaw surgery might be necessary where extensive correction is needed for best results, which requires referral to a oral maxillofacial surgeon.

Referral to specialist for dental treatment
For treatment in difficult or complex cases, your dentist may refer you to a specialist dentist. 

Subsidised dental treatment
All children aged up to 12 years are eligible for public dental services (non-concession card holders are eligible for general and denture care only). Children aged up to 17 years old may be eligible for the Medicare Child Dental Benefits Schedule if they (or their parents) are receiving certain Centrelink payments.

You may also be eligible for public dental services if you fall into one of the following categories:

Young people aged 13 to 17 years who are healthcare card holders or dependants of concession card holders
All children and young people up to 18 years of age in residential care provided by the Children Youth and Families division of the Department of Health and Human Services
All youth justice clients in custodial care, up to 18 years of age
People aged 18 years and over, who are healthcare or pensioner concession card holders or dependants of concession card holders
All refugees and asylum seekers
All Aboriginal and Torres Strait Islander people who are treated at The Royal Dental Hospital of Melbourne.', N'post/post6.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-05-18T16:57:00' AS SmallDateTime), 1, 1, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (8, N'Resource Center: 25 Valuable Health and Wellness Blogs to Follow in 2022', N'When you lack good health, everything else in life suffers. It’s harder to concentrate and study. Illness can cause you to fall behind in coursework. Stress can make you feel overwhelmed. If you want to do well in all areas of your life, you must make your health a priority.

In the 2020 Social Progress Index, which measures quality of life across the globe using 50 well-being metrics, Americans rated themselves worse off now than they were 10 years ago. This data was collected before the COVID-19 pandemic hit, which has only increased the nation’s need to live healthier, more balanced lives.

As we prepare to enter a new year, you may be considering spending more time working on health and wellness. You don’t necessarily need an expensive gym membership or a diet plan to improve your health. You can get solid health and wellness tips from authoritative blogs. The following are some of the top wellness blogs in areas of fitness, nutrition, and mental health.

Fitness

1. ACE Insights Blog
acefitness.org/education-and-resources/professional/expert-articles

The American Council on Exercise’s blog has fitness articles on a variety of topics such as yoga, strength training, and exercises for beginners. Articles are backed by guidelines and recommendations from the American Council on Exercise.

2. The Art of Healthy Living
artofhealthyliving.com

The Art of Healthy Living offers fitness articles related to subjects such as exercise supplements and high-intensity interval training. It also covers food and nutrition topics and general health and well-being trends.

3. Nerd Fitness
nerdfitness.com/blog/

This blog serves up health and fitness advice from a self-described “full-time team of 25 supportive nerds (and a massive worldwide community).” Besides a fun and engaging blog, Nerd Fitness offers an online coaching program, at-home workouts, and free resources.

4. MyFitnessPal Blog
blog.myfitnesspal.com

This is the blog of nutrition tracking app MyFitnessPal. Get healthy recipes, nutrition advice, weight loss tips, fitness inspiration, and more.

5. Shape
shape.com

The website for fitness magazine Shape has tons of free articles, including step-by-step workouts, nutrition advice, and mental health insights. There are also weight-loss tips and plans and videos.

6. Well + Good
wellandgood.com

Well + Good is a wellness blog with articles on fitness, nutrition, beauty, and more. The journalism-based fitness site features interviews with fitness experts, practical fitness tips, and videos to walk blog readers through workouts.

Nutrition

7. Bites of Wellness
bitesofwellness.com

Bites of Wellness founder Samantha Rowland is a personal trainer, nutrition coach, and fitness chef. Her site contains recipes for those with various diets, including gluten free, low carb, and dairy free. The site also contains workouts and wellness insights, such as how sleep relates to weight loss.

8. Eating Bird Food
eatingbirdfood.com

Brittany Mullins, a holistic nutritionist, runs this site and shares healthy recipe creations focused on whole foods. She also features articles on wellness and workouts.

9. Health Magazine
health.com/nutrition

Health magazine’s nutrition section offers nutritionist-researched articles on foods, diets, and nutrition trends. Find the health benefits of certain ingredients, learn about the benefits and pitfalls of various diets, and get tips for eating healthy.

10. Healthline Nutrition
healthline.com/nutrition

Healthline’s nutrition blog features daily research-based articles on nutrition and weight loss. Learn about foods you should eat more of, foods to avoid, and the scientific reasoning behind everything the site teaches you.

11. Nutrition Stripped
nutritionstripped.com/articles/

Nutrition Stripped is a website founded by Registered Dietitian McKel Kooienga. She shares articles on trending nutrition topics using science to teach readers about popular food subjects and diet habits.

12. Oh She Glows
ohsheglows.com

The Oh She Glows blog features vegan recipes, most of which are also free of gluten, soy, and processed food. Recipes are accompanied by Founder and Cookbook Author Angela Liddon’s inspirations and insights.

13. Time Nutrition
time.com/tag/nutrition/

Time magazine’s nutrition site has journalism-based articles featuring interviews with nutrition experts on trending nutrition topics. Visit the site for food news and research.

Health News and General Wellness

14. Calm
blog.calm.com

The Calm blog comes from the relaxation app of the same name. Find news regarding meditation research plus tips on how to cultivate mindfulness and practice meditation.

15. DailyOM
dailyom.com/cgi-bin/display/inspirations.cgi

The Inspirations section on the DailyOM website is full of wisdom designed to boost mental and spiritual health. Bite-size articles provide daily encouragement and thought-provoking passages to embrace mindfulness and a deeper sense of interconnectedness.

16. HealthyPlace
healthyplace.com/blogs

The HealthyPlace site has dozens of mental health blogs dedicated to specific mental health issues. These include ADHD, bipolar disorder, binge eating, post-traumatic stress disorder, and depression.

17. Mellowed
mellowed.com/category/health-wellness/

Mellowed is a website designed to help users decrease stress in their lives. The site’s “Health & Wellness” section features articles on topics ranging from improving sleep to boosting brain health to naturally lowering blood pressure.

18. National Alliance on Mental Health
nami.org/Blogs/NAMI-Blog

This national organization offers mental health support and resources, including this blog. It reflects current events, such as COVID-19, trauma, and racism and how they shape our mental health. 

19. Sleep Junkies
sleepjunkies.com

High-quality sleep is essential to relieve stress and improve mental health. Sleep Junkies teaches users how to improve sleep with research-backed articles and insights on how other wellness issues, such as anxiety, interact with sleep.

20. Student Minds
studentmindsorg.blogspot.com

Student Minds is a blog for and written by university students covering a variety of mental health issues, including eating disorders, depression, and substance-use disorder. The blog has practical tips and unique voices throughout.

21. Tiny Buddha
tinybuddha.com

Tiny Buddha’s mission is to provide “simple wisdom for complex lives.” The website covers mental health topics such as love and relationships, meaning and passion, and health habits.



22. NPR Shots
npr.org/sections/health-shots/

This is the blog for health stories produced by the NPR Science Desk. The “Shots” section publishes the latest news on health, research, and medical treatments, as well as articles on how policy shapes our health.

23. Well—The New York Times
nytimes.com/section/well

The “Well” section of The New York Times features articles in five categories: eat, move, mind, family, and live. You’ll also find “Ask Well,” a collection of Q&As between readers and medical experts.

24. Harvard Health Blog
health.harvard.edu/blog/

Harvard Health Blog publishes medical news, viewpoints, and articles across a wide variety of topics. The blog covers such areas as exercise and fitness, pain management, healthy eating, mental health, mind-body medicine, relationships, heart health, and children’s, men’s, and women’s health.

25. Mayo Clinic Connect
connect.mayoclinic.org

Mayo Clinic Connect is an online community where you can share your experiences and find support from people experiencing the same health challenges as you. There are moderated groups in more than 50 categories. You can also read articles by Mayo Clinic health experts.

Discover More Wellness Insights for College Students
At Purdue University Global, we are committed to promoting health and wellness to all students, so they’re prepared with the tools to have a successful school journey and a more fulfilling life. These are some of the top health blogs to follow, but be sure you also visit our Student Life blog for more advice on health and wellness, time management, and work-school-life balance. If you’re interested in learning how our online college programs can help you meet your career goals, reach out to us today.

', N'post/post8.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-05-18T16:57:00' AS SmallDateTime), 1, 1, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (9, N'7 Healthy-Living Bloggers You Should Totally Start Following', N'We could all use some inspiration and motivation. And what better way than to follow a health blogger who’s bursting with bright ideas about new workouts, delicious and nutritious recipes, and expert advice on how to feel good inside and out, both mentally and physically.

“Following food and fitness blogs can be very motivating—if you follow the right ones. Sometimes all it takes is one post to motivate change within ourselves,” says Heather Mangieri, R.D.N., spokesperson for the Academy of Nutrition and Dietetics. The key is to follow blogs that promote positive messages, she says. Overall, their posts should make you feel good about yourself.

On the other hand, people who promote unrealistic images don’t belong on your feed. That’s especially true “if they are claiming you should look and feel like they do, even though they’re not dealing with your barriers,” says Mangieri. To cultivate the perfect blogs for you, look for health bloggers who lead a similar lifestyle as you, and also think about exactly what you’re looking to get out of the blog (healthy recipes, motivation, real life stories?), she suggests. 

To help you get started, let’s talk about seven positive and inspirational health bloggers you may want to follow. (Editor''s note: Mangieri’s blog, Nutrition Checkup, where she shares recipes and tips for athletes on fueling their busy lifestyles, belongs on this list, too!) Whether you’re in an eating slump, feeling confused about which foods are “bad" or good for you, or you''re getting bored with your workout routine, make these girls your go-tos. And, if you need a little rah-rah spirit to remind you that you are amazing/dynamic/worthy of all the good things, they’re here for that, too.

Get ready to fall in love...', N'post/post9.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-05-18T16:57:00' AS SmallDateTime), 1, 1, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (10, N'RECOGNIZING URGENT SYMPTOMS – WHEN YOU SHOULD TAKE YOUR CHILD TO THE DOCTOR', N'Being a parent is a full-time job. There will be times when you will come across innumerable scuffs, sicknesses, and various boo-boo’s, particularly if you have more than one child. It may seem like not a day goes by without some incident occurring.

Each passing day is filled with small crises, and if not one of your children has bumped or scraped anything, then you feel like you have just been awarded with the Super Mom or Super Dad award. However, with all those incidences, it can be tough to determine which one is serious enough to call for a trip to the doctor. Have no fear Super Mom/Dad, we’ll share with you some of the most ordinary symptoms that means it’s time to head to your pediatrician for a child checkup!

FEVER THAT DOESN’T LOWER WITH ACETAMINOPHEN

At some point in time, it is necessary that your child will fall low with a fever or flu – it’s the circle of life. There’s really no need to worry, just give them the right dosages of Acetaminophen to get them to feel better slowly and gradually. However, there is a point when you should start becoming concerned. This is when you’ve given them the appropriate dosage of acetaminophen (medicine used when your child has a fever) and the fever doesn’t start to lower or even still, continues to rise. If their fever remains over 100 degrees Fahrenheit, then it’s time to make your trip to the pediatrician as soon as possible.

EXTREME CASE OF VOMITING MULTIPLE TIMES EACH HOUR
Stomach viruses are unavoidable and your child may get one sooner or later. They, again, are completely normal and may be the cause of eating unhealthy or eating junk food too often. The point when these get quite alarming is actually when the vomiting continues, even after it seems that your child may have nothing left in their stomach or hasn’t eaten in a while yet keeps throwing up, and they don’t start to feel better even after a couple of hours of doing so. It means that something is wrong, such as a slight case of food poisoning, and it’s best to call your pediatrician or take your child to them right away.

BLEEDING AFTER A BANDAGE IS APPLIED
Children are born to play around and getting boo-boo’s whilst doing so is something that is not in our hands. Cuts and scrapes are therefore unavoidable. They can be treated very easily at home, by a small kiss from mommy and a bandage on top. The point where you should actually enquire what is going on from your pediatrician is when a cut seems to be deeper than the usual scrape, or if the bleeding continues to be stable or increases over a period of time, or if the cut is deep and is somewhat close to a chief artery, then you need to bring your child in to the doctor’s office right away. This is so that the wound can be closed completely with stitches. Wounds that are not closed properly can cause infections, and it’s better to prevent infection now than to treat it later on.

Set up an appointment today, to get your child back to feeling better!', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-05-19T10:00:00' AS SmallDateTime), 1, 1, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (11, N'Steal This Movie!', N'Other specified injuries of right wrist, hand and finger(s), initial encounter', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-12-13T00:00:00' AS SmallDateTime), 1, 3, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (12, N'Unbearable Lightness of Being, The', N'<p>Poisoning by other antidepressants, intentional self-harm, subsequent encounter</p>', N'post\BSC_HealthyPlate_1200x628_Revised-1200x610.jpg                                                                                                                                                                                                                                                         ', CAST(N'2022-05-01T00:00:00' AS SmallDateTime), 1, 2, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (13, N'Death Watch (La Mort en Direct)', N'Burn of unspecified degree of multiple sites of left wrist and hand, initial encounter', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-10-28T00:00:00' AS SmallDateTime), 1, 8, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (14, N'Mugabe and the White African', N'Benign neoplasm of pineal gland', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-10-18T00:00:00' AS SmallDateTime), 0, 6, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (15, N'Quiet City', N'Genetic torsion dystonia', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-04-28T00:00:00' AS SmallDateTime), 0, 5, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (16, N'Sixth Sense, The', N'Unspecified injury of muscle and tendon of long extensor muscle of toe at ankle and foot level, right foot, subsequent encounter', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-02-08T00:00:00' AS SmallDateTime), 0, 12, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (17, N'Ringu 2 (Ring 2)', N'Laceration of unspecified blood vessel at lower leg level, left leg', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-10-20T00:00:00' AS SmallDateTime), 1, 1, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (18, N'Dead Man', N'Alcohol dependence with unspecified alcohol-induced disorder', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-11-06T00:00:00' AS SmallDateTime), 1, 2, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (19, N'Ghost', N'Poisoning by antithrombotic drugs, assault, initial encounter', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-01-05T00:00:00' AS SmallDateTime), 0, 4, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (20, N'Snows of Kilimanjaro, The', N'Enteropathic arthropathies, left shoulder', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-09-10T00:00:00' AS SmallDateTime), 0, 9, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (21, N'Shattered Image', N'Laceration without foreign body of abdominal wall, left upper quadrant without penetration into peritoneal cavity, sequela', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-09-28T00:00:00' AS SmallDateTime), 0, 7, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (22, N'Film Geek', N'Sprain of interphalangeal joint of left middle finger, subsequent encounter', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-11-11T00:00:00' AS SmallDateTime), 0, 5, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (23, N'Too Shy to Try (Je suis timide... mais je me soigne)', N'Unspecified fracture of unspecified pubis, initial encounter for open fracture', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-12-11T00:00:00' AS SmallDateTime), 1, 11, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (24, N'Objective, Burma!', N'Poisoning by, adverse effect of and underdosing of other antiepileptic and sedative-hypnotic drugs', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-08-13T00:00:00' AS SmallDateTime), 1, 1, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (25, N'Amores Perros (Love''s a Bitch)', N'Aphasia following nontraumatic subarachnoid hemorrhage', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-02-22T00:00:00' AS SmallDateTime), 0, 3, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (26, N'Yoo-Hoo, Mrs. Goldberg', N'Unspecified inflammatory spondylopathy, lumbosacral region', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-04-18T00:00:00' AS SmallDateTime), 1, 5, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (27, N'Flesh Gordon', N'Senile entropion of eyelid', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-08-04T00:00:00' AS SmallDateTime), 0, 6, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (28, N'Kids for Cash', N'Turner''s syndrome, unspecified', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-05-26T00:00:00' AS SmallDateTime), 1, 2, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (29, N'Red Planet', N'Nondisplaced fracture of left radial styloid process, subsequent encounter for open fracture type IIIA, IIIB, or IIIC with routine healing', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-05-04T00:00:00' AS SmallDateTime), 0, 8, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (30, N'General, The', N'Keratosis follicularis et parafollicularis in cutem penetrans', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-03-01T00:00:00' AS SmallDateTime), 0, 7, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (31, N'Chaos (Kaosu)', N'Fracture of ramus of left mandible', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-02-08T00:00:00' AS SmallDateTime), 0, 5, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (32, N'Game of Their Lives, The', N'Complete traumatic metacarpophalangeal amputation of unspecified finger, sequela', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-06-26T00:00:00' AS SmallDateTime), 0, 4, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (33, N'Queen of Blood', N'Puncture wound with foreign body of left little finger without damage to nail', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-02-18T00:00:00' AS SmallDateTime), 0, 12, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (34, N'Against the Ropes', N'Passenger of military vehicle injured in nontraffic accident, sequela', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-06-07T00:00:00' AS SmallDateTime), 1, 5, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (35, N'Saawariya', N'Rheumatoid arthritis without rheumatoid factor, right shoulder', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-03-27T00:00:00' AS SmallDateTime), 1, 1, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (36, N'Critters 4', N'Contusion and laceration of left cerebrum with loss of consciousness greater than 24 hours without return to pre-existing conscious level with patient surviving', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-09-12T00:00:00' AS SmallDateTime), 0, 8, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (37, N'Send Me No Flowers', N'Labor and delivery complicated by umbilical cord complications', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-03-07T00:00:00' AS SmallDateTime), 0, 3, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (38, N'Raiders of the Lost Ark: The Adaptation', N'Burn of second degree of nose (septum), subsequent encounter', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-05-24T00:00:00' AS SmallDateTime), 0, 9, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (39, N'Groundhog Day', N'Intentional self-harm by explosive material, sequela', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-05-24T00:00:00' AS SmallDateTime), 0, 9, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (40, N'Midnight (Primeiro Dia, O)', N'Laceration of intrinsic muscle, fascia and tendon of right ring finger at wrist and hand level, initial encounter', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-10-31T00:00:00' AS SmallDateTime), 0, 7, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (41, N'Four Christmases', N'Corrosion of first degree of unspecified palm, initial encounter', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-09-02T00:00:00' AS SmallDateTime), 0, 12, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (42, N'Now!', N'Contusion of shoulder', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-12-07T00:00:00' AS SmallDateTime), 0, 8, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (43, N'88 Minutes', N'Varicose veins of unspecified lower extremity with ulcer of heel and midfoot', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-10-28T00:00:00' AS SmallDateTime), 0, 10, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (44, N'Man-Proof', N'Laceration without foreign body of unspecified great toe without damage to nail, sequela', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-02-28T00:00:00' AS SmallDateTime), 0, 9, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (45, N'Lair of the White Worm, The', N'Corrosion of first degree of chin, initial encounter', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-04-03T00:00:00' AS SmallDateTime), 1, 12, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (46, N'DiG!', N'Abrasion of right ring finger, initial encounter', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-02-04T00:00:00' AS SmallDateTime), 0, 12, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (47, N'League of Extraordinary Gentlemen, The (a.k.a. LXG)', N'Fistula, right wrist', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-03-17T00:00:00' AS SmallDateTime), 1, 10, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (48, N'House III: The Horror Show', N'Acute hematogenous osteomyelitis, right tibia and fibula', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-11-09T00:00:00' AS SmallDateTime), 0, 5, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (49, N'Headhunter''s Sister, The', N'Nondisplaced comminuted fracture of shaft of unspecified fibula, subsequent encounter for closed fracture with delayed healing', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-07-21T00:00:00' AS SmallDateTime), 0, 12, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (50, N'Let the Right One In (Låt den rätte komma in)', N'Retained (old) magnetic foreign body in anterior chamber', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-08-24T00:00:00' AS SmallDateTime), 0, 8, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (51, N'Pitfall (Otoshiana)', N'Contusion of bladder, initial encounter', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-05-23T00:00:00' AS SmallDateTime), 0, 8, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (52, N'Aftershock (Tangshan dadizhen)', N'Rheumatoid arthritis of right elbow with involvement of other organs and systems', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-10-06T00:00:00' AS SmallDateTime), 0, 3, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (53, N'Friends and Family', N'Laceration of other specified intrathoracic organs, sequela', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-06-24T00:00:00' AS SmallDateTime), 0, 4, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (54, N'Grand Masti', N'Salter-Harris Type I physeal fracture of lower end of unspecified tibia, subsequent encounter for fracture with routine healing', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-07-15T00:00:00' AS SmallDateTime), 1, 2, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (55, N'Romantics, The', N'Corrosion of first degree of multiple sites of head, face, and neck, initial encounter', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-06-10T00:00:00' AS SmallDateTime), 0, 2, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (56, N'Blind Mountain (Mang shan)', N'Displaced fracture of base of neck of right femur, initial encounter for open fracture type I or II', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-07-19T00:00:00' AS SmallDateTime), 1, 11, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (57, N'Tere Bin Laden', N'Other osteoporosis with current pathological fracture, vertebra(e)', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-11-13T00:00:00' AS SmallDateTime), 0, 3, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (58, N'Evening with Kevin Smith 2: Evening Harder, An', N'War operations involving fragments from munitions, civilian', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-08-06T00:00:00' AS SmallDateTime), 1, 3, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (59, N'Pekka ja Pätkä lumimiehen jäljillä', N'Partial traumatic amputation of left lower leg, level unspecified, initial encounter', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-02-07T00:00:00' AS SmallDateTime), 1, 11, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (60, N'In Old California', N'Nondisplaced fracture of body of unspecified calcaneus, initial encounter for open fracture', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-04-24T00:00:00' AS SmallDateTime), 1, 5, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (61, N'Regeneration', N'Subluxation of symphysis (pubis) in the puerperium', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-04-10T00:00:00' AS SmallDateTime), 0, 8, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (62, N'Cannibal Holocaust', N'Military operations involving destruction of aircraft due to accidental detonation of onboard munitions and explosives, civilian, initial encounter', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-01-02T00:00:00' AS SmallDateTime), 0, 2, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (63, N'Enchanted Cottage, The', N'Gout due to renal impairment, unspecified ankle and foot', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-04-08T00:00:00' AS SmallDateTime), 0, 7, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (64, N'Carry on Cruising', N'Partial traumatic amputation at level between knee and ankle, right lower leg, subsequent encounter', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-04-14T00:00:00' AS SmallDateTime), 0, 6, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (65, N'Phantoms', N'Unspecified foreign body in larynx causing asphyxiation, initial encounter', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-06-23T00:00:00' AS SmallDateTime), 0, 5, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (66, N'Man Is Not a Bird (Covek nije tica)', N'Complete traumatic amputation at level between left shoulder and elbow', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-05-23T00:00:00' AS SmallDateTime), 0, 12, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (67, N'Nora''s Will (Cinco días sin Nora)', N'Neoplasm of uncertain behavior of left kidney', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-11-16T00:00:00' AS SmallDateTime), 1, 5, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (68, N'Amelie (Fabuleux destin d''Amélie Poulain, Le)', N'Lesion of ulnar nerve, unspecified upper limb', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-01-25T00:00:00' AS SmallDateTime), 1, 1, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (69, N'Sidewalks of London (St. Martin''s Lane)', N'Corrosion of third degree of single finger (nail) except thumb', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-03-05T00:00:00' AS SmallDateTime), 1, 9, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (70, N'Ivan''s Childhood (a.k.a. My Name is Ivan) (Ivanovo detstvo)', N'Suicide attempt', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-08-06T00:00:00' AS SmallDateTime), 0, 11, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (71, N'Princess Diaries, The', N'Displaced fracture of medial condyle of left tibia, subsequent encounter for open fracture type I or II with delayed healing', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-09-27T00:00:00' AS SmallDateTime), 1, 3, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (72, N'Other Side of the Bed, The (Otro lado de la cama, El)', N'Frostbite with tissue necrosis of unspecified sites, sequela', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-11-26T00:00:00' AS SmallDateTime), 0, 6, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (73, N'Someone Marry Barry', N'Acute lymphangitis of unspecified toe', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-10-27T00:00:00' AS SmallDateTime), 0, 8, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (74, N'Criminals', N'Nondisplaced longitudinal fracture of unspecified patella, subsequent encounter for closed fracture with malunion', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-01-29T00:00:00' AS SmallDateTime), 1, 1, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (75, N'New York Confidential', N'Brown-Sequard syndrome at T1 level of thoracic spinal cord, sequela', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-11-23T00:00:00' AS SmallDateTime), 0, 11, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (76, N'Love and Lemons (Små citroner gula)', N'Anterior dislocation of left radial head, subsequent encounter', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-05-30T00:00:00' AS SmallDateTime), 0, 3, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (77, N'My Crazy Life (Mi vida loca)', N'Poisoning by, adverse effect of and underdosing of psychostimulants', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-02-21T00:00:00' AS SmallDateTime), 0, 12, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (78, N'Million to Juan, A', N'Other reduction defects of upper limb, bilateral', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-06-04T00:00:00' AS SmallDateTime), 1, 4, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (79, N'Café de Flore', N'Osteitis condensans, right thigh', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-05-21T00:00:00' AS SmallDateTime), 1, 6, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (80, N'Assault on Precinct 13', N'Unspecified physeal fracture of lower end of right fibula, subsequent encounter for fracture with malunion', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-01-16T00:00:00' AS SmallDateTime), 1, 5, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (81, N'Traffic', N'Central cord syndrome at C4 level of cervical spinal cord, sequela', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-04-01T00:00:00' AS SmallDateTime), 0, 7, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (82, N'Manta, Manta', N'Corrosion of third degree of right shoulder, initial encounter', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-06-07T00:00:00' AS SmallDateTime), 1, 9, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (83, N'Hearts of Darkness: A Filmmakers Apocalypse', N'Unspecified injury of right eye and orbit, sequela', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-11-13T00:00:00' AS SmallDateTime), 1, 6, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (84, N'Robin and the 7 Hoods', N'Toxic effect of cobra venom, undetermined, sequela', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-10-11T00:00:00' AS SmallDateTime), 0, 4, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (85, N'China 9, Liberty 37 (Amore, piombo e furore)', N'Degeneration of ciliary body, bilateral', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-04-21T00:00:00' AS SmallDateTime), 1, 10, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (86, N'Buttcrack', N'Laceration of extensor muscle, fascia and tendon of right ring finger at wrist and hand level, subsequent encounter', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-06-27T00:00:00' AS SmallDateTime), 1, 5, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (87, N'Anaconda', N'Child physical abuse, confirmed, initial encounter', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-07-24T00:00:00' AS SmallDateTime), 1, 11, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (88, N'The Anderssons in Greece: All Inclusive', N'Open bite of right wrist, initial encounter', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-07-13T00:00:00' AS SmallDateTime), 1, 4, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (89, N'Anvil! The Story of Anvil', N'Other tear of unspecified meniscus, current injury, right knee, subsequent encounter', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-07-13T00:00:00' AS SmallDateTime), 1, 11, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (90, N'Werner - Gekotzt wird später', N'Poisoning by coronary vasodilators, intentional self-harm, initial encounter', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-03-27T00:00:00' AS SmallDateTime), 1, 8, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (91, N'Cherry Falls', N'Terrorism, secondary effects, civilian injured, subsequent encounter', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-07-08T00:00:00' AS SmallDateTime), 0, 12, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (92, N'American Werewolf in London, An', N'Puncture wound of abdominal wall without foreign body, unspecified quadrant without penetration into peritoneal cavity, sequela', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-09-29T00:00:00' AS SmallDateTime), 1, 12, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (93, N'Thin Ice', N'Polyp of cervix uteri', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-12-06T00:00:00' AS SmallDateTime), 1, 6, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (94, N'Superman and the Mole-Men', N'Gluteal tendinitis', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-06-08T00:00:00' AS SmallDateTime), 1, 8, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (95, N'Bullet for Joey, A', N'Unspecified injury of unspecified muscle, fascia and tendon at wrist and hand level, right hand', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-11-24T00:00:00' AS SmallDateTime), 0, 9, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (96, N'Klip (Clip)', N'Other specified fracture of unspecified acetabulum', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-09-11T00:00:00' AS SmallDateTime), 1, 11, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (97, N'2046', N'Toxic effect of other ingested (parts of) plant(s), assault', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-07-30T00:00:00' AS SmallDateTime), 0, 1, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (98, N'Beyond the Walls (Hors les murs)', N'Major laceration of heart with hemopericardium, sequela', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-11-20T00:00:00' AS SmallDateTime), 1, 2, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (99, N'Fierce Light: When Spirit Meets Action', N'Military operations involving unspecified weapon of mass destruction [WMD]', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-06-07T00:00:00' AS SmallDateTime), 0, 6, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (100, N'Cheech & Chong''s The Corsican Brothers', N'Unspecified fracture of right patella', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-06-07T00:00:00' AS SmallDateTime), 1, 9, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (101, N'Concepción Arenal, la visitadora de cárceles', N'Fracture of unspecified phalanx of left middle finger', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-09-23T00:00:00' AS SmallDateTime), 0, 6, 3)
GO
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (102, N'Saving General Yang', N'Poisoning by hemostatic drug, assault', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-03-19T00:00:00' AS SmallDateTime), 0, 3, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (103, N'Harvest', N'Activity, unspecified', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-11-21T00:00:00' AS SmallDateTime), 0, 6, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (104, N'Poison', N'Procreative counseling and advice using natural family planning', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-10-01T00:00:00' AS SmallDateTime), 0, 10, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (105, N'It''s Alive 2: It Lives Again', N'Poisoning by anticholinesterase agents, intentional self-harm, sequela', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-06-25T00:00:00' AS SmallDateTime), 0, 3, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (106, N'Private Eyes, The', N'Nondisplaced comminuted fracture of shaft of unspecified femur, initial encounter for open fracture type I or II', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-10-25T00:00:00' AS SmallDateTime), 0, 11, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (107, N'November', N'Other specified disorders of right middle ear and mastoid', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-03-21T00:00:00' AS SmallDateTime), 0, 3, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (108, N'Necessary Death, A', N'Unspecified superficial injury of unspecified wrist, initial encounter', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-06-05T00:00:00' AS SmallDateTime), 1, 2, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (109, N'7 Days in Havana', N'Other serum reaction due to vaccination, subsequent encounter', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-07-09T00:00:00' AS SmallDateTime), 1, 5, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (110, N'Farmageddon', N'Complete traumatic amputation of right hip and thigh, level unspecified, subsequent encounter', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-08-26T00:00:00' AS SmallDateTime), 1, 4, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (111, N'Whore', N'Anterior subcapsular polar infantile and juvenile cataract, right eye', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-06-27T00:00:00' AS SmallDateTime), 1, 6, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (112, N'Octopussy', N'Nondisplaced fracture of neck of second metacarpal bone, left hand, subsequent encounter for fracture with malunion', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-08-17T00:00:00' AS SmallDateTime), 1, 8, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (113, N'Vertigo', N'Displaced dome fracture of right talus, subsequent encounter for fracture with delayed healing', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-09-03T00:00:00' AS SmallDateTime), 1, 9, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (114, N'Casual Sex?', N'Loose body in unspecified elbow', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-07-06T00:00:00' AS SmallDateTime), 1, 3, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (115, N'Ju-on: The Beginning of the End (Ju-on: Owari no hajimari)', N'Puncture wound with foreign body of right front wall of thorax with penetration into thoracic cavity, sequela', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-09-14T00:00:00' AS SmallDateTime), 1, 5, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (116, N'Backfire', N'Other physeal fracture of lower end of unspecified tibia, initial encounter for closed fracture', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-09-09T00:00:00' AS SmallDateTime), 1, 7, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (117, N'TerrorVision', N'Laceration without foreign body of right front wall of thorax with penetration into thoracic cavity, initial encounter', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-04-13T00:00:00' AS SmallDateTime), 1, 6, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (118, N'Amityville Horror, The', N'Central pontine myelinolysis', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-01-22T00:00:00' AS SmallDateTime), 1, 6, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (119, N'Moomins on the Riviera (Muumit Rivieralla)', N'Burn of third degree of palm', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-07-26T00:00:00' AS SmallDateTime), 1, 6, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (120, N'All the Right Moves', N'Other superficial bite of other part of head, initial encounter', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-03-22T00:00:00' AS SmallDateTime), 1, 3, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (121, N'Sorceress', N'Toxic effect of other insecticides', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-01-29T00:00:00' AS SmallDateTime), 1, 2, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (122, N'Milka - A Film About Taboos (Milka - elokuva tabuista)', N'Family history of ischemic heart disease and other diseases of the circulatory system', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-11-26T00:00:00' AS SmallDateTime), 1, 10, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (123, N'Goon', N'Displaced fracture of olecranon process with intraarticular extension of right ulna, initial encounter for open fracture type IIIA, IIIB, or IIIC', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-06-14T00:00:00' AS SmallDateTime), 1, 1, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (124, N'Billy Two Hats (Lady and the Outlaw, The)', N'Osteonecrosis due to previous trauma, right toe(s)', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-05-24T00:00:00' AS SmallDateTime), 1, 2, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (125, N'Purpose', N'Other juvenile arthritis, elbow', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-04-03T00:00:00' AS SmallDateTime), 1, 8, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (126, N'Holy Smoke', N'Salter-Harris Type II physeal fracture of lower end of ulna, left arm, subsequent encounter for fracture with delayed healing', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-03-21T00:00:00' AS SmallDateTime), 1, 2, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (127, N'Hope and Glory', N'Other contact with goose, subsequent encounter', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-09-03T00:00:00' AS SmallDateTime), 1, 1, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (128, N'Kiss Before Dying, A', N'Presence of other orthopedic joint implants', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-05-11T00:00:00' AS SmallDateTime), 1, 5, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (129, N'Vice', N'Burn of first degree of left scapular region, subsequent encounter', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-10-30T00:00:00' AS SmallDateTime), 1, 9, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (130, N'Lore', N'Encounter for screening for nervous system disorders', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-08-19T00:00:00' AS SmallDateTime), 1, 7, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (131, N'TV Set, The', N'Cysts of unspecified eye, unspecified eyelid', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-05-27T00:00:00' AS SmallDateTime), 1, 5, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (132, N'When Will I Be Loved', N'Mixed simple and mucopurulent chronic bronchitis', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-01-07T00:00:00' AS SmallDateTime), 1, 1, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (133, N'A One-Way Trip to Antibes', N'Subluxation of metacarpophalangeal joint of unspecified thumb, sequela', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-03-18T00:00:00' AS SmallDateTime), 1, 11, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (134, N'Go-Getter, The', N'Postprocedural seroma of a circulatory system organ or structure following cardiac bypass', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-09-12T00:00:00' AS SmallDateTime), 1, 9, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (135, N'TV Set, The', N'Other superficial bite of unspecified great toe', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-12-15T00:00:00' AS SmallDateTime), 1, 5, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (136, N'Genevieve', N'Motorcycle driver injured in collision with heavy transport vehicle or bus in traffic accident, subsequent encounter', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-12-23T00:00:00' AS SmallDateTime), 1, 11, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (137, N'Three Quarter Moon', N'Cardiac arrest due to anesthesia during pregnancy, second trimester', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-06-21T00:00:00' AS SmallDateTime), 1, 8, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (138, N'London Conspiracy', N'Subluxation of distal interphalangeal joint of right middle finger, sequela', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-06-23T00:00:00' AS SmallDateTime), 1, 12, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (139, N'Hanging Tree, The', N'Displaced spiral fracture of shaft of left femur, sequela', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-02-22T00:00:00' AS SmallDateTime), 1, 3, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (140, N'Within the Woods', N'Non-pressure chronic ulcer of left thigh with unspecified severity', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-07-12T00:00:00' AS SmallDateTime), 1, 7, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (141, N'Dots', N'Cerebrospinal fluid leak from spinal puncture', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-12-31T00:00:00' AS SmallDateTime), 1, 12, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (142, N'Cameraman: The Life and Work of Jack Cardiff', N'Angioneurotic edema, initial encounter', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-01-10T00:00:00' AS SmallDateTime), 1, 1, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (143, N'La cravate', N'Other superficial bite of unspecified thumb, subsequent encounter', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-12-03T00:00:00' AS SmallDateTime), 1, 5, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (144, N'Ask the Dust', N'Unspecified car occupant injured in collision with other nonmotor vehicle in traffic accident', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-02-09T00:00:00' AS SmallDateTime), 1, 8, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (145, N'Goliath Awaits', N'Displaced osteochondral fracture of right patella, subsequent encounter for open fracture type IIIA, IIIB, or IIIC with nonunion', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-02-05T00:00:00' AS SmallDateTime), 1, 1, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (146, N'Torment', N'Smith''s fracture of unspecified radius, subsequent encounter for open fracture type IIIA, IIIB, or IIIC with routine healing', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-07-22T00:00:00' AS SmallDateTime), 1, 1, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (147, N'Reuben, Reuben', N'Displaced fracture of coracoid process, right shoulder, subsequent encounter for fracture with nonunion', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-06-12T00:00:00' AS SmallDateTime), 1, 11, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (148, N'Kurt Cobain: Montage of Heck', N'Puncture wound with foreign body of oral cavity, initial encounter', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-12-31T00:00:00' AS SmallDateTime), 1, 6, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (149, N'Evil Roy Slade', N'Unspecified open wound of abdominal wall, left upper quadrant without penetration into peritoneal cavity', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-04-30T00:00:00' AS SmallDateTime), 1, 6, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (150, N'I Don''t Want to Be a Man (Ich möchte kein Mann sein)', N'Multiple gestation', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-10-31T00:00:00' AS SmallDateTime), 1, 12, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (151, N'Winnie the Pooh', N'Other specified injury of intrinsic muscle, fascia and tendon of left thumb at wrist and hand level, sequela', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-09-07T00:00:00' AS SmallDateTime), 1, 10, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (152, N'Like Water for Chocolate (Como agua para chocolate)', N'External constriction of right front wall of thorax, initial encounter', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-09-23T00:00:00' AS SmallDateTime), 1, 12, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (153, N'Life of Emile Zola, The', N'Cerebral infarction due to thrombosis of left middle cerebral artery', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-07-25T00:00:00' AS SmallDateTime), 1, 10, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (154, N'Trap, The (Klopka)', N'Complications of attempted introduction of fertilized ovum following in vitro fertilization', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-08-05T00:00:00' AS SmallDateTime), 1, 2, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (155, N'Angel Named Billy, An', N'Laceration of extensor muscle, fascia and tendon of unspecified finger at wrist and hand level, sequela', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-01-06T00:00:00' AS SmallDateTime), 1, 1, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (156, N'5th Day of Peace (Dio è con noi)', N'Major laceration of thoracic aorta, initial encounter', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-03-15T00:00:00' AS SmallDateTime), 1, 7, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (157, N'Pathfinder (Ofelas)', N'Fall on same level from slipping, tripping and stumbling with subsequent striking against sharp glass, initial encounter', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-08-06T00:00:00' AS SmallDateTime), 1, 8, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (158, N'Harmful Insect (Gaichu)', N'Nondisplaced fracture of right radial styloid process, subsequent encounter for closed fracture with delayed healing', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-06-25T00:00:00' AS SmallDateTime), 1, 3, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (159, N'Gatekeepers, The', N'Typhoid osteomyelitis', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-11-16T00:00:00' AS SmallDateTime), 1, 3, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (160, N'Pocket Money', N'Malignant neoplasm of small intestine, unspecified', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-04-08T00:00:00' AS SmallDateTime), 1, 12, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (161, N'Passion of Darkly Noon, The', N'Metabolic acidemia in newborn', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-04-02T00:00:00' AS SmallDateTime), 1, 7, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (162, N'Hiding Out', N'Bitten by other nonvenomous reptiles, sequela', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-05-09T00:00:00' AS SmallDateTime), 1, 3, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (163, N'Fatso', N'Nondisplaced fracture of sternal end of right clavicle, subsequent encounter for fracture with delayed healing', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-06-19T00:00:00' AS SmallDateTime), 1, 5, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (164, N'Cloverfield', N'Anterior synechiae (iris)', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-07-06T00:00:00' AS SmallDateTime), 1, 5, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (165, N'Fool for Love', N'Toxic effect of phosphorus and its compounds, assault, subsequent encounter', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-01-06T00:00:00' AS SmallDateTime), 1, 2, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (166, N'Bob Funk', N'Nondisplaced spiral fracture of shaft of radius, unspecified arm, subsequent encounter for open fracture type IIIA, IIIB, or IIIC with delayed healing', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-11-26T00:00:00' AS SmallDateTime), 1, 6, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (167, N'Steam of Life (Miesten vuoro)', N'Corrosion of second degree of multiple sites of lower limb, except ankle and foot', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-01-19T00:00:00' AS SmallDateTime), 1, 4, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (168, N'Pax Americana and the Weaponization of Space', N'Unspecified open wound, right thigh, sequela', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-12-07T00:00:00' AS SmallDateTime), 1, 12, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (169, N'Going to Kansas City', N'Puncture wound without foreign body of right middle finger without damage to nail, initial encounter', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-04-16T00:00:00' AS SmallDateTime), 1, 10, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (170, N'Musketeers of Pig Alley, The', N'Asymptomatic human immunodeficiency virus [HIV] infection status', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-09-19T00:00:00' AS SmallDateTime), 1, 10, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (171, N'World''s Fastest Indian, The', N'Puncture wound with foreign body of right middle finger with damage to nail', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-07-23T00:00:00' AS SmallDateTime), 1, 5, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (172, N'Goya''s Ghosts', N'Malignant melanoma of lip', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-11-10T00:00:00' AS SmallDateTime), 1, 5, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (173, N'Birder''s Guide to Everything, A', N'Other tear of medial meniscus, current injury, left knee, subsequent encounter', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-09-19T00:00:00' AS SmallDateTime), 1, 1, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (174, N'Gable: The King Remembered', N'Frostbite with tissue necrosis of nose, sequela', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-10-10T00:00:00' AS SmallDateTime), 1, 10, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (175, N'Shot in the Heart', N'Implantation cysts of iris, ciliary body or anterior chamber, bilateral', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-04-22T00:00:00' AS SmallDateTime), 1, 9, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (176, N'Murder by Death', N'Other mechanical complication of indwelling ureteral stent, subsequent encounter', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-08-18T00:00:00' AS SmallDateTime), 1, 9, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (177, N'Judge Priest', N'Pedal cycle passenger injured in collision with other and unspecified motor vehicles in nontraffic accident', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-06-13T00:00:00' AS SmallDateTime), 1, 7, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (178, N'Mad Masters, The (Les maîtres fous)', N'Unspecified injury of unspecified blood vessel at hip and thigh level', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-08-25T00:00:00' AS SmallDateTime), 1, 12, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (179, N'Christmas Comes but Once a Year', N'Nondisplaced fracture of neck of scapula, right shoulder, initial encounter for open fracture', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-04-14T00:00:00' AS SmallDateTime), 1, 2, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (180, N'Beau Pere (a.k.a. Stepfather) (Beau-père)', N'Person boarding or alighting from bus injured in collision with heavy transport vehicle or bus, sequela', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-03-20T00:00:00' AS SmallDateTime), 1, 6, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (181, N'Dirty Ho (Lan tou He)', N'Polyp of stomach and duodenum', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-07-08T00:00:00' AS SmallDateTime), 1, 2, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (182, N'Lightning Bolt: The Power of Salad', N'Corneal staphyloma, right eye', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-11-22T00:00:00' AS SmallDateTime), 1, 6, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (183, N'Live Flesh (Carne trémula)', N'Unspecified reduction defect of unspecified lower limb', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-12-07T00:00:00' AS SmallDateTime), 1, 2, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (184, N'Power Play', N'Displaced fracture of neck of first metacarpal bone, left hand, sequela', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-09-17T00:00:00' AS SmallDateTime), 1, 9, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (185, N'My Life as a Dog (Mitt liv som hund)', N'Adverse effect of immunoglobulin, subsequent encounter', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-06-18T00:00:00' AS SmallDateTime), 1, 5, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (186, N'Good Shepherd, The', N'Unspecified infection of urinary tract in pregnancy', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-08-14T00:00:00' AS SmallDateTime), 1, 10, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (187, N'Yesterday Girl (Abschied von gestern - Anita G.)', N'Blister (nonthermal) of left elbow, initial encounter', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-08-05T00:00:00' AS SmallDateTime), 1, 5, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (188, N'Booker''s Place: A Mississippi Story', N'Disorder of vein, unspecified', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-12-27T00:00:00' AS SmallDateTime), 1, 3, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (189, N'Dear Me', N'Bent bone of left ulna, sequela', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-10-08T00:00:00' AS SmallDateTime), 1, 6, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (190, N'Shadows (Cienie)', N'Unspecified open wound of unspecified ear, initial encounter', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-10-10T00:00:00' AS SmallDateTime), 1, 5, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (191, N'Tom at the Farm (Tom à la ferme)', N'Toxic effect of other corrosive organic compounds, intentional self-harm', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-06-09T00:00:00' AS SmallDateTime), 1, 12, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (192, N'Vampires, Les', N'Laceration without foreign body of abdominal wall, right lower quadrant with penetration into peritoneal cavity, subsequent encounter', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-02-10T00:00:00' AS SmallDateTime), 1, 2, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (193, N'Extreme Justice', N'Sepsis of newborn due to other staphylococci', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-12-26T00:00:00' AS SmallDateTime), 1, 9, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (194, N'Kummeli V', N'Displaced fracture of proximal third of navicular [scaphoid] bone of left wrist', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-01-16T00:00:00' AS SmallDateTime), 1, 7, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (195, N'Rabbit Test', N'Infective myositis, left toe(s)', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2022-04-28T00:00:00' AS SmallDateTime), 1, 11, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (196, N'Without Pity', N'Displaced supracondylar fracture with intracondylar extension of lower end of unspecified femur, subsequent encounter for closed fracture with nonunion', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-06-14T00:00:00' AS SmallDateTime), 1, 10, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (197, N'Provocateur (Prowokator)', N'Dislocation of interphalangeal joint of unspecified toe(s), initial encounter', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-06-20T00:00:00' AS SmallDateTime), 1, 12, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (198, N'Summer Palace (Yihe yuan)', N'Poisoning by other specified systemic anti-infectives and antiparasitics, undetermined', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-03-12T00:00:00' AS SmallDateTime), 1, 4, 3)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (199, N'Japanese Summer: Double Suicide (Muri shinjû: Nihon no natsu)', N'Subluxation of metacarpophalangeal joint of left middle finger, initial encounter', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-02-02T00:00:00' AS SmallDateTime), 1, 10, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (200, N'Lovely, Still', N'Other fracture of upper end of right radius, initial encounter for open fracture type IIIA, IIIB, or IIIC', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-12-19T00:00:00' AS SmallDateTime), 1, 5, 4)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (201, N'Triad Election (Election 2) (Hak se wui yi wo wai kwai)', N'Complete loss of teeth due to caries, class I', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-04-08T00:00:00' AS SmallDateTime), 1, 4, 2)
GO
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (202, N'Space Milkshake', N'Contact with liquid air, initial encounter', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2022-01-29T00:00:00' AS SmallDateTime), 1, 2, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (203, N'Tears of the Sun', N'Strain of intrinsic muscle, fascia and tendon of left index finger at wrist and hand level, subsequent encounter', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-10-20T00:00:00' AS SmallDateTime), 1, 8, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (204, N'Lacombe Lucien', N'Cyst of ora serrata, right eye', N'post/post10.jpg                                                                                                                                                                                                                                                                                             ', CAST(N'2021-12-09T00:00:00' AS SmallDateTime), 1, 1, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (205, N'North West Frontier', N'Laceration of other specified muscles and tendons at ankle and foot level, left foot, initial encounter', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-12-13T00:00:00' AS SmallDateTime), 1, 11, 2)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (206, N'What to Expect When You''re Expecting', N'Passenger on bus injured in collision with pedal cycle in traffic accident', N'post/post5.jpg                                                                                                                                                                                                                                                                                              ', CAST(N'2021-08-14T00:00:00' AS SmallDateTime), 1, 11, 1)
INSERT [dbo].[Post] ([postID], [postTitle], [postDetail], [postImg], [postDate], [onHomepage], [c_id], [a_id]) VALUES (211, N'What’s MyPlate? Learn About These Dietary Guidelines', N'<p>Since 1916, the United States Department of Agriculture (USDA) has been providing science-based guidance on healthy eating choices. What we learned as the food pyramid in the 1990s and 2000s has been replaced with MyPlate.</p>
<p>MyPlate focuses on:</p>
<ul>
<li style="text-align: center;">Making small changes in your diet for healthier living</li>
<li style="text-align: center;">Including all healthy foods and beverages in your diet</li>
<li style="text-align: center;">Creating a&nbsp;<a href="https://blog.bonsecours.com/healthy/meal-prepping-tips/">healthy-eating lifestyle</a>&nbsp;to fit each stage of life</li>
</ul>
<h3><span style="font-family: ''times new roman'', times, serif;"><strong>What&rsquo;s MyPlate?</strong></span></h3>
<p>MyPlate is an easy way to see what foods you should eat to get the nutrition your body needs to stay healthy. It shows you how much you should eat each day of:</p>
<ul>
<li style="text-align: center;">Dairy</li>
<li style="text-align: center;">Fruits</li>
<li style="text-align: center;">Grains</li>
<li style="text-align: center;">Vegetables</li>
<li style="text-align: center;">Protein foods</li>
</ul>
<p>Half of your plate should be filled with fruits and vegetables. The other half is grains and protein foods. Dairy is on the side as a serving next to your plate. MyPlate also focuses on choosing foods and drinks that are low in sodium, saturated fats, and added sugars.</p>
<h3><span style="color: rgb(45, 194, 107);"><strong>How to build a healthy plate</strong></span></h3>
<p><strong><img style="display: block; margin-left: auto; margin-right: auto;" src="https://sf1.mariefranceasia.com/wp-content/uploads/sites/7/2016/09/food-fitness-fashion-615x410.jpg"></strong></p>
<p>To build your healthy plate, focus on variety, nutrition and amount. Also, keep the following tips in mind.</p>
<p><strong>Vary your veggies.</strong>&nbsp;Vegetables are full of minerals and vitamins. You can eat them cooked or raw and mix them in other foods such as omelets and wraps. Vegetables are grouped into five types, and eating healthily means choosing from each group:</p>
<ul>
<li>Starchy vegetables, like corn, potatoes and parsnips</li>
<li>Red and orange vegetables, such as carrots, tomatoes and squash</li>
<li>Beans and peas, such as black-eyed peas, kidney beans and lentils</li>
<li>Dark green vegetables, including broccoli, spinach and romaine lettuce</li>
<li>Other vegetables, including avocado, cucumbers, green beans and zucchini</li>
</ul>
<p><strong>Focus on whole fruits.</strong>&nbsp;When you eat whole fruits instead of just the juice, you get the added benefits of fiber as well as more vitamins and minerals. Fruits are healthy substitutes for sweet snacks and eating them may lower your risk of some diseases. Fruits that are fresh, frozen, dried, or canned count as part of your healthy plate. For variety, choose fresh fruits when they&rsquo;re in season, such as strawberries in the spring and apples in the fall.</p>
<p><strong>Make half your grains whole grains.</strong>&nbsp;Whole grains are wheat, rice, oats, corn, and other grains in their natural form. They&rsquo;re healthier than refined grains because they contain all three parts of the nutritious seed &mdash; the bran, germ, and endosperm. Refined or enriched grains only have the endosperm, so they&rsquo;ve lost 25 percent of their protein and over 17 nutrients. You can find whole grains by looking for the &ldquo;100% Whole Grain&rdquo; stamp or words in the ingredients list such as &ldquo;whole grain,&rdquo; &ldquo;whole wheat&rdquo; and &ldquo;brown rice.&rdquo;</p>
<p><strong>Move to low-fat or fat-free dairy.</strong>&nbsp;Dairy products are rich in vitamin D and calcium, which help your body build and maintain strong bones. However, choosing low-fat or fat-free dairy can keep you healthier. Dairy that&rsquo;s high in saturated fat raises the &ldquo;bad&rdquo; cholesterol in your bloodstream, which can lead to heart problems.</p>
<p>Dairy foods on your healthy plate include:</p>
<ul>
<li>Milk</li>
<li>Yogurt</li>
<li>Cheese</li>
<li>Fortified soymilk</li>
<li>Non-dairy foods rich in calcium, such as fortified cereals, canned fish and almond milk</li>
</ul>
<p><strong>Vary your protein.</strong>&nbsp;Foods rich in protein are essential for healthy muscles, skin, bones, cartilage, and blood. Protein foods also contain important vitamins and minerals. To get the right amounts of nutrients, choose a variety of proteins:</p>
<ul>
<li>Eggs</li>
<li>Seafood, including salmon, tuna and shellfish</li>
<li>Meats, such as lean beef, pork and game meats</li>
<li>Poultry, like chicken, turkey and Cornish game hen</li>
<li><a href="https://blog.bonsecours.com/healthy/protein-without-meat/">Beans and peas</a>, including chickpeas, refried beans and tofu</li>
<li>Nuts and seeds, such as walnuts, peanut butter and sunflower seeds</li>
</ul>
<p>Additionally, the&nbsp;<a href="https://www.choosemyplate.gov/">USDA&rsquo;s website</a>&nbsp;has handy tools that helps you build your healthy plate according to your age, height, weight, and activity level. Once you&rsquo;ve determined your recommended daily calorie intake, you&rsquo;ll get guidelines on how many cups or ounces of the five food groups you should</p>
<p><strong>Learn more about the&nbsp;<a href="https://www.bonsecours.com/health-care-services/nutrition">nutrition services</a>&nbsp;offered at Bon Secours.&nbsp;</strong></p>', N'post/BSC_HealthyPlate_1200x628_Revised-1200x610.jpg                                                                                                                                                                                                                                                         ', CAST(N'2022-06-16T00:00:00' AS SmallDateTime), 1, 2, 1)
SET IDENTITY_INSERT [dbo].[Post] OFF
GO
INSERT [dbo].[Role] ([r_id], [r_name]) VALUES (1, N'Admin')
INSERT [dbo].[Role] ([r_id], [r_name]) VALUES (2, N'Manager')
INSERT [dbo].[Role] ([r_id], [r_name]) VALUES (3, N'Receptionist')
INSERT [dbo].[Role] ([r_id], [r_name]) VALUES (4, N'Doctor')
INSERT [dbo].[Role] ([r_id], [r_name]) VALUES (5, N'Customer')
GO
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (1, 6)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (1, 7)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (1, 8)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (1, 12)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (1, 32)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (1, 33)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (1, 34)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (1, 35)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (1, 36)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (2, 7)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (2, 8)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (2, 12)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (2, 14)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (2, 15)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (2, 16)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (2, 17)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (2, 18)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (2, 24)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (2, 26)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (2, 27)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (2, 30)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (2, 34)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (2, 39)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (2, 41)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (2, 42)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (2, 44)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (2, 45)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (2, 46)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (2, 48)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (2, 49)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (2, 50)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (2, 51)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (2, 52)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (2, 53)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (2, 54)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (3, 39)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (3, 46)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (4, 40)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (5, 1)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (5, 2)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (5, 3)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (5, 7)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (5, 8)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (5, 12)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (5, 19)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (5, 20)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (5, 21)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (5, 22)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (5, 23)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (5, 31)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (5, 34)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (5, 37)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (5, 38)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (5, 43)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (5, 47)
INSERT [dbo].[Role_Feature] ([r_id], [f_id]) VALUES (5, 55)
GO
SET IDENTITY_INSERT [dbo].[Service] ON 

INSERT [dbo].[Service] ([id], [name], [status], [original_price], [sale_price], [t_id], [details], [updated_date], [photo]) VALUES (1, N'Tim Mạch', 1, 200000.0000, 20000.0000, 1, N'kiểm tra trạng thái của tim, và các bệnh liên quan đến tim như tim bẩm sinh, hở van tim,...', CAST(N'2022-11-08' AS Date), N'frontend/assert/img/service/about.jpg')
INSERT [dbo].[Service] ([id], [name], [status], [original_price], [sale_price], [t_id], [details], [updated_date], [photo]) VALUES (2, N'Tiêu Hóa', 1, 300000.0000, 20000.0000, 1, N'các vấn đề liên quan đến tiêu hóa( đau bụng, đi ngoài, đau ruột thừa,..)', CAST(N'2022-11-08' AS Date), N'frontend/assert/img/service/about.jpg')
INSERT [dbo].[Service] ([id], [name], [status], [original_price], [sale_price], [t_id], [details], [updated_date], [photo]) VALUES (3, N'Truyền nhiễm', 1, 150000.0000, 20000.0000, 1, N'các vấn đề liên quan đến truyền nhiễm( qua đường máu, do virus hoặc vi khuẩn,...)', CAST(N'2022-11-08' AS Date), N'frontend/assert/img/service/about.jpg')
INSERT [dbo].[Service] ([id], [name], [status], [original_price], [sale_price], [t_id], [details], [updated_date], [photo]) VALUES (4, N'Chấn Thương, Chỉnh Hình', 1, 200000.0000, 20000.0000, 2, N'các vấn đề liên quan đến xương khớp(chỉnh xương gãy)', CAST(N'2022-11-08' AS Date), N'frontend/assert/img/service/about.jpg')
INSERT [dbo].[Service] ([id], [name], [status], [original_price], [sale_price], [t_id], [details], [updated_date], [photo]) VALUES (5, N'Chấn Thương Sọ Não', 1, 300000.0000, 20000.0000, 2, N'các vấn đề liên quan đến tổn thương sọ não', CAST(N'2022-11-08' AS Date), N'frontend/assert/img/service/about.jpg')
INSERT [dbo].[Service] ([id], [name], [status], [original_price], [sale_price], [t_id], [details], [updated_date], [photo]) VALUES (6, N'Chấn Thương Bụng', 1, 400000.0000, 20000.0000, 2, N'các vấn đề liên quan đến bụng, nội tạng(vỡ gan, vỡ thận, vỡ lách,...)', CAST(N'2022-11-08' AS Date), N'frontend/assert/img/service/about.jpg')
INSERT [dbo].[Service] ([id], [name], [status], [original_price], [sale_price], [t_id], [details], [updated_date], [photo]) VALUES (7, N'Mắt', 1, 200000.0000, 20000.0000, 3, N'các vấn đề liên quan đến mắt', CAST(N'2022-11-08' AS Date), N'frontend/assert/img/service/about.jpg')
INSERT [dbo].[Service] ([id], [name], [status], [original_price], [sale_price], [t_id], [details], [updated_date], [photo]) VALUES (8, N'Tai Mũi họng', 1, 300000.0000, 20000.0000, 3, N'các vấn đề liên quan đến tai mũi họng', CAST(N'2022-11-08' AS Date), N'frontend/assert/img/service/about.jpg')
INSERT [dbo].[Service] ([id], [name], [status], [original_price], [sale_price], [t_id], [details], [updated_date], [photo]) VALUES (9, N'Răng Hàm Mặt', 1, 200000.0000, 20000.0000, 3, N'Các vấn đề liên quan đến răng hàm mặt', CAST(N'2022-11-08' AS Date), N'frontend/assert/img/service/about.jpg')
INSERT [dbo].[Service] ([id], [name], [status], [original_price], [sale_price], [t_id], [details], [updated_date], [photo]) VALUES (10, N'Xquang', 1, 3000000.0000, 20000.0000, 1, N'Các vấn đề về xương, hình ảnh nội tạng,khối u,...', CAST(N'2022-07-19' AS Date), N'frontend\assert\img\service\team-5.jpg')
INSERT [dbo].[Service] ([id], [name], [status], [original_price], [sale_price], [t_id], [details], [updated_date], [photo]) VALUES (11, N'Siêu Âm ', 1, 250000.0000, 20000.0000, 4, N'Các vấn đề về nội tạng', CAST(N'2022-11-08' AS Date), N'frontend/assert/img/service/about.jpg')
INSERT [dbo].[Service] ([id], [name], [status], [original_price], [sale_price], [t_id], [details], [updated_date], [photo]) VALUES (12, N'Thần kinh', 0, 500000.0000, 20000.0000, 1, N'Các bệnh liên quan đến thần kinh ( thắt dây thần kinh,  động kinh, co dây thần kinh tủy,..)', CAST(N'2022-11-08' AS Date), N'frontend/assert/img/service/about.jpg')
SET IDENTITY_INSERT [dbo].[Service] OFF
GO
SET IDENTITY_INSERT [dbo].[Service_Type] ON 

INSERT [dbo].[Service_Type] ([t_id], [t_name]) VALUES (1, N'Internal Medicine')
INSERT [dbo].[Service_Type] ([t_id], [t_name]) VALUES (2, N'Surgery')
INSERT [dbo].[Service_Type] ([t_id], [t_name]) VALUES (3, N'Specialist')
INSERT [dbo].[Service_Type] ([t_id], [t_name]) VALUES (4, N'Paraclinical')
SET IDENTITY_INSERT [dbo].[Service_Type] OFF
GO
SET IDENTITY_INSERT [dbo].[Slide] ON 

INSERT [dbo].[Slide] ([slideID], [slideTitle], [slideDetail], [slideImg], [slideDate], [onHomepage]) VALUES (1, N'make it complicated', N'lorem loreraslkjflkwjrna', N'https://img.thuthuatphanmem.vn/uploads/2018/10/19/nice-wallpaper-4k_040153771.jpg                                                                                                                                                                                                                           ', CAST(N'2022-05-21T12:19:00' AS SmallDateTime), 1)
INSERT [dbo].[Slide] ([slideID], [slideTitle], [slideDetail], [slideImg], [slideDate], [onHomepage]) VALUES (2, N'make it complicated', N'lorem loreraslkjflkwjrna', N'https://img.thuthuatphanmem.vn/uploads/2018/10/19/nice-wallpaper-4k_040153771.jpg                                                                                                                                                                                                                           ', CAST(N'2022-05-19T22:40:00' AS SmallDateTime), 1)
INSERT [dbo].[Slide] ([slideID], [slideTitle], [slideDetail], [slideImg], [slideDate], [onHomepage]) VALUES (3, N'make it complicated', N'lorem loreraslkjflkwjrna', N'https://img.thuthuatphanmem.vn/uploads/2018/10/19/nice-wallpaper-4k_040153771.jpg                                                                                                                                                                                                                           ', CAST(N'2022-05-21T12:19:00' AS SmallDateTime), 1)
INSERT [dbo].[Slide] ([slideID], [slideTitle], [slideDetail], [slideImg], [slideDate], [onHomepage]) VALUES (4, N'make it complicated', N'lorem loreraslkjflkwjrna', N'https://img.thuthuatphanmem.vn/uploads/2018/10/19/nice-wallpaper-4k_040153771.jpg                                                                                                                                                                                                                           ', CAST(N'2022-05-21T12:19:00' AS SmallDateTime), 1)
INSERT [dbo].[Slide] ([slideID], [slideTitle], [slideDetail], [slideImg], [slideDate], [onHomepage]) VALUES (5, N'make it complicated', N'lorem loreraslkjflkwjrna', N'https://img.thuthuatphanmem.vn/uploads/2018/10/19/nice-wallpaper-4k_040153771.jpg                                                                                                                                                                                                                           ', CAST(N'2022-05-21T12:19:00' AS SmallDateTime), 1)
INSERT [dbo].[Slide] ([slideID], [slideTitle], [slideDetail], [slideImg], [slideDate], [onHomepage]) VALUES (6, N'make it complicated', N'lorem loreraslkjflkwjrna', N'https://img.thuthuatphanmem.vn/uploads/2018/10/19/nice-wallpaper-4k_040153771.jpg                                                                                                                                                                                                                           ', CAST(N'2022-05-21T12:19:00' AS SmallDateTime), 1)
SET IDENTITY_INSERT [dbo].[Slide] OFF
GO
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'anhvhhs153214@fpt.edu.vn', N'Ha Anh', 0, N'0349019012', N'Ha Noi', CAST(N'2001-11-11' AS Date), N'photo\team-5.jpg', N'Thonguyen123', 1, 4)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'doctor1@gmail.com', N'Orv Aizik', 0, N'511 681 4952', N'Quevedo', CAST(N'1990-01-04' AS Date), N'photo\team-1.jpg', N'Thonguyen123', 1, 4)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'doctor10@gmail.com', N'Kiele Towse', 0, N'784 639 2967', N'Krasnolesnyy', CAST(N'1989-12-11' AS Date), N'photo\team-5.jpg', N'Thonguyen123', 1, 4)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'doctor11@gmail.com', N'Zak Ryle', 0, N'579 765 3255', N'Banjar Kertasari', CAST(N'1966-09-05' AS Date), N'photo\team-1.jpg', N'Thonguyen123', 1, 4)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'doctor12@gmail.com', N'Fremont Ramas', 1, N'169 889 3625', N'Kuching', CAST(N'1978-03-05' AS Date), N'photo\team-2.jpg', N'Thonguyen123', 1, 4)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'doctor13@gmail.com', N'Lacey Bowld', 1, N'195 931 7492', N'Chengxiang', CAST(N'1966-10-03' AS Date), N'photo\team-3.jpg', N'Thonguyen123', 1, 4)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'doctor14@gmail.com', N'Vergil Oiseau', 1, N'369 282 8585', N'Lac La Biche', CAST(N'1988-02-20' AS Date), N'photo\team-4.jpg', N'Thonguyen123', 1, 4)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'doctor15@gmail.com', N'Alric Hugo', 1, N'817 546 5583', N'Iz¯eh', CAST(N'1971-10-26' AS Date), N'photo\team-5.jpg', N'Thonguyen123', 1, 4)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'doctor16@gmail.com', N'Westbrook Wannop', 0, N'651 195 7433', N'Senggreng', CAST(N'1984-11-25' AS Date), N'photo\team-1.jpg', N'Thonguyen123', 1, 4)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'doctor17@gmail.com', N'Baudoin Gladden', 1, N'685 998 7654', N'Prostki', CAST(N'1968-03-08' AS Date), N'photo\team-2.jpg', N'Thonguyen123', 1, 4)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'doctor18@gmail.com', N'Bryanty Pottie', 0, N'209 659 1181', N'Nogueira do Cravo', CAST(N'1976-08-08' AS Date), N'photo\team-3.jpg', N'Thonguyen123', 1, 4)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'doctor19@gmail.com', N'Edi Besnard', 0, N'817 239 2699', N'Fort Worth', CAST(N'1987-07-21' AS Date), N'photo\team-4.jpg', N'Thonguyen123', 1, 4)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'doctor2@gmail.com', N'Irena Scardifield', 0, N'102 613 1416', N'Zalesnoye', CAST(N'1984-08-09' AS Date), N'photo\team-2.jpg', N'Thonguyen123', 1, 4)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'doctor20@gmail.com', N'Tait Mc Caughan', 0, N'711 415 3573', N'Nanpu', CAST(N'1986-04-28' AS Date), N'photo\team-5.jpg', N'Thonguyen123', 1, 4)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'doctor3@gmail.com', N'Gillie Grece', 1, N'643 986 4910', N'Gjirokastër', CAST(N'1982-03-20' AS Date), N'photo\team-3.jpg', N'Thonguyen123', 1, 4)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'doctor4@gmail.com', N'Arlan Klimke', 0, N'552 204 3164', N'Libode', CAST(N'1973-02-23' AS Date), N'photo\team-4.jpg', N'Thonguyen123', 1, 4)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'doctor5@gmail.com', N'Abra Crooks', 1, N'586 149 6523', N'Santa Fe', CAST(N'1986-08-29' AS Date), N'photo\team-5.jpg', N'Thonguyen123', 1, 4)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'doctor6@gmail.com', N'Zola Burkwood', 0, N'186 365 0113', N'Clifden', CAST(N'1977-04-25' AS Date), N'photo\team-1.jpg', N'Thonguyen123', 1, 4)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'doctor7@gmail.com', N'Fallon Nayshe', 0, N'278 552 8248', N'Stari Bar', CAST(N'1971-07-28' AS Date), N'photo\team-2.jpg', N'Thonguyen123', 1, 4)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'doctor8@gmail.com', N'Viviene Greensted', 1, N'542 221 0568', N'Genova', CAST(N'1969-03-24' AS Date), N'photo\team-3.jpg', N'Thonguyen123', 1, 4)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'doctor9@gmail.com', N'Ludovika Nerval', 1, N'371 880 1113', N'Maunggora', CAST(N'1981-02-16' AS Date), N'photo\team-4.jpg', N'Thonguyen123', 1, 4)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'hihi@gmail.com', N'hihi', 1, N'0349019012', N'HaNoi', CAST(N'2001-02-22' AS Date), N'photo\kthedup..jpg', N'Thonguyen123', 1, 5)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'huhuihi@gmail.com', N'Th odz', 1, N'0349019011', N'My luong', CAST(N'2022-06-26' AS Date), N'frontend\assert\img\FB_IMG_1609264041632.jpg', N'Thonguyen123', 1, 5)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'mai', N'mai', 0, N'0349019012', N'hanoi', CAST(N'2001-01-01' AS Date), N'photo\WIN_20210511_09_45_34_Pro.jpg', N'Thonguyen123', 1, 5)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'mai1@gmail.com', N'Ban Mai', 0, N'0862791104', N'Bac Giang', CAST(N'2001-04-11' AS Date), N'photo\WIN_20210511_09_45_34_Pro.jpg', N'Thonguyen123', 1, 5)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'mai2@gmail.com', N'Ban Mai', 1, N'0862791104', N'Ha Noi', CAST(N'2011-04-11' AS Date), N'photo\WIN_20210511_09_45_34_Pro.jpg', N'Thonguyen123', 1, 5)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'mai3@gmail.com', N'Linh Anh', 1, N'0862791104', N'Ha Nam', CAST(N'2011-04-11' AS Date), N'photo\WIN_20210511_09_45_34_Pro.jpg', N'Thonguyen123', 1, 5)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'mai4@gmail.com', N'Linh Trang', 1, N'0862791104', N'Ha Tinh', CAST(N'2011-04-11' AS Date), N'photo\WIN_20210511_09_45_34_Pro.jpg', N'Thonguyen123', 1, 5)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'mai5@gmail.com', N'Kieu Trang', 1, N'0862791104', N'Ho Chi Minh', CAST(N'2011-04-11' AS Date), N'photo\WIN_20210511_09_45_34_Pro.jpg', N'Thonguyen123', 1, 5)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'mai6@gmail.com', N'Kieu Vu', 1, N'0862791104', N'Ho Chi Minh', CAST(N'2011-04-11' AS Date), N'photo\WIN_20210511_09_45_34_Pro.jpg', N'Thonguyen123', 1, 5)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'mai7@gmail.com', N'Lam Thao', 1, N'0862791104', N'Ha Giang', CAST(N'2011-04-11' AS Date), N'photo\WIN_20210511_09_45_34_Pro.jpg', N'Thonguyen123', 1, 5)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'mai8@gmail.com', N'Tuan Anh', 1, N'0862791104', N'Ha Giang', CAST(N'2011-04-11' AS Date), N'photo\WIN_20210511_09_45_34_Pro.jpg', N'Thonguyen123', 1, 5)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'mai9@gmail.com', N'Trang Linh', 1, N'0862791104', N'Ha Giang', CAST(N'2011-04-11' AS Date), N'photo\WIN_20210511_09_45_34_Pro.jpg', N'Thonguyen123', 1, 5)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'maivtbhe153254@fpt.edu.vn', N'Ban Mai', 0, N'0349019012', N'Ha Noi', CAST(N'2011-11-11' AS Date), N'photo\WIN_20210511_09_45_34_Pro.jpg', N'Thonguyen123', 1, 1)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'nampxhhe151338@fpt.edu.vn', N'Nam beo', 1, N'0916359969', N'My luong', CAST(N'2001-06-24' AS Date), N'photo\team-5.jpg', N'Thonguyen123', 1, 4)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'nghianvhe151124@fpt.edu.vn', N'Nguyen Nghia', 1, N'0349019012', N'Ha Noi', CAST(N'2001-11-01' AS Date), N'photo\WIN_20210511_09_45_34_Pro.jpg', N'Thonguyen123', 1, 2)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'nguyenquevip123@gmail.com', N'aa  a ', 1, N'0349019012', N'My luong', CAST(N'2022-06-26' AS Date), N'frontend\assert\img\FB_IMG_1608362164363.jpg', N'Thonguyen123', 1, 5)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'sonnkhe151341@fpt.edu.vn', N'Khac Sonnk', 1, N'0349019012', N'Ha Noi', CAST(N'2001-11-11' AS Date), N'photo\WIN_20210511_09_45_34_Pro.jpg', N'Thonguyen123', 1, 5)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'tho', N'Tho Dinh', 1, N'0349019012', N'hanoi', CAST(N'2001-12-17' AS Date), N'photo\team-1.jpg', N'Thonguyen123', 1, 4)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'thondhe@fpt.edu.vn', N'Nguyen Dinh Tho', 1, N'0349019012', N'Ha Noi', CAST(N'2001-11-11' AS Date), N'photo\team-2.jpg', N'Thonguyen123', 1, 4)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'thondhe1150527@fpt.edu.vn', N'Dung Bui', 1, N'0349019012', N'My luong', CAST(N'2022-06-26' AS Date), N'photo\team-3.jpg', N'Thonguyen123', 1, 4)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'thondhe1501527@fpt.edu.vn', N'Thodz', 1, N'0349019012', N'My luong', CAST(N'2022-06-26' AS Date), N'frontend\assert\img\FB_IMG_1609264041632.jpg', N'Thonguyen123', 1, 5)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'thondhe150527@fpt.edu.vn', N'Tho Nguyen', 1, N'0349019013', N'Ha Tay', CAST(N'1111-11-11' AS Date), N'photo\120087183_1552115428323536_3548435545460090371_n.jpg', N'Thonguyen123', 1, 3)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'thondhe2@fpt.edu.vn', N'Tho Two', 1, N'0349019012', N'Ha Noi', CAST(N'2001-11-11' AS Date), N'photo\kthedup..jpg', N'Thonguyen123', 1, 5)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'tkokoi11223@gmail.com', N'abczxcv', 1, N'0349019012', N'My luong', CAST(N'2022-06-27' AS Date), N'frontend\assert\img\FB_IMG_1608362164363.jpg', N'Thonguyen123', 1, 5)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'tkokoi1123@gmail.com', N'Tho', 1, N'0349019012', N'My luong', CAST(N'2022-06-27' AS Date), N'frontend\assert\img\FB_IMG_1608362164363.jpg', N'Thonguyen123', 1, 5)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'tkokoi123@gmail.com', N'Tho Nguyen', 1, N'0349019012', N'Ha Noi', CAST(N'1111-11-11' AS Date), N'photo\oai_pa.jpg', N'Thonguyen123', 1, 5)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'tkokoi12312@gmail.com', N'abc a', 1, N'0349019012', N'My luong', CAST(N'2022-06-26' AS Date), N'frontend\assert\img\FB_IMG_1608509818282.jpg', N'Thonguyen123', 1, 5)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'tkokoi12332@gmail.com', N'ABc', 1, N'0349019012', N'My luong', CAST(N'2022-06-07' AS Date), N'frontend\assert\img\FB_IMG_1623455142520.jpg', N'Thonguyen123', 1, 5)
INSERT [dbo].[User] ([email], [name], [gender], [phone], [address], [dob], [photo], [password], [status], [r_id]) VALUES (N'tungnguyenchi289@gmail.com', N'Tung', 1, N'0931981368', N'HungYen', CAST(N'2001-09-28' AS Date), N'photo\team-4.jpg', N'Thonguyen123', 1, 4)
GO
ALTER TABLE [dbo].[medicine] ADD  CONSTRAINT [DF_medicine_inUse]  DEFAULT ((1)) FOR [inUse]
GO
ALTER TABLE [dbo].[Order] ADD  CONSTRAINT [DF_Order_status]  DEFAULT ((0)) FOR [status]
GO
ALTER TABLE [dbo].[Order_detail] ADD  CONSTRAINT [DF_Order_detail_medical_Status]  DEFAULT ((0)) FOR [medical_Status]
GO
ALTER TABLE [dbo].[Post] ADD  CONSTRAINT [DF__Post__onHomepage__17F790F9]  DEFAULT ((0)) FOR [onHomepage]
GO
ALTER TABLE [dbo].[Slide] ADD  CONSTRAINT [DF__Slide__onHomepag__5441852A]  DEFAULT ((0)) FOR [onHomepage]
GO
ALTER TABLE [dbo].[Cart]  WITH CHECK ADD  CONSTRAINT [FK_Cart_Service] FOREIGN KEY([s_id])
REFERENCES [dbo].[Service] ([id])
GO
ALTER TABLE [dbo].[Cart] CHECK CONSTRAINT [FK_Cart_Service]
GO
ALTER TABLE [dbo].[Cart]  WITH CHECK ADD  CONSTRAINT [FK_Cart_User] FOREIGN KEY([email])
REFERENCES [dbo].[User] ([email])
GO
ALTER TABLE [dbo].[Cart] CHECK CONSTRAINT [FK_Cart_User]
GO
ALTER TABLE [dbo].[Doctor]  WITH CHECK ADD  CONSTRAINT [FK_Doctor_Service_Type] FOREIGN KEY([t_id])
REFERENCES [dbo].[Service_Type] ([t_id])
GO
ALTER TABLE [dbo].[Doctor] CHECK CONSTRAINT [FK_Doctor_Service_Type]
GO
ALTER TABLE [dbo].[Doctor]  WITH CHECK ADD  CONSTRAINT [FK_Doctor_User] FOREIGN KEY([email])
REFERENCES [dbo].[User] ([email])
GO
ALTER TABLE [dbo].[Doctor] CHECK CONSTRAINT [FK_Doctor_User]
GO
ALTER TABLE [dbo].[exam_medicine]  WITH CHECK ADD  CONSTRAINT [FK_exam_medicine_medicine] FOREIGN KEY([medicine_id])
REFERENCES [dbo].[medicine] ([id])
GO
ALTER TABLE [dbo].[exam_medicine] CHECK CONSTRAINT [FK_exam_medicine_medicine]
GO
ALTER TABLE [dbo].[exam_medicine]  WITH CHECK ADD  CONSTRAINT [FK_exam_medicine_Order_detail] FOREIGN KEY([od_id])
REFERENCES [dbo].[Order_detail] ([od_id])
GO
ALTER TABLE [dbo].[exam_medicine] CHECK CONSTRAINT [FK_exam_medicine_Order_detail]
GO
ALTER TABLE [dbo].[Feedback]  WITH CHECK ADD  CONSTRAINT [FK_Feedback_Order] FOREIGN KEY([o_id])
REFERENCES [dbo].[Order] ([o_id])
GO
ALTER TABLE [dbo].[Feedback] CHECK CONSTRAINT [FK_Feedback_Order]
GO
ALTER TABLE [dbo].[Feedback]  WITH CHECK ADD  CONSTRAINT [FK_Feedback_Service] FOREIGN KEY([s_id])
REFERENCES [dbo].[Service] ([id])
GO
ALTER TABLE [dbo].[Feedback] CHECK CONSTRAINT [FK_Feedback_Service]
GO
ALTER TABLE [dbo].[Feedback]  WITH CHECK ADD  CONSTRAINT [FK_Feedback_User] FOREIGN KEY([u_email])
REFERENCES [dbo].[User] ([email])
GO
ALTER TABLE [dbo].[Feedback] CHECK CONSTRAINT [FK_Feedback_User]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK__Order__email_cus__59FA5E80] FOREIGN KEY([email_customer])
REFERENCES [dbo].[User] ([email])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK__Order__email_cus__59FA5E80]
GO
ALTER TABLE [dbo].[Order_detail]  WITH CHECK ADD  CONSTRAINT [FK__Order_det__email__5AEE82B9] FOREIGN KEY([email_doctor])
REFERENCES [dbo].[User] ([email])
GO
ALTER TABLE [dbo].[Order_detail] CHECK CONSTRAINT [FK__Order_det__email__5AEE82B9]
GO
ALTER TABLE [dbo].[Order_detail]  WITH CHECK ADD  CONSTRAINT [FK__Order_deta__o_id__628FA481] FOREIGN KEY([o_id])
REFERENCES [dbo].[Order] ([o_id])
GO
ALTER TABLE [dbo].[Order_detail] CHECK CONSTRAINT [FK__Order_deta__o_id__628FA481]
GO
ALTER TABLE [dbo].[Order_detail]  WITH CHECK ADD  CONSTRAINT [FK__Order_deta__s_id__5CD6CB2B] FOREIGN KEY([s_id])
REFERENCES [dbo].[Service] ([id])
GO
ALTER TABLE [dbo].[Order_detail] CHECK CONSTRAINT [FK__Order_deta__s_id__5CD6CB2B]
GO
ALTER TABLE [dbo].[Order_detail]  WITH CHECK ADD  CONSTRAINT [FK_Order_detail_Medical_record1] FOREIGN KEY([mr_id])
REFERENCES [dbo].[Medical_record] ([mr_id])
GO
ALTER TABLE [dbo].[Order_detail] CHECK CONSTRAINT [FK_Order_detail_Medical_record1]
GO
ALTER TABLE [dbo].[Post]  WITH CHECK ADD  CONSTRAINT [FK_Post_Author] FOREIGN KEY([a_id])
REFERENCES [dbo].[Author] ([a_id])
GO
ALTER TABLE [dbo].[Post] CHECK CONSTRAINT [FK_Post_Author]
GO
ALTER TABLE [dbo].[Post]  WITH CHECK ADD  CONSTRAINT [FK_Post_Category] FOREIGN KEY([c_id])
REFERENCES [dbo].[Category] ([c_id])
GO
ALTER TABLE [dbo].[Post] CHECK CONSTRAINT [FK_Post_Category]
GO
ALTER TABLE [dbo].[Role_Feature]  WITH CHECK ADD  CONSTRAINT [FK_Group_Feature_Feature] FOREIGN KEY([f_id])
REFERENCES [dbo].[Feature] ([f_id])
GO
ALTER TABLE [dbo].[Role_Feature] CHECK CONSTRAINT [FK_Group_Feature_Feature]
GO
ALTER TABLE [dbo].[Role_Feature]  WITH CHECK ADD  CONSTRAINT [FK_Group_Feature_Group] FOREIGN KEY([r_id])
REFERENCES [dbo].[Role] ([r_id])
GO
ALTER TABLE [dbo].[Role_Feature] CHECK CONSTRAINT [FK_Group_Feature_Group]
GO
ALTER TABLE [dbo].[Service]  WITH CHECK ADD  CONSTRAINT [FK_Service_Service_Type] FOREIGN KEY([t_id])
REFERENCES [dbo].[Service_Type] ([t_id])
GO
ALTER TABLE [dbo].[Service] CHECK CONSTRAINT [FK_Service_Service_Type]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Role ] FOREIGN KEY([r_id])
REFERENCES [dbo].[Role] ([r_id])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Role ]
GO
/****** Object:  Trigger [dbo].[getMr_idAfterInsert]    Script Date: 7/25/2022 1:09:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create trigger [dbo].[getMr_idAfterInsert] ON [dbo].[Medical_record]
After Insert
AS
BEGIN
SET NOCOUNT ON;
	Select i.mr_id from inserted i 
	END
GO
ALTER TABLE [dbo].[Medical_record] ENABLE TRIGGER [getMr_idAfterInsert]
GO
/****** Object:  Trigger [dbo].[getO_idAfterInsert]    Script Date: 7/25/2022 1:09:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create trigger [dbo].[getO_idAfterInsert] ON [dbo].[Order]
after Insert
as 
begin 
set nocount ON;
	Select i.o_id from inserted i 
	EnD
GO
ALTER TABLE [dbo].[Order] ENABLE TRIGGER [getO_idAfterInsert]
GO
