<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../component/top.jsp"></jsp:include>



    <!-- Full Screen Search Start -->
    <div class="modal fade" id="searchModal" tabindex="-1">
        <div class="modal-dialog modal-fullscreen">
            <div class="modal-content" style="background: rgba(9, 30, 62, .7);">
                <div class="modal-header border-0">
                    <button type="button" class="btn bg-white btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body d-flex align-items-center justify-content-center">
                    <div class="input-group" style="max-width: 600px;">
                        <input type="text" class="form-control bg-transparent border-primary p-3" placeholder="Type search keyword">
                        <button class="btn btn-primary px-4"><i class="bi bi-search"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Full Screen Search End -->


    <!-- Hero Start -->
    <div class="container-fluid bg-primary py-5 hero-header mb-5">
        <div class="row py-3">
            <div class="col-12 text-center">
                <h1 class="display-3 text-white animated zoomIn">Doctor's Information</h1>
                <a href="home" class="h4 text-white">Home</a>
                <i class="far fa-circle text-white px-2"></i>
                <a href="doctor" class="h4 text-white">Doctor</a>
                <i class="far fa-circle text-white px-2"></i>
                <a href="#" class="h4 text-white">Doctor's Information</a>
            </div>
        </div>
    </div>
    <!-- Hero End -->


    <!-- Doctor Start -->
    <div class="container-fluid py-5">
        <div class="container">
            <div class="row g-5">
                <div class="col-xl-4 col-lg-6 wow slideInUp" data-wow-delay="0.1s">
                    <div class="bg-light rounded h-100 p-5">
                        <div class="d-flex align-items-center mb-2">
                            <div class="text-start">
                                <h5 class="mb-0">Position</h5>
                                <span>${requestScope.doctor.position}</span>
                            </div>
                        </div>
                        <div class="d-flex align-items-center mb-2">
                           
                            <div class="text-start">
                                <h5 class="mb-0">Training Process</h5>
                                <span>${requestScope.doctor.trainingProcess}</span>
                            </div>
                        </div>
                        <div class="d-flex align-items-center">
                           
                            <div class="text-start">
                                <h5 class="mb-0">Working Process</h5>
                                <span>${requestScope.doctor.workingProcess}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6 wow slideInUp" data-wow-delay="0.1s">
                    <div class="bg-light rounded h-100 p-5">
                        <div class="d-flex align-items-center mb-2">
                           
                            <div class="text-start">
                                <h5 class="mb-0">Research Work</h5>
                                <span>${requestScope.doctor.researchWork}</span>
                            </div>
                        </div>
                        <div class="d-flex align-items-center mb-2">
                           
                            <div class="text-start">
                                <h5 class="mb-0">Scientific Research Topics</h5>
                                <span>${requestScope.doctor.scientificResearchTopics}</span>
                            </div>
                        </div>
                        <div class="d-flex align-items-center">
                            
                            <div class="text-start">
                                <h5 class="mb-0">Bonus</h5>
                                <span>${requestScope.doctor.bonus}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6 wow slideInUp" data-wow-delay="0.3s">
                    <div class="team-item">
                        <div class="position-relative rounded-top" style="z-index: 1;">
                            <img class="img-fluid rounded-top w-100" src="${requestScope.doctor.user.photo}" alt="Doctor_Photo">
                            <div class="position-absolute top-100 start-50 translate-middle bg-light rounded p-2 d-flex">
                                <a class="btn btn-primary btn-square m-1" href="${requestScope.doctor.twitter}"><i class="fab fa-twitter fw-normal"></i></a>
                                <a class="btn btn-primary btn-square m-1" href="${requestScope.doctor.facebook}"><i class="fab fa-facebook-f fw-normal"></i></a>
                                <a class="btn btn-primary btn-square m-1" href="${requestScope.doctor.linkedin}"><i class="fab fa-linkedin-in fw-normal"></i></a>
                                <a class="btn btn-primary btn-square m-1" href="${requestScope.doctor.instagram}"><i class="fab fa-instagram fw-normal"></i></a>
                            </div>
                        </div>
                        <div class="team-text position-relative bg-light text-center rounded-bottom p-4 pt-5">
                            <h4 class="mb-2"> 
                                Dr. ${requestScope.doctor.user.name}</a>
                            </h4>
                            <p class="text-primary mb-0">
                                ${requestScope.doctor.serviceType.t_name}</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Doctor End -->

    <jsp:include page="../component/bottom.jsp"></jsp:include>