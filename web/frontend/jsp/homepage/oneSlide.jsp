<%-- 
    Document   : service
    Created on : Jun 5, 2022, 11:59:30 AM
    Author     : nghia
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../component/top.jsp"></jsp:include>

    <style>
        .slide-detail {
            text-align: justify;
            color: white;
        }
        .slide-container{
            -webkit-backdrop-filter: blur(10px);
            backdrop-filter: blur(10px);
            background-color: rgba(2, 2, 2, 0.486);
            padding:10px;
            border-radius: 10px;
            transition: linear 0.5s;

        }
        .slide-container:hover{
            background-color: var(--primary);
        }
    </style>
    <!-- Hero Start -->
    <div class="container-fluid mb-5 py-5"  style="background-image: url('${requestScope.slide.slideImg.replace('\\','/')}');background-size: cover;">
        <div class="container slide-container text-center ">
            <div class='row'>
                <div class='col-lg-2'>

                </div>
                <div class='col-lg-8'>
                    <h3 style="text-transform: capitalize" class="display-3 text-white animated zoomIn">${requestScope.slide.slideTitle}</h3>
                </div>
                <div class='col-lg-2 position-relative'>
                    <p style="position: absolute;right:10px;top:0px;color:white">${slide.slideDate}</p>
                </div>
            </div>


            <div class="container">
                <p class="slide-detail">&emsp;${slide.slideDetail}</p>
            </div>




        </div>


    </div>
    <!-- Hero End -->








</div>

<!-- Newsletter Start -->
<div class="container-fluid position-relative pt-5 wow fadeInUp" data-wow-delay="0.1s" style="z-index: 1;">

</div>
<!-- Newsletter End -->

<jsp:include page="../component/bottom.jsp"></jsp:include>