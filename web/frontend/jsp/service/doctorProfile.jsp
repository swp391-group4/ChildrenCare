<%-- 
    Document   : Profile
    Created on : May 20, 2022, 10:54:40 PM
    Author     : Asus
--%>

<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../component/top.jsp"></jsp:include>
   
      

        <!-- Hero Start -->
        <div class="container-fluid bg-primary py-5 hero-header mb-5">
            <div class="row py-3">
                <div class="col-12 text-center">
                    <h1 class="display-3 text-white animated zoomIn">Doctor Profile</h1>
                    <a href="" class="h4 text-white">Home</a>
                    <i class="far fa-circle text-white px-2"></i>
                    <a href="" class="h4 text-white">Doctor Profile</a>
                </div>
            </div>
        </div>
        <!-- Hero End -->


        <!-- Contact Start -->
        <c:set var="user" value="${requestScope.user}"/>
        <div class="container-fluid py-5">
            <div class="container ">
                <div class="row g-5">
                    <div class="col-xl-6 col-lg-6 wow slideInUp" data-wow-delay="0.1s">
                        <div class="bg-light rounded h-100 p-5">
                            <div class="section-title">
                                <h5 class="position-relative d-inline-block text-primary text-uppercase">Contact Us</h5>
                                <h1 class="display-6 mb-4">Feel Free To Contact Us</h1>
                            </div>
                            <div class="d-flex align-items-center mb-2">
                                <i class="bi bi-geo-alt fs-1 text-primary me-3"></i>
                                <div class="text-start">
                                    <h5 class="mb-0">Our Office</h5>
                                    <span>Khu Cong nghe Cao Hoa Lac, Thach That, Ha Noi</span>
                                </div>
                            </div>
                            <div class="d-flex align-items-center mb-2">
                                <i class="bi bi-envelope-open fs-1 text-primary me-3"></i>
                                <div class="text-start">
                                    <h5 class="mb-0">Email Us</h5>
                                    <span>Team4_SWP391@gmail.com</span>
                                </div>
                            </div>
                            <div class="d-flex align-items-center">
                                <i class="bi bi-phone-vibrate fs-1 text-primary me-3"></i>
                                <div class="text-start">
                                    <h5 class="mb-0">Call Us</h5>
                                    <span>+012 345 6789</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-6 col-lg-6 wow slideInUp" data-wow-delay="0.3s">
                        <form action="profile" method="post" enctype="multipart/form-data">
                            <div class="row g-3">
                                <img src="${requestScope.doctor.photo}" alt="user_photo" style="width: 100px">

                                <div class="col-12">
                                    <p>Email: ${doctor.email} </p>
                                </div>
                                <div class="col-12">
                                    <p>Full Name: ${doctor.name}</p>
                                </div>

                                <div class="col-12">
                                    <p>Dob: ${doctor.dob}</p>
                                </div>
                                <div class="col-12">
                                    <p>Gender:<c:if test="${requestScope.doctor.gender eq true}">
                                            Male
                                        </c:if>
                                        <c:if test="${requestScope.doctor.gender eq false}">
                                            Female
                                        </c:if> </p>



                                </div>
                                <div class="col-12">
                                    <p>Phone: ${doctor.phone}</p>
                                </div>
                                <div class="col-12">
                                    <p>Address: ${doctor.address} </p>

                                </div>
                                <div class="col-12">
                                </div>
                                <div class="col-12">
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <!-- Contact End -->

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script>
            function  chooseFile(fileInput) {
                if (fileInput.files && fileInput.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#image').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(fileInput.files[0]);
                }
            }
        </script>
        <jsp:include page="../component/bottom.jsp"></jsp:include>
