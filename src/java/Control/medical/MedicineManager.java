/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Control.medical;

import Control.Login.AccountAuthentication;
import DAO.MedicineDAO;
import DAO.UserDAO;
import Model.Account.User;
import Model.medical.Medicine;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.soap.SOAPBinding;

/**
 *
 * @author Administrator
 */
public class MedicineManager extends AccountAuthentication {
    String medicineList = "/frontend/jsp/medicine/list.jsp";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        UserDAO o = new UserDAO();
        HttpSession hs = request.getSession();
        User u = (User)hs.getAttribute("account");
        try {
            User user = o.getUser(u.getEmail());
            request.setAttribute("account", user);
        } catch (Exception ex) {
            Logger.getLogger(MedicineManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        // search list
        String id = request.getParameter("id");
        String name = request.getParameter("name");
        String product = request.getParameter("produc");
        String sort = request.getParameter("sort");
        
        sort = (sort == null || sort.length() == 0)?"0":sort;
        id = (id == null || id.length() == 0)?"":id;
        name = (name == null || name.length() == 0)?"":name;
        product = (product == null || product.length() == 0)?"":product;
        
        MedicineDAO mdao = new MedicineDAO();
        List<Medicine> list = mdao.searchList(id, name, product, sort);
        request.setAttribute("id", id.trim());
        request.setAttribute("name", name.trim());
        request.setAttribute("product", product.trim());
        request.setAttribute("sort", sort);
        request.setAttribute("list", list);
        request.getRequestDispatcher(medicineList).forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
