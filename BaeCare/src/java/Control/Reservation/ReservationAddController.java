/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Control.Reservation;

import Control.Login.AccountAuthentication;
import DAO.MedicalRecordDAO;
import DAO.OrderDAO;
import DAO.OrderDetailDAO;
import DAO.ServiceDAO;
import Model.Account.User;
import Model.Order.MedicalRecord;
import Model.Order.Order;
import Model.Service.Service;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author tkoko
 */
public class ReservationAddController extends AccountAuthentication {

    final String path = "../frontend/jsp/reservationDetails/reservationAddForReceptionist.jsp";

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        ServiceDAO serviceDAO = new ServiceDAO();
        ArrayList<Service> listServiceActive = serviceDAO.getListServiceActive();
        request.setAttribute("services", listServiceActive);
        request.getRequestDispatcher(path).forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        String name = request.getParameter("name");
        boolean gender = true;
        if (request.getParameter("gender").equals("female")) {
            gender = false;
        }
        int age = Integer.parseInt(request.getParameter("age"));
        String address = request.getParameter("address");
        String reason = request.getParameter("reason");
        String phone = request.getParameter("phone");
        //
        String[] listSId = request.getParameterValues("sid");
        String[] listSym = request.getParameterValues("sid");
        double total = 0;
        ServiceDAO serviceDAO = new ServiceDAO();
        //get total 
        for (int i = 0; i < listSId.length; i++) {
            Service service = serviceDAO.getServiceByID(Integer.parseInt(listSId[i]));
            total += service.getSale_price();
            listSym[i] = request.getParameter("symptom");
        }
        //Insert to order 
        //Date order
        ZoneId zid = ZoneId.of("Asia/Ho_Chi_Minh");
        LocalDate ld = LocalDate.now(zid);
        Date dateNow = Date.valueOf(ld);
        Order o = new Order();
        o.setDateOrder(dateNow);
        o.setTotalPrice(total);
        OrderDAO oDao = new OrderDAO();
        int o_id = oDao.insertOrder(o);
        //insert medeical record 
        MedicalRecord m = new MedicalRecord();
        m.setUserName(name);
        m.setGender(gender);
        m.setAddress(address);
        m.setReasonForHospitalization(reason);
        m.setPhone(phone);
        m.setAge(age);
        MedicalRecordDAO medicalRecordDAO = new MedicalRecordDAO();
        int customerFillsInTheMedicalRecord = medicalRecordDAO.customerFillsInTheMedicalRecord(m);
        OrderDetailDAO odDao = new OrderDetailDAO();
        //insert to orderdetail
        List<Integer> listOrderNumber = new ArrayList<>();
        List<Model.Order.OrderDetail> listOrderDetail = new ArrayList<>();
        for (int i = 0; i < listSId.length; i++) {
            Model.Order.OrderDetail od = new Model.Order.OrderDetail();
            //  [o_id],[s_id],[email_doctor],[examineDate],[exam_Status],[price],[mr_id],[symptom],[ordinal_number]
            od.setoId(o_id);
            Service s = new Service();
            ServiceDAO sDao = new ServiceDAO();
            s = sDao.getServiceByID(Integer.parseInt(listSId[i]));

            od.setService(s); //Order detail
            User doctor = new User();
            doctor.setEmail(odDao.doctorHaveLeastNumberOrder(dateNow, s.getSt().getT_id()));
            od.setDoctor(doctor);//Order detail
            od.setExamDate(dateNow);//Order detail
            od.setExamStatus(false);
            od.setPrice(serviceDAO.getServiceByID(Integer.parseInt(listSId[i])).getSale_price());

            MedicalRecord mr = new MedicalRecord();
            mr.setId(customerFillsInTheMedicalRecord);
            od.setMedicalRecord(mr);
            od.setSymptom(listSym[i]);
            //get ordinal number
            od.setOrdinalNumber(-1);
            listOrderDetail.add(od);

            listOrderNumber.add(odDao.getAvailableOrderNumber(od.getExamDate(), od.getDoctor().getEmail()));
          
        }
        int minOrderNumber = Collections.min(listOrderNumber);
        int indexOfOrderNumber = listOrderNumber.indexOf(minOrderNumber);
        listOrderDetail.get(indexOfOrderNumber).setOrdinalNumber(minOrderNumber);

        for (Model.Order.OrderDetail orderDetail : listOrderDetail) {
            odDao.insertOrderDetail(orderDetail);

        }
        response.sendRedirect("list");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
