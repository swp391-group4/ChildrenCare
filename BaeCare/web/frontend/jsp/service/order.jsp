<%-- 
    Document   : order
    Created on : Jul 3, 2022, 5:03:55 PM
    Author     : chitung
--%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../component/top.jsp"></jsp:include>


    <!-- Hero Start -->
    <div class="container-fluid bg-primary py-5 hero-header mb-5">
        <div class="row py-3">
            <div class="col-12 text-center">
                <h1 class="display-3 text-white animated zoomIn">Doctor Profile</h1>
                <a href="" class="h4 text-white">Home</a>
                <i class="far fa-circle text-white px-2"></i>
                <a href="" class="h4 text-white">Order</a>
            </div>
        </div>
    </div>
    <!-- Hero End -->
    <!-- Appointment Start -->
    <h4 class="text-body fst-italic mb-4">Thank you for using the system</h4>
    <p class="mb-4">The system has sent an email confirming the medical examination through the system to the customer</p>
    <p class="mb-4">Pleased to serve you</p>
    <div class="mt-2 mb-5 page-body container" style="margin-top: 10px;">
        <div class="row">

        </div>
        <div class="row">


            <div class="row" >
                <div style="overflow-x: auto; overflow-y: auto">
                    <table id="multi-table" class="table table-striped table-bordered nowrap table-hover">
                        <tr class="text-center">

                            <th>Customer Name</th>
                            <th>Date of order</th>
                            <th>Total Cost</th>  
                            <th></th>
                        </tr>
                        <!-- comment -->

                        <tr class="text-center">
                            <!--Delete-->
                            <td>${requestScope.order.customer.name}</td>
                        <td>${requestScope.order.dateOrder}</td>
                        <td>${requestScope.order.totalPrice}</td>
                        <td><button class="btn btn-primary" onclick="OrderDetail(${requestScope.order.oid})">View Order Detail</button></td>
                    </tr>
                </table>
            </div>
                    <div class="row" id="order">
                        
                    </div>

           
            <div class="row ">
                <div class="col-lg-4">

                </div>
                <div class="col-lg-4 text-center">
                    <a href="home" class="btn btn-primary w-100">Home</a>
                </div>
                <div style="text-align:end" class="col-lg-4">

                </div>

                <!-- Go to service list page -->

                <!-- Go to Reservation Contact page -->
            </div>
        </div>


    </div>
</div>
<!-- Appointment End -->
<!-- Newsletter Start -->
<div class="container-fluid position-relative pt-5 wow fadeInUp" data-wow-delay="0.1s" style="z-index: 1;">
    <div class="container">
        <!--            <div class="bg-primary p-5">
<form class="mx-auto" style="max-width: 600px;">
    <div class="input-group">
        <h4 style="text-align: center">Have Nice Day</h4>
    </div>
</form>
</div>-->
    </div>
</div>
<!-- Newsletter End -->
</body>
</html>
<script>
    function OrderDetail(value) {
        $.ajax({
            data: {

                odId: value
            },
            url: "/BaeCare/Order",
            type: "get",
            success: function (response) {

                var detailList = document.getElementById("order");
               
                    detailList.innerHTML = response;
               
console.log(response);

            },
            error: function (xhr) {
                console.log(xhr)
            }
        });
    }
</script>
<jsp:include page="../component/bottom.jsp"></jsp:include>
