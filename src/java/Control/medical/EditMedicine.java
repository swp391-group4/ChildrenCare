/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Control.medical;

import DAO.MedicineDAO;
import DAO.UserDAO;
import Model.Account.User;
import Model.medical.Medicine;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Administrator
 */
public class EditMedicine extends HttpServlet {
    String edit = "/frontend/jsp/medicine/edit.jsp";
    ArrayList<String> list = new ArrayList<>();
    int i = 0;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        MedicineDAO mdao = new MedicineDAO();
        String oldid = request.getParameter("mid");
        
        if (oldid != null) {
            list.add(oldid);
             Medicine medicine = mdao.getMedicineById(oldid);
             request.setAttribute("medicine", medicine);
             request.getRequestDispatcher(edit).forward(request, response);
        } else{
        //
        String id = request.getParameter("id");
        String name = request.getParameter("name").trim();
        String producer = request.getParameter("producer").trim();
        String element = request.getParameter("element").trim();
        String quantityy = request.getParameter("quantity").trim();
        String pricee = request.getParameter("price").trim();
        String inUse = request.getParameter("inUse").trim();
        String unit = request.getParameter("unit").trim();
        
        id = (id == null || id.length() == 0)?list.get(i):id;
        int quantity = Integer.parseInt(quantityy);
        double price = Double.parseDouble(pricee);
        boolean inuse = inUse.equals("true");
        Medicine m = new Medicine();
        m.setId(id);
        m.setName(name);
        m.setProducer(producer);
        m.setElement(element);
        m.setPrice(price);
        m.setQuantity(quantity);
        m.setInUse(inuse);
        m.setUnit(unit);
        mdao.edit(list.get(i), m);
        i+=1;
        response.sendRedirect("list");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession hs = request.getSession();
        User acc = (User)hs.getAttribute("account");
        UserDAO o = new UserDAO();
        try {
            User user = o.getUser(acc.getEmail());
            if (user.isStatus() != false && user.getRole().getId()<=2) {
                request.setAttribute("account", user);
                processRequest(request, response);
            }
        } catch (Exception ex) {
            Logger.getLogger(EditMedicine.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
