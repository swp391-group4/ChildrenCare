<%-- 
    Document   : list
    Created on : Jun 1, 2022, 8:05:08 PM
    Author     : Administrator
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../component/top.jsp"></jsp:include>
    <style>
        .checked {
            color: orange;
        }
        .fstar{
            display: flex;
            flex-direction: row;
        }
        .Chatbox{
            word-wrap: break-word;
            text-transform: capitalize;
            word-break: break-word;
        }

    </style>
    
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <!-- Main-body start -->
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- Page-header start -->
                    <div class="page-header">
                        <div class="row align-items-end">
                            <div class="col-lg-8">
                                <div class="page-header-title">
                                    <div class="d-inline">
                                        <h3 style="margin-left: 150px; margin-top: 30px; color: #00AEEF">Rated List</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="page-header-breadcrumb">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Page-header end -->
                    <!-- Page body start -->

                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <c:forEach items="${requestScope.listFB}" var="f">
                                    <c:if test="${f.fid ne null}">
                                        <div class="card mb-3">
                                            <div class="row g-0">
                                                <div class="col-md-4">
                                                    <img src="../${f.fphoto}" width="350px" class="img-fluid rounded-start" alt="..." style="margin-bottom: 30px; margin-left: 40px;">
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="card-body">
                                                        <h5 class="card-title"> ${f.service.sname}<br>
                                                            <c:forEach begin="1" end="5" step="1" var="star">
                                                                <c:if test="${star<=f.star}">
                                                                    <span class="fa fa-star checked"></span>
                                                                </c:if>
                                                                <c:if test="${star>f.star}">
                                                                    <span class="fa fa-star"></span>
                                                                </c:if>
                                                            </c:forEach></h5>
                                                        <p class="card-text text-truncate Chatbox">${f.detail}</p>
                                                        <p class="card-text"><a class="btn btn-primary waves-effect waves-light" href="edit?fid=${f.fid}">Edit</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </c:if>
                                    <c:if test="${f.fid eq null}">
                                        <h2>You have some examined services that have not feedback yet!</h2>
                                    </c:if>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                    <!-- Page body end -->
                </div>
            </div>
        </div>
    </div>


    <jsp:include page="../component/bottom.jsp"></jsp:include>

