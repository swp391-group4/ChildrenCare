<%-- 
    Document   : service
    Created on : Jun 5, 2022, 11:59:30 AM
    Author     : nghia
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../component/top.jsp"></jsp:include>




    <!-- Hero Start -->
    <div class="container-fluid bg-primary py-5 hero-header mb-5">
        <div class="row py-3">
            <div class="col-12 text-center">
                <h1 class="display-3 text-white animated zoomIn">Services</h1>
                <a href="home" class="h4 text-white">Home</a>
                <i class="far fa-circle text-white px-2"></i>
                <a href="service" class="h4 text-white">Services</a>
            </div>
        </div>
    </div>
    <!-- Hero End -->


    <!-- Service Start -->
    <div class="container-fluid py-5 wow fadeInUp" data-wow-delay="0.1s">
        <div class="container">
            <div class="row mb-5 p-5" style="background-color: var(--primary) ; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
                <div class="col-lg-3">
                    <form action="service" method="get">

                        <select class="form-select p-2" name="tId" id="" onchange="form.submit()">
                            <c:if test="${requestScope.tId eq 0}">


                                <option value="0" selected>
                                    All service
                                </option>
                            </c:if>
                            <c:if test="${requestScope.tId ne 0}">
                                <option value="0">
                                    All service
                                </option>
                            </c:if>
                            <c:forEach items="${requestScope.serviceTypeList}" var="st">
                                <c:if test="${requestScope.tId eq st.t_id}">


                                    <option value="${st.t_id}" selected>
                                        ${st.t_name}
                                    </option>
                                </c:if>
                                <c:if test="${requestScope.tId ne st.t_id}">
                                    <option value="${st.t_id}" >
                                        ${st.t_name}
                                    </option>
                                </c:if>
                            </c:forEach>
                        </select>
                    </form>

                </div>
                <div class="col-lg-6">
                    <form action="service" method="get" id="serviceSearch">
                        <div class="row">
                            <div class="col-lg-9">
                                <input type="hidden" name="tId" value="${requestScope.tId}">
                                <input type="search" class="form-control p-2" name="searchContent" value="${searchContent}" placeholder="Search a service">
                            </div>
                            <div class="col-lg-3">
                                <button form="serviceSearch" type="submit" class="btn btn-dark p-2">Search</button>
                            </div>
                        </div>


                    </form>

                </div>
                <div class="col-lg-3">

                </div>

            </div>
            <div class="row g-5 mb-5">
                <c:if test="${requestScope.serviceList.size() ne 0}">
                    <div class="col-lg-5 wow zoomIn" data-wow-delay="0.3s" style="min-height: 400px;">
                        <a href="servicedetail?sId=${requestScope.serviceList.get(serviceStart).getSid()}">
                            <img class="w-100 h-100" src="${requestScope.serviceList.get(serviceStart).getPhoto()}" style="object-fit: cover;">
                        </a>
                    </div>
                    <div class="col-lg-7">
                        <div class="section-title mb-5">
                            <a href="servicedetail?sId=${requestScope.serviceList.get(serviceStart).getSid()}">
                                <h5 class="position-relative d-inline-block text-primary text-uppercase">Our Services</h5>
                                <h1 class="display-5 mb-0">${requestScope.serviceList.get(serviceStart).getSname()}</h1>
                            </a>
                        </div>


                    </div>
                </c:if>
                <c:if test="${requestScope.serviceList.size() eq 0}">
                    <div class="row align-items-center text-center">
                        <img style='margin:10px auto -10px auto;' class='w-25' src="frontend/assert/img/service/notfound.png" alt="alt" />
                        <h1>
                            Can't not found this service!!
                        </h1>
                    </div>

                </c:if>

            </div>
            <c:if test="${requestScope.serviceList.size() >1}">
                <div class="row g-5 wow fadeInUp" data-wow-delay="0.1s">

                    <c:forEach items="${requestScope.serviceList}" var="s" begin="${requestScope.serviceStart+1}" end="${requestScope.serviceEnd}">


                        <div class="col-lg-3">
                            <div class="row g-5">
                                <a href="servicedetail?sId=${s.getSid()}">
                                    <div class="col-md-12 service-item wow zoomIn" data-wow-delay="0.3s">
                                        <div class="rounded-top overflow-hidden">
                                            <img class="img-fluid" src="${s.getPhoto()}" alt="">
                                        </div>
                                        <div class="position-relative bg-light rounded-bottom text-center p-4">
                                            <h5 class="m-0">${s.getSname()}</h5>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </c:if>
        </div>
    </div>
    <!-- Service End -->
    <div class="row">
        <div class="col-lg-4"></div>
        <div class="col-lg-4 text-center">
            <a class="btn btn-primary" href="service?page=${1}&tId=${tId}&searchContent=${searchContent}">Start</a>
            <c:if test="${page-2 >0}">
                <a class="btn btn-primary" href="service?page=${page-2}&tId=${tId}&searchContent=${searchContent}">${page-2}</a>
            </c:if>
            <c:if test="${page-1 >0}">
                <a class="btn btn-primary" href="service?page=${page-1}&tId=${tId}&searchContent=${searchContent}">${page-1}</a>
            </c:if>

            <a class="btn btn-dark" href="service?page=${page}&tId=${tId}&searchContent=${searchContent}">${page}</a>
            <c:if test="${page+1 <=totalPage}">
                <a class="btn btn-primary" href="service?page=${page+1}&tId=${tId}&searchContent=${searchContent}">${page+1}</a>
            </c:if>
            <c:if test="${page+2 <=totalPage}">
                <a class="btn btn-primary" href="service?page=${page+2}&tId=${tId}&searchContent=${searchContent}">${page+2}</a>
            </c:if>

            <a class="btn btn-primary" href="service?page=${totalPage}&tId=${tId}&searchContent=${searchContent}">End</a>
        </div>
        <div class="col-lg-4"></div>

    </div>

    <!-- Newsletter Start -->
    <div class="container-fluid position-relative pt-5 wow fadeInUp" data-wow-delay="0.1s" style="z-index: 1;">
        <div class="container">
            <div class="bg-primary p-5">
                <form class="mx-auto" style="max-width: 600px;">
                    <div class="input-group">
                        <input type="text" class="form-control border-white p-3" placeholder="Your Email">
                        <button class="btn btn-dark px-4">Sign Up</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Newsletter End -->

    <jsp:include page="../component/bottom.jsp"></jsp:include>