<%-- 
    Document   : reservationDetail
    Created on : Jul 8, 2022, 1:15:11 PM
    Author     : tkoko
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 <%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!DOCTYPE html>
<html>
    <jsp:include page="../component/top.jsp"></jsp:include>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>JSP Page</title>
    </head>
    
    <body>
        <form action="add" method="POST">
                    <div style="margin-bottom: 100px;width: 100%">
                        <div style="float: left;width: 30%" >
                          <input type="button" onclick="response()" value="List Page"><br/><br/>
        
            Name:<input type="text" name="name" placeholder="Full Name"><span id="errorName"></span><br/>
            Male:<input type="radio" name="gender" value="male" checked="">
            Female:<input type="radio" name="gender" value="female"><br/>
            Age:<input type="text" name="age" placeholder="Age"><span id="errorAge"></span><br/>
            Address:<input type="text" name="address" placeholder="Address"><span id="errorAddress"></span><br/>
            Reason:<input type="text" name="reason" placeholder="Reason"><span id="errorReason"></span><br/>
            Phone:<input type="text" name="phone" placeholder="Phone"><span id="errorPhone"></span><br/>
            
            
            <!--<input type="button" value="List service"  name="service" id="service" onclick="displayListService()" placeholder="">-->
        
                          </div>
                        
                    <div  style="float: none;width: 70%">
                        <table class="table  table-bordered text-center" id= "table-id" style="display: none;width: 100%">
                        <tr>
                            <th>Service Name</th>
                            <th>Original Price</th>
                            <th>Sale Price</th>
                            <th>Symptom</th>
                        </tr>
                      
                        <input  value="Delete all service" type="button" onclick="clearListServices()">
                        <input  type="submit" value="Save" onclick="return chooseServiceToForm()">
                    </table>
                        
                    </div>
                        
                    </div>
            <div id="listServices" ">
                   <table class="table  table-bordered text-center"  >
                            <tr>
                                <th>Name</th>
                                <th>Original_price</th>
                                <th>Sale_price</th>
                                <th></th>
                            </tr>
                            <c:forEach items="${requestScope.services}" var="s">
                                <tr>
                                    <td>${s.sname}</td>
                                    <td>${s.original_price}</td>
                                    <td>${s.sale_price}</td>
                                    <td onclick="addService(${s.sid},'${s.sname}',${s.original_price},${s.sale_price})"><input type="button" value="Add"></td>
                                </tr>
                        </c:forEach>
                        </table> 
                
                        </div>
            </form>
    
    <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
    </body>
    <script>
        function clearListServices(){
            document.getElementById("table-id").innerHTML = '<table class="table  table-bordered text-center" id= "table-id" style="display: none;width: 100%">'
                       + '<tr>'
                        +    '<th>Service Name</th>'
                         +  ' <th>Original Price</th>'
                          +  '<th>Sale Price</th>'
                        +'    <th>Symptom</th>'
                      +'  </tr>'
                   +' </table>';
           document.getElementById("table-id").style.display = "none";
        }
        function chooseServiceToForm() {
        let result = true;
            //validate name
            var name = document.getElementsByName("name")[0].value;
            if (validateName(name)) {
                document.getElementById("errorName").innerHTML = "";
            } else {
                result = false;
                document.getElementById("errorName").innerHTML = "Name invalid";
            }
            //validate address
            var address = document.getElementsByName("address")[0].value;
            if (validateAddress(address)) {
                document.getElementById("errorAddress").innerHTML = "";
            } else {
                result = false;
                document.getElementById("errorAddress").innerHTML = "Address invalid";
            }
            //validate reason for hospitalization
            var reason = document.getElementsByName("reason")[0].value;
            if (validateAddress(reason)) {
                document.getElementById("errorReason").innerHTML = "";
            } else {
                result = false;
                document.getElementById("errorReason").innerHTML = "Reason invalid";
            }
            //validate phone
            var reason = document.getElementsByName("phone")[0].value;
            if (validatePhone(reason)) {
                document.getElementById("errorPhone").innerHTML = "";
            } else {
                result = false;
                document.getElementById("errorPhone").innerHTML = "Phone invalid";
            }
            //validate age
            var age = document.getElementsByName("age")[0].value;
            if (validateAge(age)) {
                document.getElementById("errorAge").innerHTML = "";
            } else {
                result = false;
                document.getElementById("errorAge").innerHTML = "Age invalid";
            }
        if (result) {
            return true;
            //hien thi service cho ng dung chon ai kham service nao 
        }else{
            return false;
        }
    }
        function response(){
            window.location.href = "list";
        }
        function displayListService(){
            if(document.getElementById("listServices").style.value === "List service"){
                document.getElementById("listServices").style.display = "flex";
            document.getElementById("listServices").style.value = "Close list service";
            }else{
                document.getElementById("listServices").style.display = "none";
            document.getElementById("listServices").style.value = "List service";
            }
            
        }
        var index = 1;
        function addService(sid,sname,original_price,sale_price){
            //get service choose by user 
           // let getService = document.getElementById("service").value;
            //document.getElementById("service").value = '';
            const listService = document.getElementsByName("sname");
            let result = true;
            for(let i = 0;i<listService.length;i++){
                if(listService[i].value === sname){
                    result = false;
                }
            }
            if(result){
                document.getElementById("table-id").style.display = "block";
            document.getElementById("table-id").innerHTML +=
                            '<tr>'
                            +'<input type="text" name="sid" value="'+sid+'" hidden="" >'
                            +'<td>'+sname+'<input type="text" name="sname" value="'+sname+'" hidden="" > </td>'
                            +'<td>'+original_price+'<input type="text" name="originalprice" value="'+original_price+'" hidden="" > </td>'
                            +'<td>'+sale_price+'<input type="text" name="saleprice" value="'+sale_price+'" hidden="" ></td>'
                            +'<td><textarea type="text" name="symptom"></textarea></td>'
                        +'</tr>';
            }else{
                alert("Service has been selected");
            }
            //add service to div listServicesSelected 
            
            
//           let newService = document.createElement("<input value = +"getService"+>");
//           document.getElementById("listServicesSelected").appendChild(newService);
        }
        function validateAge(age) {
        var numberRegex = /^\d+$/;
        if (numberRegex.test(age))
            return true;
        else
            return false;
     }
        
         function validatePhone(phone) {
        let regexPhone = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/;
        if (regexPhone.test(phone))
            return true;
        else
            return false;
    }
    function validateSymptom(symptom) {
        let regexSymptom = /^[a-zA-Z0-9\s,.'-]{3,}$/;
        if (regexSymptom.test(symptom))
            return true;
        else
            return false;
    }
    function validateAddress(address) {
        let regexAddress = /^[a-zA-Z0-9\s,.'-]{3,}$/;
        if (regexAddress.test(address))
            return true;
        else
            return false;
    }
    function validateName(name) {
        let regexName = /^(([A-Za-z]+[\-\']?)*([A-Za-z]+)?\s)+([A-Za-z]+[\-\']?)*([A-Za-z]+)?$/;
        if (regexName.test(name))
            return true;
        else
            return false;
    }
        
    </script>
</html>

<jsp:include page="../component/bottom.jsp"></jsp:include>