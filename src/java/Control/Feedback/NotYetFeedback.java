/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Control.Feedback;

import Control.Login.AccountAuthentication;
import DAO.FeedbackDao;
import DAO.OrderDetailDAO;
import Model.Account.User;
import Model.Order.OrderDetail;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author chitung
 */
public class NotYetFeedback extends AccountAuthentication {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception{
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("account");
        String email = user.getEmail();
        FeedbackDao fDao = new FeedbackDao();
        OrderDetailDAO odDao = new OrderDetailDAO();
        ArrayList<OrderDetail> listCustomerUsedTheService = fDao.getCustomerUsedTheService(email);
        ArrayList<OrderDetail> listNotYetFeedback = new ArrayList<>();
        for (OrderDetail od : listCustomerUsedTheService) {         
                boolean orderDetailExist = odDao.orderDetailExist(email, od.getoId(), od.getService().getSid());
                if (orderDetailExist == true) {
                    listNotYetFeedback.add(od);
                }
        }
        request.setAttribute("notYetList", listNotYetFeedback);
        request.setAttribute("email", email);
        request.getRequestDispatcher("frontend/jsp/feedback/notYet.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(NotYetFeedback.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(NotYetFeedback.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
