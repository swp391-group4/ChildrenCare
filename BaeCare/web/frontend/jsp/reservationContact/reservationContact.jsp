<%-- 
    Document   : reservationContact
    Created on : Jun 4, 2022, 4:12:47 PM
    Author     : tkoko
--%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../component/top.jsp"></jsp:include>
    <script>
        function back() {
            location.href = "../Reservation";
        }
    </script>


    <!-- Navbar End -->
    <div class="container-fluid bg-primary py-5 hero-header mb-5">
        <div class="row py-3">
            <div class="col-12 text-center">
                <h1 class="display-3 text-white animated zoomIn">Reservation Contact</h1>
                <a href="../home" class="h4 text-white">Home</a>
                <i class="far fa-circle text-white px-2"></i>
                <a href="contect" class="h4 text-white"></a>
            </div>
        </div>
    </div>
    <input type="submit" value="Change" onclick="back()" id="submitButton">
    <p id="notice">Click to return to page Reservation Details</p>
    <br/>
    <!--(including full-name, gender, email, mobile, address, note-->
    <div id="div_giua">
        <form action="completion" method="POST" id="contactForm" style="margin-top: 20px">
            <h3>Billing Information</h3>
            Full Name:<input type="text" name="name" value="${sessionScope.user.name}" placeholder="Full name"><br/>
        Gender:<input type="radio" name="gender" value="true"
                      <c:if test="${sessionScope.user.gender==true}">
                          checked=""
                      </c:if>
                      >Male
        <input type="radio" name="gender" value="false"
               <c:if test="${sessionScope.user.gender==false}">
                   checked=""
               </c:if>
               >Female<br/>
        Email:<input type="text" name="email" value="${sessionScope.user.email}" placeholder="Email"><br/>
        Phone:<input type="text" name="phone" value="${sessionScope.user.phone}" placeholder="Phone"><br/>
        Address:<input type="text" name="address" value="${sessionScope.user.address}" placeholder="Address"><br/>
        Note:<textarea name="note" placeholder="Note something"></textarea>

        <table class="table table-striped table-class" id= "table-id" border="1px">
            <tr>
                <td>Cardinal numbers</td>
                <td>Service</td>
                <td>Into money</td>
            </tr>
            <c:set var="total" value="0"></c:set>
            <c:set var="index" value="1"></c:set>
            <c:forEach items="${requestScope.carts}" var="c">
                <c:set var="total"  value="${total+c.service.sale_price*c.quantity}"></c:set>
                    <tr>
                        <td>${index}</td>
                    <td>${c.service.sname} x${c.quantity}
                    </td>
                    <td>
                        ${c.service.sale_price * c.quantity}$
                    </td>
                </tr>
                <c:set var="index" value="${index+1}"></c:set>
            </c:forEach>
            <tr>
                <td>Total</td>
                <td></td>
                <td>${total} $</td>
            </tr>
        </table>
        <button name="submit" type="submit" id="contact-submit" data-submit="...Sending">Submit</button>
    </form>
</div>
<!-- Back to Top -->
<!-- Footer Start -->
<div class="container-fluid bg-dark text-light py-5 wow fadeInUp" data-wow-delay="0.3s" style="margin-top: -75px;">
    <div class="container pt-5">
        <div class="row g-5 pt-4">
            <div class="col-lg-3 col-md-6">
                <h3 class="text-white mb-4">Quick Links</h3>
                <div class="d-flex flex-column justify-content-start">
                    <a class="text-light mb-2" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>Home</a>
                    <a class="text-light mb-2" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>About Us</a>
                    <a class="text-light mb-2" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>Our Services</a>
                    <a class="text-light mb-2" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>Latest Blog</a>
                    <a class="text-light" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>Contact Us</a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <h3 class="text-white mb-4">Popular Links</h3>
                <div class="d-flex flex-column justify-content-start">
                    <a class="text-light mb-2" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>Home</a>
                    <a class="text-light mb-2" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>About Us</a>
                    <a class="text-light mb-2" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>Our Services</a>
                    <a class="text-light mb-2" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>Latest Blog</a>
                    <a class="text-light" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>Contact Us</a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <h3 class="text-white mb-4">Get In Touch</h3>
                <p class="mb-2"><i class="bi bi-geo-alt text-primary me-2"></i>123 Street, New York, USA</p>
                <p class="mb-2"><i class="bi bi-envelope-open text-primary me-2"></i>info@example.com</p>
                <p class="mb-0"><i class="bi bi-telephone text-primary me-2"></i>+012 345 67890</p>
            </div>
            <div class="col-lg-3 col-md-6">
                <h3 class="text-white mb-4">Follow Us</h3>
                <div class="d-flex">
                    <a class="btn btn-lg btn-primary btn-lg-square rounded me-2" href="#"><i class="fab fa-twitter fw-normal"></i></a>
                    <a class="btn btn-lg btn-primary btn-lg-square rounded me-2" href="#"><i class="fab fa-facebook-f fw-normal"></i></a>
                    <a class="btn btn-lg btn-primary btn-lg-square rounded me-2" href="#"><i class="fab fa-linkedin-in fw-normal"></i></a>
                    <a class="btn btn-lg btn-primary btn-lg-square rounded" href="#"><i class="fab fa-instagram fw-normal"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>



