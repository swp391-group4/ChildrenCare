<%-- 
    Document   : notYet
    Created on : Jul 24, 2022, 8:39:13 PM
    Author     : chitung
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../component/top.jsp"></jsp:include>
    <style>
        .checked {
            color: orange;
        }
        .fstar{
            display: flex;
            flex-direction: row;
        }
        .Chatbox{
            word-wrap: break-word;
            text-transform: capitalize;
            word-break: break-word;
        }

    </style>

    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <!-- Main-body start -->
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- Page-header start -->
                    <div class="page-header">
                        <div class="row align-items-end">
                            <div class="col-lg-8">
                                <div class="page-header-title">
                                    <div class="d-inline">
                                        <h3 style="margin-left: 150px; margin-top: 30px; color: #00AEEF">List Not Yet Feedback</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="page-header-breadcrumb">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Page-header end -->
                    <!-- Page body start -->

                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                            <c:if test="${requestScope.notYetList.size() > 0}">
                                <div style="overflow-x: auto; overflow-y: auto">
                                    <table id="multi-table" class="table table-striped table-bordered nowrap table-hover">

                                        <tr class="text-center">


                                            <th>Service Name</th>

                                            <th>Doctor Name</th>

                                            <th>Date Of Exam</th>
                                            <th>Date Of Checkout</th>


                                            <th></th>
                                        </tr>
                                        <!-- comment --> 


                                        <c:forEach items="${requestScope.notYetList}" var="f">

                                            <tr class="text-center">

                                                <td>${f.service.sname}</td>

                                                <td>${f.doctor.name}</td>


                                                <td>${f.examDate}</td>
                                                <td>${f.checkoutDate}</td>



                                                <td><a class="btn btn-primary waves-effect waves-light" href="feedback/add?email=${requestScope.email}&orderId=${f.oId}&serviceId=${f.service.sid}">Add Feedback</a></td>

                                            </tr>

                                        </c:forEach>

                                    </table>
                                </c:if>
                                <c:if test="${requestScope.notYetList.size() == 0}">
                                    No Record To Display
                                </c:if> 
                            </div> 
                        </div>
                    </div>
                </div>
                <!-- Page body end -->
            </div>
        </div>
    </div>
</div>

<!-- Appointment End -->
<!-- Newsletter Start -->
<div class="container-fluid position-relative pt-5 wow fadeInUp" data-wow-delay="0.1s" style="z-index: 1;">
    <div class="container">
        <!--            <div class="bg-primary p-5">
<form class="mx-auto" style="max-width: 600px;">
    <div class="input-group">
        <h4 style="text-align: center">Have Nice Day</h4>
    </div>
</form>
</div>-->
    </div>
</div>
<!-- Newsletter End -->
</body>
</html>

<br/><br/>
<jsp:include page="../component/bottom.jsp"></jsp:include>