<%-- 
    Document   : detail
    Created on : Jun 14, 2022, 9:07:36 PM
    Author     : Administrator
--%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../component/top.jsp"></jsp:include>
  
        <script>
            function viewService(id)
            {
                window.location.href = "../servicedetail?sId=" + id;
            }
        </script>
      

        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- Main-body start -->
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- Page-header start -->
                        <div class="page-header">
                            <div class="row align-items-end">
                                <div class="col-lg-8">
                                    <div class="page-header-title">
                                        <div class="d-inline">
                                            <h3 style="margin-left: 100px; margin-top: 30px; color: #00AEEF">My Reservation</h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="page-header-breadcrumb">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Page-header end -->
                        <!-- Page body start -->
                        <div class="page-body">
                            <!-- Row Created Callback table start -->
                            <div class="card">
                                <div class="card-header">
                                    <h5 style="margin-left: 250px;">Order Number: ${requestScope.oid}</h5>
                            </div>
                            <div class="card-block">
                                <div class="table-responsive dt-responsive">
                                    <c:if test="${requestScope.order != null}">
                                        <c:forEach items="${requestScope.order}" var="od">
                                            <table id="row-callback" ondblclick="viewService(${o.oid})" class="table table-striped table-bordered nowrap" style="width: 700px; margin: 40px auto;">
                                                <tbody>
                                                    <tr>
                                                        <td>Booking id</td>
                                                        <td>${od.odId}</td>
                                                        <td rowspan="6">
                                                            <img onclick="viewService(${od.service.sid})" src="../${od.service.photo}" width="250px" alt="Service"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Booking Date</td>
                                                        <td>${od.examDate}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Doctor Examined</td>
                                                        <td>
                                                            ${od.doctor.name}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Time Finished</td>
                                                        <td>${od.checkoutDate}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Total Cost</td>
                                                        <td>${od.price}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Status</td>
                                                        <td>${od.examStatus == true ?"Examined":"Not Examined yet"}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </c:forEach>
                                        <div style="margin-left: 700px; margin-bottom: 20px;">
                                            <a href="list" ><button class="btn btn-primary waves-effect waves-light">Cancel</button></a>
                                        </div>

                                    </c:if>
                                    <c:if test="${requestScope.order.size() == 0}">
                                        <h3 style="margin: 50px auto; text-align: center;">You have not added any orders to your list yet</h3>
                                    </c:if>
                                </div>
                            </div>
                        </div>
                        <!-- Row Created Callback table end -->

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page body end -->

    <jsp:include page="../component/bottom.jsp"></jsp:include>

