/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Control.Doctor;

import Control.Login.AccountAuthentication;
import Control.User.UploadFile;
import DAO.DoctorDAO;
import DAO.ServiceDAO;
import DAO.UserDAO;
import Model.Account.Doctor;
import Model.Account.User;
import Model.Service.ServiceType;
import jakarta.servlet.http.HttpServlet;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Asus
 */
public class EditDoctorController extends AccountAuthentication {

    final String profileJsp = "../frontend/jsp/doctor/edit.jsp";

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User account = (User) session.getAttribute("account");
        UserDAO userDAO = new UserDAO();
        User user = null;
        try {
            user = userDAO.getUser(account.getEmail());
        } catch (Exception ex) {
            Logger.getLogger(EditDoctorController.class.getName()).log(Level.SEVERE, null, ex);
        }
        request.setAttribute("account", user);
        
        try {
            DoctorDAO doctorDAO = new DoctorDAO();
            ServiceDAO serviceDao = new ServiceDAO();
            ArrayList<ServiceType> listService = serviceDao.listServiceType();
            request.setAttribute("listService", listService);

            String email = request.getParameter("email");
            Doctor doctor = doctorDAO.getDoctorByEmail(email);
            request.setAttribute("doctor", doctor);
            request.getRequestDispatcher(profileJsp).forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(EditDoctorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User account = (User) session.getAttribute("account");
        UserDAO userDAO = new UserDAO();
        User user = null;
        try {
            user = userDAO.getUser(account.getEmail());
        } catch (Exception ex) {
            Logger.getLogger(EditDoctorController.class.getName()).log(Level.SEVERE, null, ex);
        }
        request.setAttribute("account", user);
        try {
            String email = request.getParameter("email");
            String raw_service = request.getParameter("service");
            String raw_status = request.getParameter("status");
            String twitter = request.getParameter("twitter");
            String facebook = request.getParameter("facebook");
            String linkedin = request.getParameter("linkedin");
            String instagram = request.getParameter("instagram");

            String position = request.getParameter("position");
            String trainingProcess = request.getParameter("trainingProcess");
            String workingProcess = request.getParameter("workingProcess");
            String researchWork = request.getParameter("researchWork");
            String scientificResearchTopics = request.getParameter("scientificResearchTopics");
            String bonus = request.getParameter("bonus");

            int service = Integer.parseInt(raw_service);
            boolean status = raw_status.equals("on");

            DoctorDAO doctorDAO = new DoctorDAO();
            Doctor doctor = doctorDAO.getDoctorByEmail(email);

            ServiceType serviceType = new ServiceType();
            serviceType.setT_id(service);

            doctor.setPosition(position);
            doctor.setTrainingProcess(trainingProcess);
            doctor.setWorkingProcess(workingProcess);
            doctor.setResearchWork(researchWork);
            doctor.setScientificResearchTopics(scientificResearchTopics);
            doctor.setBonus(bonus);
            doctor.setServiceType(serviceType);
            doctor.setTwitter(twitter);
            doctor.setFacebook(facebook);
            doctor.setLinkedin(linkedin);
            doctor.setInstagram(instagram);

            doctorDAO.updateStatus(status, email);
            Doctor doctorNew = doctorDAO.updateDoctor(doctor, email);
            request.setAttribute("doctor", doctorNew);
            request.getRequestDispatcher("../frontend/jsp/doctor/doctorDetail.jsp").forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(EditDoctorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
