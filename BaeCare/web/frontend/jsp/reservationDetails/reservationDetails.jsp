<%-- 
    Document   : reservationDetails
    Created on : Jun 2, 2022, 11:59:27 PM
    Author     : chitung
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../component/top.jsp"></jsp:include>

    <script>
        function checkAllService() {
            if (document.getElementById("checkAllService").checked == true) {
                checkboxes = document.getElementsByName('serviceSelect');
                for (var i = 0; i < checkboxes.length; i++) {
                    checkboxes[i].checked = true;
                }
            } else {
                checkboxes = document.getElementsByName('serviceSelect');
                for (var i = 0; i < checkboxes.length; i++) {
                    checkboxes[i].checked = false;
                }
            }
        }

        function deleteCart(id) {
            var result = confirm("Are you sure?");
            if (result) {
                window.location.href = "deleteCart?serviceID=" + id;
            }
        }

    </script>


    <!-- Full Screen Search Start -->
    <div class="modal fade" id="searchModal" tabindex="-1">
        <div class="modal-dialog modal-fullscreen">
            <div class="modal-content" style="background: rgba(9, 30, 62, .7);">
                <div class="modal-header border-0">
                    <button type="button" class="btn bg-white btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body d-flex align-items-center justify-content-center">
                    <div class="input-group" style="max-width: 600px;">
                        <input type="text" class="form-control bg-transparent border-primary p-3" placeholder="Type search keyword">
                        <button class="btn btn-primary px-4"><i class="bi bi-search"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Full Screen Search End -->
    <!-- Hero Start -->
    <div class="container-fluid bg-primary py-5 hero-header mb-5">
        <div class="row py-3">
            <div class="col-12 text-center">
                <h1 class="display-3 text-white animated zoomIn">Reservation Details</h1>
                <a href="home" class="h4 text-white">Home</a>
                <i class="far fa-circle text-white px-2"></i>
                <a href="service" class="h4 text-white">More Service</a>
            </div>
        </div>
    </div>
    <!-- Hero End -->

    <!-- Appointment Start -->
    <div class="mt-2 mb-5 page-body container" style="margin-top: 10px;">
        <div class="row">

        </div>
        <div class="row">


            <div class="">
                <c:if test="${requestScope.carts.size() gt 0}">


                    <div style="overflow-x: auto; overflow-y: auto">
                        <table id="multi-table" class="table table-striped table-bordered nowrap table-hover">

                            <tr class="text-center">
                                <th><input class="form-check-input" type="checkbox" id="checkAllService" onclick="checkAllService();" name="serviceSelect"></th>


                                <th>Service</th>

                                <th>Exam date</th>

                                <th>Original Price</th>
                                <th>Sale Price</th>


                                <th><a class="btn btn-danger" onclick="deleteCart(0);"><i class="bi bi-x-square"></i></a></th>
                            </tr>
                            <!-- comment --> 
                            <form action="${pageContext.request.contextPath}/reservation/contact" method="get" id="serviceToOrder">

                                <c:forEach items="${requestScope.carts}" var="c">

                                    <tr class="text-center">
                                        <td>
                                            <input class="form-check-input" type="checkbox" name="serviceSelect" value="${c.id}">
                                        </td>

                                        <!--Delete-->

                                        <td>${c.service.sname}</td>

                                        <td>${c.dateExam}</td>
                                    <input type="hidden" name="dateExam${c.id}" value="${c.dateExam}">

                                    <td>${c.service.original_price}</td>
                                    <td>${c.service.sale_price}</td>



                                    <td><a class="btn btn-danger" href="#" onclick="deleteCart(${c.service.sid});"><i class="bi bi-x-square"></i></a></td>

                                    </tr>

                                </c:forEach>
                            </form>

                        </table>
                    </div>


                    <div style="background-color: white;margin-left:  45%; width: 10%;color: black;margin-top: 10px"></div>
                    <br/>
                    <div class="row ">
                        <div class="col-lg-4">

                        </div>
                        <div class="col-lg-4 text-center">
                            <button form="serviceToOrder" type="submit" class="btn btn-primary w-100">Take Orders</button>
                        </div>
                        <div style="text-align:end" class="col-lg-4">

                        </div>

                        <!-- Go to service list page -->

                        <!-- Go to Reservation Contact page -->
                    </div>
                </c:if>
                <c:if test="${requestScope.carts.size() eq 0}">
                    <h1>You do not have service here. Go to <a style="text-decoration: underline" href="service">service page</a> to take a reservation!</h1>
                </c:if>
            </div>


        </div>
    </div>
    <!-- Appointment End -->

    <!-- Newsletter Start -->
    <div class="container-fluid position-relative pt-5 wow fadeInUp" data-wow-delay="0.1s" style="z-index: 1;">
        <div class="container">
            <!--            <div class="bg-primary p-5">
    <form class="mx-auto" style="max-width: 600px;">
        <div class="input-group">
            <h4 style="text-align: center">Have Nice Day</h4>
        </div>
    </form>
</div>-->
        </div>
    </div>
    <!-- Newsletter End -->
</body>
<jsp:include page="../component/bottom.jsp"></jsp:include>

</html>